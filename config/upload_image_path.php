<?php
return [
    "gallery" => "galleries/",
    "post-thumbnail" => "post-thumbnails/",
    "brand-thumbnail" => "brand-thumbnails/",
    "product-thumbnail" => "product-thumbnails/",
    "category-thumbnail" => "category-thumbnails/",
    "seo-image" => "seo-images/",
    "themes" => "themes/",
    "ticket-attachment" => "ticket-attachment/",
];
