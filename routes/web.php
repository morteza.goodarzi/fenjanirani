<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ConsultationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductKeyController;
use App\Http\Controllers\ProductValueController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SearchHistoryController;
use App\Http\Controllers\ShoppingCartController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketMessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(["prefix"=>"panel","as"=>"admin.",'middleware' => ["auth","role:super-admin"]],function (){
    Route::get("/dashboard",[DashboardController::class,"index"])->name("dashboard.index");

    Route::post('/ckeditor/upload',[CKEditorController::class,"upload"])->name("ckeditor.upload");

    // category route
    Route::get('/categories',[CategoryController::class,"index"])->name("categories.index");
    Route::get('/categories/create',[CategoryController::class,"create"])->name("categories.create");
    Route::post('/categories/{category:slug?}',[CategoryController::class,"store"])->name("categories.store");
    Route::get('/categories/{category:slug}/edit',[CategoryController::class,"edit"])->name("categories.edit");
    Route::get('/categories/{category:slug}',[CategoryController::class,"destroy"])->name("categories.destroy");

    // post route
    Route::get('/posts',[PostController::class,"index"])->name("posts.index");
    Route::get('/posts/create',[PostController::class,"create"])->name("posts.create");
    Route::post('/posts/{post:slug?}',[PostController::class,"store"])->name("posts.store");
    Route::get('/posts/{post:slug}/edit',[PostController::class,"edit"])->name("posts.edit");
    Route::get('/posts/{post:slug}',[PostController::class,"destroy"])->name("posts.destroy");

    // comments  route
    Route::get('/comments',[CommentController::class,"index"])->name("comments.index");
    Route::get('/comments/create',[CommentController::class,"create"])->name("comments.create");
    Route::post('/comments/{comment:id?}',[CommentController::class,"store"])->name("comments.store");
    Route::get('/comments/{comment:id}/edit',[CommentController::class,"edit"])->name("comments.edit");
    Route::get('/comments/{comment:id}',[CommentController:: class,"destroy"])->name("comments.destroy");

    // brand route
    Route::get('/brands',[BrandController::class,"index"])->name("brands.index");
    Route::get('/brands/create',[BrandController::class,"create"])->name("brands.create");
    Route::post('/brands/{brand:slug?}',[BrandController::class,"store"])->name("brands.store");
    Route::get('/brands/{brand:slug}/edit',[BrandController::class,"edit"])->name("brands.edit");
    Route::get('/brands/{brand:slug}',[BrandController::class,"destroy"])->name("brands.destroy");

    // product keys route
    Route::get('/keys',[ProductKeyController::class,"index"])->name("keys.index");
    Route::get('/keys/create',[ProductKeyController::class,"create"])->name("keys.create");
    Route::post('/keys/{key:id?}',[ProductKeyController::class,"store"])->name("keys.store");
    Route::get('/keys/{key:id}/edit',[ProductKeyController::class,"edit"])->name("keys.edit");
    Route::get('/keys/{key:id}',[ProductKeyController:: class,"destroy"])->name("keys.destroy");

    // product values route
    Route::get('/values',[ProductValueController::class,"index"])->name("values.index");
    Route::get('/values/create',[ProductValueController::class,"create"])->name("values.create");
    Route::post('/values/{value:id?}',[ProductValueController::class,"store"])->name("values.store");
    Route::get('/values/{value:id}/edit',[ProductValueController::class,"edit"])->name("values.edit");
    Route::get('/values/{value:id}',[ProductValueController:: class,"destroy"])->name("values.destroy");
    Route::get('/ajax-values',[ProductValueController:: class,"getByKeyAjax"])->name("values.by.Key.ajax");

    // product  route
    Route::get('/products',[ProductController::class,"index"])->name("products.index");
    Route::get('/products/create',[ProductController::class,"create"])->name("products.create");
    Route::post('/products/{product:slug?}',[ProductController::class,"store"])->name("products.store");
    Route::get('/products/{product:slug}/edit',[ProductController::class,"edit"])->name("products.edit");
    Route::get('/products/{product:slug}',[ProductController:: class,"destroy"])->name("products.destroy");

    // orders  route
    Route::get('/orders',[OrderController::class,"index"])->name("orders.index");
    Route::get('/orders/create',[OrderController::class,"create"])->name("orders.create");
    Route::post('/orders/{order:id?}',[OrderController::class,"store"])->name("orders.store");
    Route::get('/orders/{order:id}/edit',[OrderController::class,"edit"])->name("orders.edit");
    Route::get('/orders/{order:id}',[OrderController:: class,"destroy"])->name("orders.destroy");
    Route::get('/orders/change-status/{order:id}/{status}',[OrderController:: class,"changeStatus"])->name("orders.change.status");

    Route::get("tickets",[TicketController::class,"index"])->name("ticket.index");
    Route::get("tickets/{ticket:code}",[TicketController::class,"detail"])->name("ticket.detail");
    Route::get("tickets/close/{ticket:code}",[TicketController::class,"close"])->name("ticket.close");
    Route::get("tickets/close/{ticket:code}",[TicketController::class,"close"])->name("ticket.close");

    //menu route
    Route::get("menu",[MenuController::class,"index"])->name("menu.index");
    Route::get("menu/new",[MenuController::class,"create"])->name("menu.create");
    Route::get("menu/edit/{menu:id}",[MenuController::class,"edit"])->name("menu.edit");
    Route::post("menu/new/{menu:id?}",[MenuController::class,"store"])->name("menu.store");
    Route::get("menu/delete/{menu:id}",[MenuController::class,"destroy"])->name("menu.destroy");

    //page route
    Route::get("pages",[PageController::class,"index"])->name("page.index");
    Route::get("pages/new",[PageController::class,"create"])->name("page.create");
    Route::get("pages/edit/{page:slug}",[PageController::class,"edit"])->name("page.edit");
    Route::post("pages/new/{page:slug?}",[PageController::class,"store"])->name("page.store");
    Route::get("pages/delete/{page:slug}",[PageController::class,"destroy"])->name("page.destroy");

    Route::get("consultations",[ConsultationController::class,"index"])->name("consultations.index");
    Route::get("newsletter",[NewsletterController::class,"index"])->name("newsletter.index");
});

Route::group(["prefix"=>"profile","as"=>"profile.",'middleware' => ["auth","role:user"]],function (){
    Route::get("/",[ProfileController::class,"index"])->name("index");
    Route::get("/edit",[ProfileController::class,"profileEdit"])->name("edit");
    Route::post("/getCitiesAjax",[CityController::class,"getCitiesAjax"])->name("cities.ajax");
    Route::post("/update",[ProfileController::class,"updateProfile"])->name("profile.update");
    Route::get("/address",[AddressController::class,"address"])->name("address.index");
    Route::post("/address/create",[AddressController::class,"store"])->name("address.create");
    Route::get("/orders",[OrderController::class,"orderProfile"])->name("order.index");
    Route::get("/orders/{order:refuse_id}",[OrderController::class,"orderProfileDetail"])->name("order.detail");
    Route::get("/tickets",[TicketController::class,"profileTicket"])->name("ticket.index");
    Route::get("/tickets/new",[TicketController::class,"profileNewTicket"])->name("ticket.new");
    Route::get("/tickets/{ticket:code}",[TicketController::class,"profileTicketDetail"])->name("ticket.detail");
    Route::post("/tickets/create",[TicketController::class,"store"])->name("ticket.store");
    Route::post("/tickets-message/create/{ticket:code}",[TicketMessageController::class,"store"])->name("ticket.message.store");
    Route::post('/post/{post:slug}',[PostController::class,"storeComment"])->name("post.comment.store");
    Route::post('/product/{product:slug}',[ProductController::class,"storeComment"])->name("product.comment.store");
    Route::get("/payments",[PaymentController::class,"paymentProfile"])->name("payment.index");

    Route::get('/checkout', [ShoppingCartController::class, 'checkout'])->name('shopping-cart.checkout');
    Route::post('/order/submit', [ShoppingCartController::class, 'SendPayment'])->name('shopping-cart.payment.send');
    Route::get('/verify', [PaymentController::class, 'verify'])->name('payment.verify');

});


Route::group(["middleware" => "redirect"],function (){
    Route::get("/",[HomeController::class,"index"])->name("home");
    Route::get('/product/{product:slug}',[ProductController::class,"show"])->name("products.show");
    Route::post('/newsletter/create',[NewsletterController::class,"store"])->name("newsletter.store");
    Route::post('/consultation/create',[ConsultationController::class,"store"])->name("consultation.store");

    Route::get('/profile/login',[LoginController::class,"showLoginPage"])->name("login.page");
    Route::post('/profile/login',[LoginController::class,"profileLogin"])->name("profile.login");
    Route::post('/profile/authenticate',[LoginController::class,"doLogin"])->name("profile.authenticate");
    Route::get("/page/{page:slug}",[PageController::class,"show"])->name("page.show");
    Route::get("/post/{post:slug}",[PostController::class,"show"])->name("post.show");
    Route::get("/category/post/{category:slug?}",[CategoryController::class,"showPost"])->name("category.post.show");
    Route::get("/category/product/{product:slug?}",[CategoryController::class,"showProduct"])->name("category.product.show");
    Route::get("/post/search",[PostController::class,"search"])->name("post.search");
    Route::post('/cart/add/{product:slug}', [ShoppingCartController::class, 'addToCart'])->name('shopping-cart.add');
    Route::get('/cart', [ShoppingCartController::class, 'index'])->name('shopping-cart.index');
    Route::post('/cart/destroy', [ShoppingCartController::class, 'destroy'])->name('shopping-cart.destroy');
    Route::post('/cart/update', [ShoppingCartController::class, 'update'])->name('shopping-cart.update');
    Route::post('/cart/discount/update', [DiscountController::class, 'update'])->name('discount.update');
    Route::post('/search', [SearchHistoryController::class, 'search'])->name('search.post');
});
