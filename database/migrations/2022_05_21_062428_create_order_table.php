<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('refuse_id');
            $table->string('delivery_code');
            $table->bigInteger('shopping_cart_id')->unsigned();
            $table->tinyInteger('buy_type')->comment("1:online, 2:presence ");
            $table->string('tracking_code')->unique();
            $table->integer('status')->comment("1010:unpaid, 1011:paid,1012: preparing, 1013: sending, 1014: done")->default(config("order-status.unPaid"));
            $table->text('description');
            $table->string('receiver_name');
            $table->string('receiver_family');
            $table->text('receiver_address');
            $table->string('receiver_national_code');
            $table->string('receiver_postal_code');
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("shopping_cart_id")->references("id")->on("shopping_carts")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
