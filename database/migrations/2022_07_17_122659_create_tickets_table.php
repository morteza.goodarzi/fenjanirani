<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string("code")->unique();
            $table->string("subject");
            $table->integer("priority");
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("sms_notifiable")->unsigned()->comment("0: disabled, 1: enabled")->default(1);
            $table->bigInteger("department_id")->unsigned()->nullable();
            $table->integer("status")->default(config("ticket-status.awaiting-answer"));
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
