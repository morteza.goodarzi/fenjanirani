<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCartItemValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart_item_values', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("cart_item_id")->unsigned();
            $table->bigInteger("product_key_id")->unsigned();
            $table->bigInteger("product_value_id")->unsigned();
            $table->decimal("price",16,0);
            $table->timestamps();

            $table->foreign("cart_item_id")->references("id")->on("shopping_cart_items")->onDelete("cascade");
            $table->foreign("product_key_id")->references("id")->on("product_keys")->onDelete("cascade");
            $table->foreign("product_value_id")->references("id")->on("product_values")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart_item_value');
    }
}
