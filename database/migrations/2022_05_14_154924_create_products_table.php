<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->string("slug")->unique();
            $table->decimal("price")->unique();
            $table->boolean("exist")->default(true);
            $table->integer("warranty")->nullable()->comment("warranty by month count");
            $table->bigInteger("brand_id")->unsigned()->nullable();
            $table->integer("comment_count")->default(0);
            $table->longText("overview");
            $table->longText("expert_review")->nullable();
            $table->boolean("indexable")->default(true);
            $table->boolean("commentable")->default(true);
            $table->string("thumbnail")->default("thumbnail.png");

            // seo field
            $table->string("canonical")->comment("Canonical Url")->nullable();
            $table->string("seo_title")->nullable();
            $table->string("seo_description")->nullable();
            $table->string("seo_image")->default("thumbnail.png");
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
