<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->decimal("price",10,0);
            $table->bigInteger("user_id")->unsigned();
            $table->string("tracking_code")->nullable();
            $table->text("description")->nullable();
            $table->integer("status")->comment("1: initialize, 2: success,3: failed");
            $table->integer("bank_id")->comment("1:Mellat,2: Sepehr, 3: Melli, 4:Pasargad,5: ZarinPal");
            $table->bigInteger("order_id")->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
