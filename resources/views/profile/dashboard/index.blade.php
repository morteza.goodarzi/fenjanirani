@extends("layouts.profile.riady")
@section("head")
    <title>داشبورد</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header no-border">
                        <h4 class="box-title">تاریخچه ورود به سیستم</h4>
                    </div>
                </div>
                <div class="box">
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th class="text-center">مرورگر</th>
                                    <th class="text-center">آدرس ip</th>
                                    <th class="text-center">تاریخ</th>
                                    <th class="text-center">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Chrome/77.0.3865.90</td>
                                    <td>54.15.64.87</td>
                                    <td>CH</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">گرفتن دسترسی</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-body">
                        <div class="d-flex flex-row">
                            <div class=""><img src="{{ asset("avatar/avatar.png") }}" alt="user" class="rounded-circle bg-success-light" width="100"></div>
                            <div class="ps-20">
                                <h3>{{ \Illuminate\Support\Facades\Auth::user()->name." ". \Illuminate\Support\Facades\Auth::user()->family }}</h3>
                                <h6>شغل شما</h6>
                            </div>
                        </div>
                        <div class="row mt-40">
                            <div class="col b-r text-center">
                                <h2 class="font-light">0</h2>
                                <h6>پست ها</h6></div>
                            <div class="col b-r text-center">
                                <h2 class="font-light">0</h2>
                                <h6>نظرات</h6></div>
                            <div class="col text-center">
                                <h2 class="font-light">0</h2>
                                <h6>اشتراک گذاری</h6></div>
                        </div>
                    </div>
                    <div class="box-body">
                        <p class="text-center aboutscroll">
                            با فعال کردن اکانت وبلاگ خود در فنجان ایرانی میتوانید به عنوان نویسنده در پلتفرم بلاگ فنجان ایرانی فعالیت کنید.
                        </p>
                        <ul class="list-inline text-center">
                            <li><a href="javascript:void(0)" data-bs-toggle="tooltip" title="" data-bs-original-title="Website" aria-label="Website"><i class="fa fa-globe fs-20"></i></a></li>
                            <li><a href="javascript:void(0)" data-bs-toggle="tooltip" title="" data-bs-original-title="twitter" aria-label="twitter"><i class="fa fa-twitter fs-20"></i></a></li>
                            <li><a href="javascript:void(0)" data-bs-toggle="tooltip" title="" data-bs-original-title="Facebook" aria-label="Facebook"><i class="fa fa-facebook-square fs-20"></i></a></li>
                        </ul>
                        <div class="text-center">
                            <button class="btn btn-info-light mb-4 mt-2"><i class="ti-check"></i> فعال کردن اکانت وبلاگ</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">

            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
