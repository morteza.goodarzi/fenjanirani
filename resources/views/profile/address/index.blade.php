@extends("layouts.profile.riady")
@section("head")
    {{--    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />--}}
    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />
    <title>مدیریت آدرس ها</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-12">
                        @include("profile.partials.errors")
                        @include("profile.partials.notifications")
                    </div>
                </div>
                <div class="box">
                    <form class="form" method="post" action="{{ route("profile.address.create") }}">
                        @csrf
                        <div class="box-body">
                            <h4 class="box-title text-info mb-0"><i class="mdi mdi-map-marker-multiple me-15"></i>ثبت آدرس جدید</h4>
                            <hr class="my-15">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">آدرس</label>
                                        <textarea type="text" class="form-control" placeholder="آدرس" name="address"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-md mb-md-0 mb-2">
                                <i class="ti-save-alt"></i> ثبت
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="text-center">لیست آدرس ها</th>
                                </tr>
                                @forelse($address as $item)
                                    <tr>
                                        <td>{{ $item->address }}</td>
                                        <td><a class="btn btn-danger btn-md" href="#"><i class="ti-trash"></i>حذف</a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>داده ای برای نمایش وجود ندارد</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
