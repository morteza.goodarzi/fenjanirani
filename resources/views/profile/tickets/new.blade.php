@extends("layouts.profile.riady")
@section("head")
    {{--    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />--}}
    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />
    <title>ایجاد تیکت جدید</title>
    <style>
        textarea{
            height: 200px !important;
            resize: none;
        }
    </style>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-12">
                        @include("profile.partials.errors")
                        @include("profile.partials.notifications")
                    </div>
                </div>
                <div class="box">
                    <form class="form" method="post" action="{{ route("profile.ticket.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i>ایجاد تیکت جدید</h4>
                            <hr class="my-15">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">موضوع</label>
                                        <input autocomplete="off" type="text" class="form-control " placeholder="موضوع" name="subject" value="{{ old("subject") }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">اولویت</label>
                                        <select class="form-control"  name="priority" id="priority">
                                            <option value="{{ config("message-priority.low") }}">پایین</option>
                                            <option selected value="{{ config("message-priority.medium") }}">متوسط</option>
                                            <option value="{{ config("message-priority.high") }}">بالا</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">دپارتمان</label>
                                        <select class="form-control" name="department_id" id="department">
                                            <option value="{{ array_keys(config("department"))[0] }}">واحد حسابداری</option>
                                            <option value="{{ array_keys(config("department"))[1] }}">واحد فنی</option>
                                            <option value="{{ array_keys(config("department"))[2] }}">واحد مدیریت</option>
                                            <option selected value="{{ array_keys(config("department"))[3] }}">واحد پشتیبانی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">پیام</label>
                                        <textarea class="form-control" placeholder="پیام" name="message">{{ old("message") }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="my-3">
                                        <label for="formFile" class="form-label">انتخاب فایل ضمیمه <strong> (فقط فرمت png و jpg مجاز است)</strong></label>
                                        <input class="form-control" name="attachment" accept=".jpg, .png, .jpeg" type="file" id="formFile">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <input type="checkbox" id="basic_checkbox_1" name="sms_notifiable">
                                        <label for="basic_checkbox_1">بعد از هر پاسخ به من sms بده</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-md mb-md-0 mb-2">
                                <i class="ti-save-alt"></i> ثبت
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")
@endsection
