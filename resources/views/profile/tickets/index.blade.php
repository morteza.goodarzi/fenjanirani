@extends("layouts.profile.riady")
@section("head")
    <title>سفارشات من</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ route("profile.ticket.index",["status" => config("ticket-status.rejected")]) }}" class="btn btn-primary btn-md mb-2">رد شده</a>
                                <a href="{{ route("profile.ticket.index",["status" => config("ticket-status.awaiting-answer")]) }}" class="btn btn-warning btn-md mb-2">در انتظار پاسخ</a>
                                <a href="{{ route("profile.ticket.index",["status" => config("ticket-status.answered")]) }}" class="btn btn-success btn-md mb-2">پاسخ داده شده</a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route("profile.ticket.new") }}" class="btn btn-primary btn-md mb-2 float-none float-lg-end"> جدید</a>

                            </div>
                        </div>

                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">شماره تیکت</th>
                                    <th class="text-center">کاربر</th>
                                    <th class="text-center">موضوع</th>
                                    <th class="text-center">وضعیت</th>
                                    <th class="text-center">تاریخ</th>
                                    <th class="text-center">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($tickets as $ticket)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ticket->code }}</td>
                                            <td>{{ $ticket->user->name." ". $ticket->user->family }}</td>
                                            <td>{{ $ticket->subject }}</td>
                                            <td>
                                                <span
                                                @if($ticket->status == config("ticket-status.awaiting-answer"))
                                                    class="badge badge-primary"
                                                @elseif($ticket->status == config("ticket-status.rejected"))
                                                class="badge badge-danger"
                                                @elseif($ticket->status == config("ticket-status.answered"))
                                                class="badge badge-success"
                                                @endif
                                                >
                                                    {{ $ticket->persianStatus }}
                                                </span>
                                            </td>
                                            <td>{{ \Morilog\Jalali\Jalalian::forge($ticket->created_at)->format("%d - %m - %Y") }}</td>
                                            <td>
                                                <a href="{{ route("profile.ticket.detail",["ticket" => $ticket->code]) }}" class="btn btn-primary btn-md">جزئیات</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <th class="text-center p-5" colspan="9">
                                                <h4>داده ای برای نمایش وجود ندارد.</h4>
                                            </th>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
