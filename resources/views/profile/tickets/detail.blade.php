@extends("layouts.profile.riady")
@section("head")
    <title>جزئیات تیکت</title>
    <style>
        textarea{
            height: 200px !important;
        }
        .slimScrollDiv{
            height:0 !important;
        }
        @media(max-width: 768px){
            .mobile-w-100{
                max-width:100% !important;
                width:100% !important;
            }
        }
    </style>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include("profile.partials.notifications")
                <div class="box">
                    <div class="box-header">
                        <div class="media align-items-top p-0">
                            <a class="avatar avatar-lg status-success mx-0" href="#">
                                <img src="{{ asset("profile-assets/images/logo-dark-text.png") }}" class="rounded-circle" alt="...">
                            </a>
                            <div class="d-lg-flex d-block justify-content-between align-items-center w-p100">
                                <div class="media-body mb-lg-0 mb-20">
                                    <p class="fs-16">
                                        <a class="hover-primary" href="#"><strong>{{ config("department.".$ticket->department_id) }}</strong></a>
                                    </p>
                                    <p class="fs-12">آخرین پیام در 12:24 بعد از ظهر</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="slimScrollDiv">
                            <div class="chat-box-one2">
                                @forelse($ticket->messages as $message)
                                <div class="card d-inline-block mb-3 me-0 me-md-2 max-w-p80 mobile-w-100 @if($message->is_admin) float-end bg-primary @else no-shadow float-start bg-lighter  @endif">
                                    <div class="position-absolute pt-1 pe-2 r-0">
                                        <span class="text-extra-small ms-3  @if($message->is_admin) text-white @else text-dark @endif">{{ \Morilog\Jalali\Jalalian::forge($message->created_at)->format("i : H") }}</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex flex-row pb-2">
                                            <a class="d-flex" href="#">
                                                <img alt="Profile" src="{{ asset("front/demo4/images/vendor/avatar/4.jpg") }}" class="avatar me-10">
                                            </a>
                                            <div class="d-flex flex-grow-1 min-width-zero">
                                                <div class="m-2 ps-0 align-self-center d-flex flex-column flex-lg-row justify-content-between">
                                                    <div class="min-width-zero">
                                                        <p class="mb-0 fs-16 @if($message->is_admin) text-white @else text-dark @endif">{{ $message->user->name." ".$message->user->family }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat-text-start ps-55">
                                            <p class="mb-0 text-semi-muted">{!! $message->message !!}</p>
                                            @if($message->attachment)
                                                <a href="{{ asset("ticket-attachment/default")."/".$message->attachment }}" target="_blank" class="btn btn-primary btn-md mt-3">فایل ضمیمه</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                @empty

                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <form class="form" method="post" action="{{ route("profile.ticket.message.store",["ticket" => $ticket->code]) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="box-body">
                                <h4 class="box-title text-info mb-0"><i class="ti-email me-15"></i>پاسخ</h4>
                                <hr class="my-15">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">پیام</label>
                                            <textarea class="form-control" placeholder="پیام" name="message">{{ old("message") }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="my-3">
                                            <label for="formFile" class="form-label">انتخاب فایل ضمیمه <strong> (فقط فرمت png و jpg مجاز است)</strong></label>
                                            <input class="form-control" name="attachment" accept=".jpg, .png, .jpeg" type="file" id="formFile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-md mb-md-0 mb-2">
                                    <i class="ti-save-alt"></i> ثبت
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
