@extends("layouts.profile.riady")
@section("head")
    <title>سفارشات من</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ route("profile.order.index",["status" => config("order-status.delivered")]) }}" class="btn btn-success btn-md mb-2">تحویل داده شده</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.sending")]) }}" class="btn btn-primary btn-md mb-2">درحال ارسال</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.preparing")]) }}" class="btn btn-danger btn-md mb-2">درحال پردازش</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.unPaid")]) }}" class="btn btn-warning btn-md mb-2">پرداخت نشده</a>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">شماره سفارش</th>
                                        <th class="text-center">کد تحویل</th>
                                        <th class="text-center">محصولات</th>
                                        <th class="text-center">وضعیت</th>
                                        <th class="text-center">تاریخ ایجاد</th>
                                        <th class="text-center">عملیات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($orders as $order)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $order->refuse_id }}</td>
                                        <td>{{ $order->delivery_code }}</td>
                                        <td>
                                        @forelse($order->shoppingCart->cartItem as $item)
                                            <p>{{ $loop->iteration.".".$item->product->title ." (". $item->quantity." عدد)"  }}</p>
                                            @empty
                                                -
                                            @endforelse
                                        </td>
                                        <td>
                                            <span
                                            @if($order->status == config("order-status.unPaid")) class="badge badge-danger"
                                            @elseif($order->status == config("order-status.sending")) class="badge badge-primary"
                                            @elseif($order->status == config("order-status.preparing")) class="badge badge-info"
                                            @elseif($order->status == config("order-status.delivered")) class="badge badge-success"
                                            @endif >
                                                {{ $order->persianStatus }}
                                            </span>
                                        </td>
                                        <td>{{ \Morilog\Jalali\Jalalian::forge($order->created_at)->format("%d - %m - %Y") }}</td>
                                        <td>
                                            <a href="{{ route("profile.order.detail",["order" => $order->refuse_id]) }}" class="btn btn-primary btn-md">جزئیات</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9">داده ای برای نمایش وجود ندارد</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
