@extends("layouts.profile.riady")
@section("head")
    <title>سفارشات من</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <p class="mb-3 text-center">شماره سفارش {{ $order->refuse_id }} <span
                                @if($order->status == config("order-status.unPaid")) class="badge badge-danger mr-2"
                                @elseif($order->status == config("order-status.sending")) class="badge badge-primary mr-2"
                                @elseif($order->status == config("order-status.preparing")) class="badge badge-info mr-2"
                                @elseif($order->status == config("order-status.delivered")) class="badge badge-success mr-2"
                                            @endif >
                                                {{ $order->persianStatus }}
                                            </span></p>
                        <div class="row">
                            <div class="col-6">
                                <p class="m-0">
                                    کد تحویل: {{ $order->delivery_code }}
                                </p>
                            </div>
                            <div class="col-6">
                                <p class="m-0 text-end">{{ \Morilog\Jalali\Jalalian::forge($order->created_at)->format("%d - %m - %Y") }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">تصویر</th>
                                        <th class="text-center">نام محصول</th>
                                        <th class="text-center">ویژگی ها</th>
                                        <th class="text-center">قیمت خرید</th>
                                        <th class="text-center">عملیات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($order->shoppingCart->cartItem as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <img src="{{ asset("product-thumbnails/sm")."/".$item->product->thumbnail }}" class="thumbnail" alt="">
                                        </td>
                                        <td>{{ $item->product->title }}</td>
                                        <td>
                                            @if($item->itemValue->count() > 0)
                                                {{ $item->itemValue->value->value }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ number_format($item->price)." تومان" }}</td>
                                        <td>
                                            <a href="{{ route("products.show",["product" => $item->product->slug ]) }}" target="_blank" class="btn btn-primary btn-md">نمایش محصول</a>
                                        </td>
                                    </tr>
                                    @empty
                                        در این سفارش محصولی وجود ندارد.
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="9">جمع کل سفارش: {{ $order->shoppingCart->total_price }} تومان</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
