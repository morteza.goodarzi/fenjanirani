@extends("layouts.profile.riady")
@section("head")
{{--    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />--}}
    <link rel="stylesheet" href="{{ asset("profile-assets/persian-date-picker/css/persianDatepicker-default.css") }}" />
    <title>ویرایش پروفایل</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-12">
                        @include("profile.partials.errors")
                    </div>
                </div>
                <div class="box">
                    <form class="form" method="post" action="{{ route("profile.profile.update") }}">
                        @csrf
                        <div class="box-body">
                            <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i>ویرایش اطلاعات کاربری</h4>
                            <hr class="my-15">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">نام</label>
                                        <input type="text" class="form-control" placeholder="نام" name="name" value="{{ $user->name }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">نام خانوادگی</label>
                                        <input type="text" class="form-control" placeholder="نام خانوادگی" name="family" value="{{ $user->family }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">ایمیل</label>
                                        <input type="text" class="form-control" placeholder="ایمیل" name="email" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">تاریخ تولد</label>
                                        <input autocomplete="off" class="form-control" type="text" id="birth-date" name="birth_date" value="{{ $user->birth_date }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">استان</label>
                                        <select class="form-select" id="province" name="province">
                                            @foreach($provinces as $province)
                                                <option @if($province->id == $user->province) selected @endif value="{{ $province->id }}">{{ $province->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">شهر</label>
                                        <select class="form-select" name="city" id="city">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">کد پستی</label>
                                        <input type="text" class="form-control" placeholder="کد پستی" name="postal_code" value="{{ $user->postal_code }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">کد ملی</label>
                                        <input type="text" class="form-control" placeholder="کد ملی" name="national_code" value="{{ $user->national_code }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-md mb-md-0 mb-2">
                                <i class="ti-save-alt"></i> ویرایش
                            </button>
                            <a  href="#" class="btn btn-success btn-md">
                                <i class="ti-check"></i> فعال کردن اکانت فنجان کلاب
                            </a>
                        </div>
                    </form>
                </div>
                <div class="box">
                    <form action="#" method="post">
                        @csrf
                        <div class="box-body">
                            <h4 class="box-title text-info mb-0"><i class="ti-mobile me-15"></i>تغییر تلفن همراه</h4>
                            <hr class="my-15">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">تلفن همراه</label>
                                        <input type="text" class="form-control" placeholder="مثال: 09121111111" name="mobile" autocomplete="off" value="{{ $user->mobile }}">
                                    </div>
                                    <div class="form-group">
                                        <a href="#" class="btn btn-primary btn-md">
                                            <i class="ti-email"></i>
                                            ارسال کد تایید
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">کد تایید</label>
                                        <input type="text" class="form-control" placeholder="کد تایید" name="mobile" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <a href="#" class="btn btn-success btn-md">
                                            <i class="ti-save-alt"></i>
                                            تغییر تلفن همراه</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")
    <script src="{{ asset("profile-assets/persian-date-picker/js/persianDatepicker.min.js") }}"></script>
    <script>
        $("#elementId, .elementClass").persianDatepicker();
    </script>
    <script type="text/javascript">
        $(function() {
            let date = '{{ $user->birth_date }}';
            $("#birth-date").persianDatepicker({
                formatDate: "YYYY/MM/DD",
                showGregorianDate: true,
                cellWidth: 40,
                cellHeight: 40,
            });
            document.getElementById("birth-date").SelectedDate = DateTime.Today;
        });
    </script>
    <script>
        let province = {{ $user->province }};
        $(document).ready(function (){
            $.ajax({
                type:'POST',
                url:"{{ route("profile.cities.ajax") }}",
                beforeSend: function() {
                    $("#loading").show();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "province_id": province
                },
                success:function(data) {
                    $("#loading").hide();
                    $("#city").html(data).find("option[value='{{ $user->city }}']").attr("selected",true);
                }
            });
        });
        $(document).on("change","#province",(function( event ) {
           province = $(this).val();
            $.ajax({
                type:'POST',
                url:"{{ route("profile.cities.ajax") }}",
                beforeSend: function() {
                    $("#loading").show();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "province_id": province
                },
                success:function(data) {
                    $("#loading").hide();
                    $("#city").html(data);
                }
            });
        }));
    </script>
@endsection
