<header class="main-header">
    <div class="d-flex align-items-center logo-box justify-content-start">
        <a href="#" class="waves-effect waves-light nav-link d-none d-md-inline-block mx-10 push-btn bg-transparent hover-primary" data-toggle="push-menu" role="button">
            <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
        </a>
        <!-- Logo -->
        <a href="index.html" class="logo">
            <!-- logo-->
            <div class="logo-lg">
                پنل فنجان ایرانی
            </div>
        </a>
    </div>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top navbar-light">
        <!-- Sidebar toggle button-->
        <div class="app-menu">
            <ul class="header-megamenu nav">
                <li class="btn-group nav-item d-md-none">
                    <a href="#" class="waves-effect waves-light nav-link push-btn btn-info-light" data-toggle="push-menu" role="button">
                        <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                    </a>
                </li>

            </ul>
        </div>

        <div class="navbar-custom-menu r-side">
            <ul class="nav navbar-nav">
                <li class="btn-group nav-item d-lg-inline-flex d-none">
                    <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link full-screen btn-info-light" title="Full Screen">
                        <i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
                    </a>
                </li>
                <li class="dropdown messages-menu">
                    <span class="label label-primary">0</span>
                    <a href="#" class="dropdown-toggle btn-primary-light" data-bs-toggle="dropdown" title="Messages">
                        <i class="icon-Incoming-mail"><span class="path1"></span><span class="path2"></span></i>
                    </a>
                    <ul class="dropdown-menu animated bounceIn" style="min-width: 300px">

                        <li class="header">
                            <div class="p-20">
                                <div class="flexbox">
                                    <div>
                                        <h4 class="mb-0 mt-0">پیام های شما</h4>
                                    </div>
                                    <div>
                                        <a href="#" class="text-danger">حذف همه</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <ul class="menu sm-scrol">
                                <li>
                                    <a href="#">
                                        هیچ پیامی وجود ندارد.
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- User Account-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle p-0 text-dark hover-primary ms-md-30 ms-10" data-bs-toggle="dropdown" title="User">
                        <span class="ps-30 d-md-inline-block d-none">سلام</span> <strong class="d-md-inline-block d-none">
                            @if(\Illuminate\Support\Facades\Auth::user()->name)
                                {{ \Illuminate\Support\Facades\Auth::user()->name }}
                            @else
                                کاربر عزیز
                            @endif
                        </strong><img src="{{ asset("profile-assets/images/avatar/avatar-7.png") }}" class="user-image rounded-circle avatar bg-white mx-10" alt="User Image">
                    </a>
                    <ul class="dropdown-menu animated flipInX">
                        <li class="user-body">
                            <a class="dropdown-item" href="{{ route("profile.edit") }}"><i class="ti-user text-muted me-2"></i> پروفایل</a>
                            <a class="dropdown-item" href="{{ route("profile.order.index") }}"><i class="ti-wallet text-muted me-2"></i>سفارشات</a>
{{--                            <a class="dropdown-item" href="#"><i class="ti-settings text-muted me-2"></i>تنظیمات اکانت</a>--}}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"><i class="ti-lock text-muted me-2" onclick="event.preventDefault();document.getElementById('logout-form').submit();" ></i>خروج</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
