@if(session('save'))
    <div class="alert alert-success mt-3">
        عملیات با موفقیت انجام شد!
    </div>
@endif
@if(session('delete'))
    <div class="alert alert-danger mt-3">
        عملیات با موفقیت انجام شد!
    </div>
@endif
@if(session('file-error'))
    <div class="alert alert-danger mt-3">
        فرمت فایل مجاز نیست!
    </div>
@endif
@if(session('cart.error'))
    <div class="alert alert-danger mt-3 text-white">
        کد تخفیف وارد شده معتبر نیست
    </div>
@endif
