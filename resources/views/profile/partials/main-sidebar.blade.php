<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">
        <div class="multinav">
            <div class="multinav-scroll" style="height: 100%;">
                <!-- sidebar menu-->
                <ul class="sidebar-menu" data-widget="tree">
                    <li>
                        <a href="{{ route("profile.index") }}">
                            <i class="mdi mdi-view-dashboard"></i>
                            <span>داشبورد</span>
                        </a>
                    </li>
                    <li class="header">اطلاعات حساب کاربری</li>
                    <li>
                        <a href="{{ route("profile.edit") }}">
                            <i class="mdi mdi-account-card-details"></i>
                            <span>اطلاعات حساب کاربری</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route("profile.address.index") }}">
                            <i class="mdi mdi-map-marker-multiple"></i>
                            <span>آدرس ها</span>
                        </a>
                    </li>
                    <li class="header">فروشگاه</li>
                    <li>
                        <a href="{{ route("profile.order.index") }}">
                            <i class="mdi mdi-cart"></i>
                            <span>سفارشات</span>
                        </a>
                    </li>
{{--                    <li class="treeview">
                        <a href="#">
                            <i class="mdi mdi-rotate-left"></i>
                            <span>مرجوع کردن کالا</span>
                        </a>
                    </li>--}}

                    <li>
                        <a href="{{ route("profile.payment.index") }}">
                            <i class="mdi mdi-credit-card"></i>
                            <span>تراکنش ها</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route("profile.ticket.index") }}">
                            <i class="mdi mdi-email-outline"></i>
                            <span>پشتیبانی</span>
                        </a>
                    </li>
                    <li class="header">محتوا</li>
                    <li class="treeview">
                        <a href="#">
                            <i class="mdi mdi-page-layout-body"></i>
                            <span>پست ها</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="mdi mdi-comment-text"></i>
                            <span>نظرات من</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="mdi mdi-wallet-giftcard"></i>
                            <span>امتیاز من</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="mdi mdi-settings"></i>
                            <span>تنظیمات</span>
                        </a>
                    </li>
                    @if(request()->user()->hasRoles("super-admin"))
                        <div class="d-block m-auto text-center pt-3">
                            <a class="btn btn-primary text-white" href="{{ route("admin.dashboard.index") }}">
                                <i class="mdi mdi-login"></i>
                                <span>پنل ادمین</span>
                            </a>
                        </div>
                    @endif
                </ul>
            </div>
        </div>
    </section>
</aside>
