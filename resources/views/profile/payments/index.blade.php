@extends("layouts.profile.riady")
@section("head")
    <title>سفارشات من</title>
@endsection

@section("content")
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ route("profile.order.index",["status" => config("order-status.delivered")]) }}" class="btn btn-success btn-md mb-2">تحویل داده شده</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.sending")]) }}" class="btn btn-primary btn-md mb-2">درحال ارسال</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.preparing")]) }}" class="btn btn-danger btn-md mb-2">درحال پردازش</a>
                        <a href="{{ route("profile.order.index",["status" => config("order-status.unPaid")]) }}" class="btn btn-warning btn-md mb-2">پرداخت نشده</a>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">هزینه</th>
                                    <th class="text-center">کاربر</th>
                                    <th class="text-center">کد پیگیری</th>
                                    <th class="text-center">توضیحات</th>
                                    <th class="text-center">وضعیت</th>
                                    <th class="text-center">بانک</th>
                                    <th class="text-center">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($payments as $payment)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ number_format($payment->price)." تومان" }}</td>
                                        <td>{{ $payment->user->name." ".$payment->user->family }}</td>
                                        <td>{{ $payment->tracking_code }}</td>
                                        <td>
                                            @if($payment->description)
                                                {{ $payment->description }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            <span
                                                @if($payment->status == config("payment-status.initialize")) class="badge badge-primary"
                                                @elseif($payment->status == config("payment-status.success")) class="badge badge-success"
                                                @elseif($payment->status == config("payment-status.failed")) class="badge badge-danger"
                                            @endif >
                                                {{ $payment->persianStatus }}
                                            </span>
                                        </td>
                                        <td>{{ \Morilog\Jalali\Jalalian::forge($payment->created_at)->format("%d - %m - %Y") }}</td>
                                        <td>
                                            <a href="{{ route("profile.order.detail",["order" => $payment->order->refuse_id]) }}" target="_blank" class="btn btn-primary btn-md">مشاهده سفارش</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9">داده ای برای نمایش وجود ندارد</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
