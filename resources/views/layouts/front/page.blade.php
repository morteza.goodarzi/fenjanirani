<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="icon" type="image/png" href="{{ asset("front/demo4/images/icons/favicon.png") }}">
    <meta name="theme-color" content="#2163e8"/>
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:site_name" content="تحلیل کریپتو">
    @include("front.partials.preload")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/bootstrap-grid.css") }}">
    <link rel="preload" href="{{ asset("front/demo4/fonts/IranYekan.woff2") }}" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{ asset("front/demo4/vendor/fontawesome-free/webfonts/fa-brands-400.woff2") }}" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{ asset("front/demo4/vendor/fontawesome-free/webfonts/fa-solid-900.woff2") }}" as="font" type="font/woff2" crossorigin>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7DBTM3K8SD"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-7DBTM3K8SD');
    </script>
    <script type="text/javascript">
        (function(c,l,a,r,i,t,y){
            c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
            t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
            y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
        })(window, document, "clarity", "script", "cnydfdnk7t");
    </script>
    @yield("header")
</head>
<body class="home">
<div class="page-wrapper rtl">
    @if($page->display_header)
        @include("front.partials.header")
    @endif
    <main class="main">
        @yield("content")
    </main>
        @if($page->display_footer)
            @include("front.partials.footer")
        @endif
</div>

<div class="sticky-footer sticky-content fix-bottom flex-row-reverse">
    <a href={{ route("home") }} class="sticky-link">
        <i class="d-icon-home"></i>
        <span>خانه</span>
    </a>
    <a href="{{ route("category.post.show") }}" class="sticky-link">
        <i class="d-icon-volume"></i>
        <span>بلاگ</span>
    </a>
    <a href="{{ route("shopping-cart.index") }}" class="sticky-link">
        <i class="d-icon-card"></i>
        <span>سبد خرید</span>
    </a>
    @if(\Illuminate\Support\Facades\Auth::check())
        <a href="{{ route("profile.index") }}" class="sticky-link">
            <i class="d-icon-user"></i>
            <span>پنل کاربری</span>
        </a>
    @else
        <a href="{{ route("login.page") }}" class="sticky-link">
            <i class="d-icon-user"></i>
            <span>ورود</span>
        </a>
    @endif
    <div class="header-search hs-toggle dir-up">
        <a href="#" class="search-toggle sticky-link">
            <i class="d-icon-search"></i>
            <span>جستجو</span>
        </a>
        <form action="{{ route("search.post") }}" class="input-wrapper" method="post">
            @csrf
            <input type="text" class="form-control" name="search" autocomplete="off"
                   placeholder="جستجو ..." required />
            <button class="btn btn-search" type="submit">
                <i class="d-icon-search"></i>
            </button>
        </form>
    </div>
</div>

<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

@include("front.partials.mobile-sidebar")

<script src="{{ asset("front/demo4/vendor/jquery/jquery.min.js") }}"></script>
<script src="{{ asset("front/demo4/vendor/elevatezoom/jquery.elevatezoom.min.js") }}"></script>
<script src="{{ asset("front/demo4/vendor/magnific-popup/jquery.magnific-popup.min.js") }}"></script>
<script src="{{ asset("front/demo4/vendor/owl-carousel/owl.carousel.min.js") }}"></script>
<script src="{{ asset("front/demo4/vendor/imagesloaded/imagesloaded.pkgd.min.js") }}"></script>
<script src="{{ asset("front/demo4/vendor/isotope/isotope.pkgd.min.js") }}"></script>
<script src="{{ asset("front/demo4/js/main.js") }}"></script>
@yield("script")

</body>


</html>
