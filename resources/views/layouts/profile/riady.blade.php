<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset("profile-assets/images/favicon.ico") }}">
    <link rel="stylesheet" href="{{ asset("profile-assets/main-rtl/css/vendors_css.css") }}">
    <link rel="stylesheet" href="{{ asset("profile-assets/main-rtl/css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("profile-assets/main-rtl/css/skin_color.css") }}">
    @yield("head")

</head>
<body class="hold-transition light-skin sidebar-mini theme-primary fixed rtl">

<div class="wrapper">
    <div id="loader"></div>
    @include("profile.partials.header")
    @include("profile.partials.main-sidebar")
    <div class="content-wrapper">
        <div class="container-full">
            @yield("content")
        </div>
    </div>
{{--    @include("profile.partials.right-bar");--}}
{{--    @include("profile.partials.footer");--}}
    <div class="control-sidebar-bg"></div>
</div>

{{--@include("profile.partials.sticky-toolbar")--}}
{{--@include("profile.partials.chat")--}}

<script src="{{ asset("profile-assets/main-rtl/js/vendors.min.js") }}"></script>
<script src="{{ asset("profile-assets/main-rtl/js/pages/chat-popup.js") }}"></script>
<script src="{{ asset("profile-assets/assets/vendor_components/apexcharts-bundle/dist/apexcharts.min.js") }}"></script>
<script src="{{ asset("profile-assets/assets/icons/feather-icons/feather.min.js") }}"></script>

<script src="{{ asset("profile-assets/main-rtl/js/template.js") }}"></script>
@yield("script")

</body>
</html>
