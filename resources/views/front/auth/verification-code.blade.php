@extends("layouts.front.riode")
@section("header")
    <title>فنجان ایرانی | ورود</title>
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <style>
        #login-from{
            padding: 3em;
            box-shadow: 0 0 12px -8px black;
            border-bottom: 1px solid #e1e1e1
        }
        .account {
            width: 100%;
            padding: 2em;
            box-shadow: 0 0 12px -8px black;
            margin-top: 5em;
        }

        .cards {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
        }

        .card-header {
            padding: 20px;
            border-bottom: 1px solid #eee;
        }
        .card-header img{
            display: block;
            margin: auto;
            width: 74px;
        }
        .card-header p {
            margin-top: 13px;
            font-size: 13px;
        }

        .card-body {
            padding: 50px;
        }

        .flex-center {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }


        .w-100 {
            width: 100% !important;
        }

        .card-footer {
            border-top: 1px solid #eee;
            justify-content: center;
            align-items: center;
            padding: 20px 50px;
        }

        .activation-code-input {
            display: none;
        }

        .activation-code {
            direction: ltr;
            position: relative;
        }

        .activation-code::before {
            content: "";
            display: block;
            position: absolute;
            bottom: 0;
            right: 0;
            left: 0;
            border-bottom: 2px solid;
            border-color: #ccc;
            transition: opacity 0.3s ease;
        }

        .activation-code > span {
            position: absolute;
            display: block;
            font-size: 13px;
            color: #ccc;
            top: 0;
            right: 0;
            transition: all 0.3s ease;
        }

        .activation-code .activation-code-inputs {
            display: flex;
            /*flex-direction: row;*/
            /*flex-wrap: nowrap;*/
            flex-flow: row nowrap;
        }

        .activation-code .activation-code-inputs input {
            display: flex;
            flex-flow: column nowrap;
            padding: 0;
            border: 0;
            outline: 0;
            min-width: 0;
            line-height: 36px;
            text-align: center;
            align-items: center;
            transition: all 0.3s ease;
            border-bottom: 2px solid;
            border-color: #ccc;
            margin-right: 8px;
            /*background: red;*/
            opacity: 0;
        }

        .activation-code .activation-code-inputs input:last-child {
            margin-right: 0;
        }

        .activation-code.active::before {
            opacity: 0;
        }

        .activation-code.active .activation-code-inputs input {
            opacity: 1 !important;
        }

        .activation-code .activation-code-inputs input:focus {
            border-color: #ec1b23 !important;
        }

        .activation-code.active > span {
            transform: translate(0, -100%);
            line-height: 30px;
            opacity: 0.6;
        }
        .title-code
        {
            text-align: right;
            font-size: 19px !important;
            font-weight: 600;
            line-height: normal;
            margin-bottom: 10px;
            color: #534f4f;
        }
        .code-ver-number
        {
            font-family: "iranSansNormal",sans-serif;
            text-align: right;
            font-size: 12px;
            color: #8d8b8b !important;
        }
        #resend-activate
        {
            font-family: 'iranSansBold';
            font-size: revert;
            color: #34a6b8;
            background: unset;
        }
        .align-footer
        {
            margin-left: auto;
            margin-right: auto;
            display: block;
            margin-top: 20px;
        }
        #p-two-countdown
        {
            line-height: unset !important;
            margin-bottom: 0 !important;
        }
        .center-logo{
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        .all-width{
            width: 100%;
        }

    </style>
@endsection
@section("content")
    <div class="page-content">
        <div class="form-box">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="col col-xl-4 col-lg-6">
                        <form method="POST" action='{{ route("profile.authenticate")}}'>
                            <input type="hidden" value="{{$redirectUrl}}" name="redirect-url">
                            @csrf
                            <div class="account">
                                <div class="cards">
                                    <div class="card-header">
                                        <img src="{{ asset("panel/assets/images/logo/logo-light.png") }}" width="100px" alt="fenjanirani" class="mb-3">
                                    </div>
                                    <div class="card-body">
                                        <div class="row flex-center">
                                            <div class="col">
                                                <p class="title-code">کد تایید را وارد نمایید</p>
                                                <p class="code-ver-number">         کد تایید برای شماره موبایل <i class="mobile-number">{{$mobile}}</i> ارسال گردید
                                                </p>
                                                <input name="user_verification_code" class="activation-code-input w-100 form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-dark btn-rounded btn-checkout all-width">ادامه</button>
                                        <p class="code-ver-number text-center align-footer" id="p-two-countdown">ارسال مجدد کد تا <span class="code-ver-number" id="two-countdown"></span> دیگر</p>
                                        <button class="align-footer" type="button" id="resend-activate" style="display: none">دریافت مجدد کد تایید</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("script")
    <script>
        $(document).ready(function () {
            $(".activation-code-input").activationCodeInput({
                number: 4
            });
            $('.activation-code-inputs input').focus();
        });

        function inputFilter(e) {
            var key = e.keyCode || e.which;

            if (
                (!e.shiftKey && !e.altKey && !e.ctrlKey && key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105) ||
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 37 ||
                key == 39
            ) {
            } else {
                return false;
            }
        }

        jQuery.fn.activationCodeInput = function (options) {
            var defaults = {
                number: 4,
                length: 1
            };
            var settings = $.extend({}, defaults, options);
            // $('#log').append('options = ' + JSON.stringify(options));
            // $('#log1').append('defaults = ' + JSON.stringify(defaults));
            // $('#log2').append('settings = ' + JSON.stringify(settings));

            return this.each(function () {
                var self = $(this);
                var activationCode = $("<div />").addClass("activation-code");
                var placeHolder = self.attr("placeholder");
                // alert(placeHolder);
                activationCode.append($("<span />").text(placeHolder));
                self.replaceWith(activationCode);
                activationCode.append(self);

                var activationCodeInputs = $("<div />").addClass("activation-code-inputs");

                for (var i = 1; i <= settings.number; i++) {
                    activationCodeInputs.append(
                        $("<input />").attr({
                            maxLength: settings.length,
                            onkeydown: "return inputFilter(event)",
                            oncopy: "return false",
                            onpaste: "return false",
                            oncut: "return false",
                            ondrag: "return false",
                            ondrop: "return false"
                        })
                    );
                }

                activationCode.prepend(activationCodeInputs);

                activationCode.on("click touchstart", function (event) {
                    // console.log(event);
                    // console.log(event.type);
                    if (!activationCode.hasClass("active")) {
                        activationCode.addClass("active");
                        setTimeout(function () {
                            activationCode
                                .find(".activation-code-inputs input:first-child")
                                .focus();
                        }, 200);
                    }
                });

                activationCode
                    .find(".activation-code-inputs")
                    .on("keyup input", "input", function (event) {
                        // $(this).css('background','red');
                        if (
                            $(this).val().toString().length == settings.length ||
                            event.keyCode == 39
                        ) {
                            $(this).next().focus();
                            if ($(this).val().toString().length) {
                                $(this).css("border-color", "#ec1b23");
                            }
                        }
                        if (event.keyCode == 8 || event.keyCode == 37) {
                            $(this).prev().focus();
                            if (!$(this).val().toString().length) {
                                $(this).css("border-color", "#ccc");
                            }
                        }
                        var value = "";
                        activationCode.find(".activation-code-inputs input").each(function () {
                            // value = value + $(this).val().toString();
                            value += $(this).val().toString();
                        });
                        self.attr({
                            value: value
                        });
                    });

                $(document).on("click touchstart", function (e) {
                    console.log(e.target);
                    console.log($(e.target).parent());
                    console.log($(e.target).parent().parent());
                    // false true = false
                    // true false = false
                    // false false = false
                    //true true = true
                    if (
                        !$(e.target).parent().is(activationCode) &&
                        !$(e.target).is(activationCode) &&
                        !$(e.target).parent().parent().is(activationCode)
                    ) {
                        var hide = true;

                        activationCode.find(".activation-code-inputs input").each(function () {
                            if ($(this).val().toString().length) {
                                hide = false;
                            }
                        });
                        if (hide) {
                            activationCode.removeClass("active");
                        } else {
                            activationCode.addClass("active");
                        }
                    }
                });
            });
        };


        function countdown( elementName, minutes, seconds )
        {
            var element, endTime, hours, mins, msLeft, time;

            function twoDigits( n )
            {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer()
            {
                msLeft = endTime - (+new Date);
                if ( msLeft < 1000 ) {
                    $('#p-two-countdown').hide();
                    $('#resend-activate').show();
                } else {
                    time = new Date( msLeft );
                    hours = time.getUTCHours();
                    mins = time.getUTCMinutes();
                    element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
                    setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
                }
            }

            element = document.getElementById( elementName );
            endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
            updateTimer();
        }


        countdown( "two-countdown",2, 0 );


        $('#resend-activate').on('click',function()
        {
            let mobile = $('.mobile-number').text();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                dataType: 'json',
                data: {'mobile':mobile},
                url: "https://rahavardkala.com/account/login/resend/verification-code",
                success: function(data){

                    if(data.status === 'ok')
                    {
                        countdown( "two-countdown", 2, 0 );
                        $('#resend-activate').hide();
                        $('#p-two-countdown').show();
                    }
                }
            });
        });


    </script>
@endsection
