@extends("layouts.front.riode")
@section("header")
    <title>فنجان ایرانی | ورود</title>
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <style>
        #login-from{
            padding: 3em;
            box-shadow: 0 0 12px -8px black;
            border-bottom: 1px solid #e1e1e1
        }
    </style>
@endsection
@section("content")
    <div class="page-content">
        <div class="form-box">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <form class="mt-10 text-center" action="{{ route("profile.login") }}" method="post" id="login-from">
                            <input type="hidden" value="{{ $redirectUrl }}" name="redirect-url">
                            @csrf
                            <img src="{{ asset("panel/assets/images/logo/logo-light.png") }}" width="100px" alt="fenjanirani" class="mb-3">
                            <h3 class="login-title">ورود به فنجان ایرانی</h3>
                            <div class="form-group mb-3">
                                <label for="">تلفن همراه</label>
                                <input type="text" autocomplete="off" class="form-control" name="mobile" placeholder="">
                            </div>
                            <div class="form-footer">
                                <div class="form-checkbox my-5">
                                    <input type="checkbox" class="custom-checkbox" id="remember" name="remember">
                                    <label class="form-control-label" for="remember">مرا به خاطر بسپار (هربار رمز وارد نکنم)</label>
                                </div>
                            </div>
                            <button class="btn btn-dark btn-block btn-rounded mb-5" type="submit">ورود</button>
                            <a href="#" class="lost-link text-center my-3 d-block">بازگردانی رمز عبور?</a>

                        </form>
                        <div class="form-choice text-center d-none">
                            <div class="social-links">
                                <a href="#" class="bg-google social-google border-no btn btn-dark rounded-3">
                                    <i class="fab fa-google"></i>
                                    ورود با گوگل
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>


    </div>
</div>
@endsection



