@extends("layouts.front.riode")
@section("header")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    @if($post->seo_title)
        <title>{{ $post->seo_title }}</title>
    @else
        <title>{{ $post->title }}</title>
    @endif
    @if(!$post->indexable)
        <meta name="robots" content="noindex">
    @endif
    @if($post->canonical)
        <link rel=“canonical” href=“{{ $post->canonical }}” />
    @endif
    <meta name="description" content="{{ $post->seo_description }}">
    <meta property="og:title" content="{{ $post->title }}" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ asset(config("upload_image_path.seo-image"))."/lg/".$post->seo_image }}" />
	<style>
		@media(max-width:968px){
			.page-title{
				font-size:1.3em;
				line-height:0.8cm
			}
		}
	</style>
@endsection
@section("content")
    <div class="page-header bg-primary">
        <h1 class="page-title">{{ $post->title }}</h1>
        <ul class="breadcrumb">
            <li><a href="{{ route("home") }}"><i class="d-icon-home"></i></a></li>
            <li class="delimiter">/</li>
            <li>{{ $post->title }}</li>
        </ul>
    </div>
    <div class="page-content">
        <div class="container">
            <section class="mt-10 pt-3">
                <div class="row gutter-lg">
                    <div class="col-lg-9">
                        <article class="post-single">
                            <figure class="post-media">
                                <a href="#">
                                    <img src="{{ asset("post-thumbnails/lg")."/".$post->thumbnail }}" width="880" height="450" alt="post">
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    توسط <span class="post-author">{{ $post->user->name." ".$post->user->family }}</span>
                                    در <span class="post-date">{{ \Morilog\Jalali\Jalalian::forge($post->updated_at)->format("%d - %m - %Y") }}</span>
                                </div>
                                <h4 class="post-title"><a href="#">{{ $post->title }}</a></h4>
                                <div class="post-body mb-7">
                                    {!! $post->content !!}
                                </div>

                                {{--<div class="post-author-detail">
                                    <figure class="author-media">
                                        <a href="#">
                                            <img src="images/blog/comments/1.jpg" alt="avatar" width="50" height="50">
                                        </a>
                                    </figure>
                                    <div class="author-body">
                                        <div class="author-header d-flex justify-content-between align-items-center">
                                            <div>
                                                <span class="author-title">AUTHOR</span>
                                                <h4 class="author-name font-weight-bold mb-0">John Doe</h4>
                                            </div>
                                            <div>
                                                <a href="#" class="author-link font-weight-semi-bold">View all posts
                                                    by John Doe <i class="d-icon-arrow-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="author-content">
                                            <p class="mb-0">Praesent dapibus, neque id cursus faucibus, tortor neque
                                                egestas auguae, eu vulputate magna eros euerat. Aliquam erat
                                                volutpat.</p>
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="post-footer mt-7 mb-3">
                                    <div class="post-tags">
                                        @foreach($post->categories as $item)
                                            <a href="{{ route("category.post.show",["category" => $item->slug]) }}" class="tag">{{ $item->title }}</a>
                                        @endforeach
                                    </div>
                                    {{--<div class="social-icons">
                                        <a href="#" class="social-icon social-facebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#" class="social-icon social-twitter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        <a href="#" class="social-icon social-pinterest" title="Pinterest"><i class="fab fa-pinterest-p"></i></a>
                                    </div>--}}
                                </div>
                            </div>
                        </article>
                        <div class="related-posts">
                            <h3 class="title title-simple text-right text-normal font-weight-bold ls-normal">مطالب مرتبط</h3>
                            <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2" data-owl-options="{
                                    'items': 1,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        }
                                    }
                                }">
                                @foreach($relatedPosts as $relatedPost)
                                    <div class="post">
                                        <figure class="post-media">
                                            <a href="{{ route("post.show",["post" => $relatedPost->slug]) }}">
                                                <img src="{{ asset("post-thumbnails/md")."/".$relatedPost->thumbnail }}" width="380" height="250"
                                                     alt="post" />
                                            </a>
                                            <div class="post-calendar">
                                                <span class="post-day">{{ \Morilog\Jalali\Jalalian::forge($relatedPost->updated_at)->format("%d") }}</span>
                                                <span class="post-month">{{ \Morilog\Jalali\Jalalian::forge($relatedPost->updated_at)->format("%B") }}</span>
                                            </div>
                                        </figure>
                                        <div class="post-details">
                                            <h4 class="post-title"><a href="{{ route("post.show",["post" => $relatedPost->slug]) }}">{{ $relatedPost->title }}</a>
                                            </h4>
                                            <a href="{{ route("post.show",["post" => $relatedPost->slug]) }}"
                                               class="btn btn-link btn-underline btn-primary">ادامه مطلب<i
                                                    class="d-icon-arrow-right"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="comments">
                            <h3 class="title title-simple text-right text-normal font-weight-bold">{{ $post->comments_count }}
                                نظر</h3>
                            <ul>
                                @foreach($post->comments as $comment)
                                    <li>
                                        <div class="comment">
                                            <figure class="comment-media">
                                                <a href="#">
                                                    <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                         alt="avatar">
                                                </a>
                                            </figure>
                                            <div class="comment-body">
                                                <div class="comment-user">
                                                    <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($comment->created_at)->format("%d - %m - %Y") }}</span>
                                                    <h4><a href="#">{{ $comment->user->name }}</a></h4>
                                                </div>
                                                <div class="comment-content mb-2">
                                                    <p>{{ $comment->content }}</p>
                                                </div>
                                                <form action="{{ route("profile.post.comment.store",["post"=>$post->slug]) }}" method="post">
                                                    @csrf
                                                    <a href="#" data-comment-id="{{ $comment->id }}" data-user="{{ $comment->user->name }}" class="btn btn-primary btn-sm mt-3 reply-comment">پاسخ</a>
                                                </form>
                                            </div>
                                        </div>
                                        @if($comment->reply->count()>0)
                                            <ul>
                                                @foreach($comment->reply as $reply)
                                                    <li>
                                                        <div class="comment">
                                                            <figure class="comment-media">
                                                                <a href="#">
                                                                    <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                                         alt="avatar">
                                                                </a>
                                                            </figure>
                                                            <div class="comment-body">
                                                                <div class="comment-user">
                                                                    <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($reply->created_at)->format("%d - %m - %Y") }}</span>
                                                                    <h4><a href="#">{{ $reply->user->name }}</a></h4>
                                                                </div>
                                                                <div class="comment-content mb-2">
                                                                    <p>{{ $reply->content }}.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <form action="{{ route("profile.post.comment.store",["post"=>$post->slug]) }}" method="post">
                                                            @csrf
                                                            <a href="#" data-comment-id="{{ $reply->id }}" data-user="{{ $reply->user->name }}" class="btn btn-primary btn-sm mt-3 reply-comment">پاسخ
                                                            </a>
                                                        </form>
                                                    </li>
                                                    @if($comment->reply->count()>0)
                                                        <ul>
                                                        @foreach($reply->reply as $reply)
                                                            <li class="me-10">
                                                                <div class="comment">
                                                                    <figure class="comment-media">
                                                                        <a href="#">
                                                                            <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                                                 alt="avatar">
                                                                        </a>
                                                                    </figure>
                                                                    <div class="comment-body">
                                                                        <div class="comment-user">
                                                                            <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($reply->created_at)->format("%d - %m - %Y") }}</span>
                                                                            <h4><a href="#">{{ $reply->user->name }}</a></h4>
                                                                        </div>
                                                                        <div class="comment-content mb-2">
                                                                            <p>{{ $reply->content }}.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                        </ul>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="reply">
                            <div class="title-wrapper text-right">
                                <h3 class="title title-simple text-right text-normal mb-5">ثبت نظر</h3>
                            </div>
                            <form action="{{ route("profile.post.comment.store",["post"=>$post->slug]) }}" method="post" id="comment-form">
                                <div class="col-12">
                                    <div class="messages border border-primary p-3 rounded"  style="display:none">

                                    </div>
                                </div>
                                @csrf
                                <textarea name="content" id="content" class="form-control mb-4" placeholder="متن *" required></textarea>
                                <button type="submit" class="btn btn-primary btn-rounded btn-sm btn-submit-comment">ارسال</button>
                            </form>
                        </div>
                    </div>
                    <aside class="col-lg-3 right-sidebar sidebar-fixed sticky-sidebar-wrapper">
                        <div class="sidebar-overlay">
                        </div>
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                        <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                        <div class="sidebar-content">
                            <div class="pin-wrapper" style="height: 1195.38px;"><div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}" style="border-bottom: 0px none rgb(102, 102, 102); width: 272.5px;">
                                    <div class="widget widget-search border-no mb-2">
                                        <form action="#" class="input-wrapper input-wrapper-inline btn-absolute">
                                            <input type="text" class="form-control" name="search" autocomplete="off" placeholder="Search in Blog" required="">
                                            <button class="btn btn-search btn-link" type="submit">
                                                <i class="d-icon-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="widget widget-collapsible border-no">
                                        <h3 class="widget-title">دسته بندی ها<span class="toggle-btn"></span></h3>
                                        <ul class="widget-body filter-items search-ul">
                                            @foreach($categories as $category)
                                            <li><a href="{{ route("category.post.show",["category"=>$category->slug]) }}">{{ $category->title }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="widget widget-collapsible">
                                        <h3 class="widget-title">محصولات ویژه<span class="toggle-btn"></span></h3>
                                        <div class="widget-body">
                                            <div class="post-col">
                                                @foreach($popularProducts as $product)
                                                <div class="post post-list-sm">
                                                    <figure class="post-media">
                                                        <a href="{{ route("products.show",["product" => $product->slug]) }}">
                                                            <img src="{{ asset("product-thumbnails/sm")."/".$product->thumbnail }}" width="90" height="90" alt="post">
                                                        </a>
                                                    </figure>
                                                    <div class="post-details">
                                                        <h4 class="post-title"><a href="{{ route("products.show",["product" => $product->slug]) }}">{{ $product->title }}</a></h4>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </section>
        </div>
    </div>
@endsection
@section("script")
    <script>
        let data_reply_id,data_user,content;
        let submit = $(".btn-submit-comment");
        $(document).on("click",".reply-comment",(function( event ) {
            $('html, body').animate({
                scrollTop: $("#comment-form").offset().top-200
            }, 2000);
            data_reply_id = $(this).attr("data-comment-id");
            data_user = $(this).attr("data-user");
            $(".messages").html("<strong class='text-dark'>"+"پاسخ به  "+ data_user + "</strong>").append('<button class="btn btn-danger btn-sm me-3 mb-0 cancel-reply">انصراف</button>').show();
        }));
        $(document).on("click",".cancel-reply",(function( event ) {
            data_reply_id = null;
            data_user = null;
            $(".messages").html("").hide();
        }));
        $(document).on("submit","#comment-form",(function( event ) {
            content = $("#content").val();
            var formData = {
                content: content,
                reply_id: data_reply_id,
                "_token": "{{ csrf_token() }}",
            };
            console.log(formData)
            // console.log("formData: "+formData);
            event.preventDefault();
            if(content){
                $.ajax({
                    type:'POST',
                    url:"{{ route("profile.post.comment.store",["post"=>$post->slug]) }}",
                    data: formData,
                    success:function(data) {
                        $("#content").val("");
                        submit.attr("disabled",true);
                        $(".messages").html("<h6>نظر شما با موفقیت ثبت شد. تا تایید شدن آن صبر کنید.</h6><h6>لطفا رخواست دیگری ارسال نکنید</h6>").show();
                        data_reply_id = null;
                        data_user = null;
                        submit.html("در حال ثبت ... (لطفا منتظر بمانید)");
                        setTimeout(function (e) {
                            submit.attr("disabled",false);
                            submit.html("Post Comment");
                            $(".messages").html("").hide();
                        },4000)
                    }
                });
            } else {
                $(".messages").html("نظر خود را وارد کنید").show();
            }
        }));
    </script>
@endsection
