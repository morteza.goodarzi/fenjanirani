@extends("layouts.front.page")
@section("header")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    @if($page->seo_title)
        <title>{{ $page->seo_title }}</title>
    @else
        <title>{{ $page->title }}</title>
    @endif
    @if(!$page->indexable)
        <meta name="robots" content="noindex">
    @endif
    @if(!$page->canonical)
        <link rel=“canonical” href=“{{ $page->canonical }}” />
    @endif
    <meta name="description" content="{{ $page->seo_description }}">
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ asset(config("upload_image_path.seo-image"))."/lg/".$page->seo_image }}" />
@endsection
@section("content")
    <div class="page-header bg-primary">
        <h1 class="page-title">{{ $page->title }}</h1>
        <ul class="breadcrumb">
            <li><a href="{{ route("home") }}"><i class="d-icon-home"></i></a></li>
            <li class="delimiter">/</li>
            <li>{{ $page->title }}</li>
        </ul>
    </div>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="content pt-5">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
