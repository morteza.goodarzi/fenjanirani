@extends("layouts.front.riode")
@section("header")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    @if($product->seo_title)
        <title>{{ $product->seo_title }}</title>
    @else
        <title>{{ $product->title }}</title>
    @endif
    @if(!$product->indexable)
        <meta name="robots" content="noindex">
    @endif
    @if(!$product->canonical)
        <link rel=“canonical” href=“{{ $product->canonical }}” />
    @endif
    <meta name="description" content="{{ $product->seo_description }}">
    <meta property="og:title" content="{{ $product->title }}" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ asset(config("upload_image_path.seo-image"))."/lg/".$product->seo_image }}" />
@endsection
@section("content")
    <div class="page-content mt-6">
        <div class="container">
            <div class="product product-single row mb-5">
                <div class="col-md-6 sticky-sidebar-wrapper">
                    <div class="product-gallery sticky-sidebar" data-sticky-options="{'minWidth': 767}">
                        <div
                            class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1 gutter-no">
                            @foreach($product->galleries as $gallery)
                            <figure class="product-image">
                                <img src="{{ asset("galleries")."/".$gallery->path }}"
                                     data-zoom-image="{{ asset("galleries")."/".$gallery->path }}"
                                     alt="{{ $product->title }}" width="800" height="900">
                            </figure>
                            @endforeach
                        </div>
                        <div class="product-thumbs-wrap">
                            <div class="product-thumbs">
                                @foreach($product->galleries as $gallery)
                                    <div class="product-thumb active">
                                        <img src="{{ asset("galleries")."/".$gallery->path }}"
                                             alt="{{ $product->title }}"
                                             width="150" height="169">
                                    </div>
                                @endforeach
                            </div>
                            <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                            <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-details">
                        <div class="product-navigation">
                            <ul class="breadcrumb breadcrumb-lg">
                                <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                                <li><a href="#" class="active">فروشگاه</a></li>
                                <li>{{ $product->title }}</li>
                            </ul>
                        </div>

                        <h1 class="product-name">{{ $product->title }}</h1>
                        @if($product->exist == "موجود")
                            <div id="product-price"
                                 class="product-price">{{ number_format($product->price)." تومان" }}</div>
                        @else
                            <div class="product-price">ناموجود</div>
                        @endif
                        <p class="product-short-desc">
                            {!! $product->overview !!}
                        </p>
                        <form action="{{ route("shopping-cart.add",["product" => $product->slug]) }}" method="post">
                            @csrf
                            @if($product->exist != "ناموجود")
                                @php $loopCount=count($details) @endphp
                                @if($details->count() > 0)
                                    <h6>انتخاب کنید</h6>
                                @endif
                                @foreach($details as $index => $detail)
                                    @php
                                        $next = $details->get(++$index);
                                    @endphp
                                    @if($loop->iteration == 1)
                                        <div class="product-form product-variations product-color">
                                            <label>{{ $detail->value->key->name }}</label>
                                            <div class="select-box">
                                                <select name="option[]" class="form-control" >
                                                    <option value="-1" selected disabled>لطفا انتخاب کنید</option>
                                                    <option data-price="{{ $detail->price }}"
                                                            value="{{ $detail->value->id }}">{{ $detail->value->value }}</option>
                                                    @else
                                                        @if($detail->value->product_key_id == $prev->value->product_key_id)
                                                            <option data-price="{{ $detail->price }}"
                                                                    value="{{ $detail->value->id }}">{{ $detail->value->value }}</option>
                                                        @else
                                                </select>
                                            </div>
                                        </div>
                                        <div class="product-form product-variations">
                                            <label>{{ $detail->value->key->name }}</label>
                                            <div class="select-box">
                                                <select name="option[]" class="form-control">
                                                    <option value="-1" selected disabled>لطفا انتخاب کنید</option>
                                                    <option data-price="{{ $detail->price }}"
                                                            value="{{ $detail->value->id }}">{{ $detail->value->value }}</option>
                                                    @endif
                                                    @endif
                                                    @if($loopCount == $loop->iteration)
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @php
                                        $prev = $detail;
                                    @endphp
                                @endforeach
                            @endif
                            <hr class="product-divider">
                            @if($product->exist == "موجود")
                                <div class="product-form product-qty">
                                    <div class="product-form-group">
                                        <button type="submit" class="btn btn-primary text-normal ls-normal font-weight-semi-bold" id="add-to-cart">
                                            <i class="d-icon-bag ml-1"></i>
                                            افزودن به سبد خرید
                                        </button>
                                    </div>
                                </div>
                            @else
                                <a href="#" class="btn btn-light">
                                    <i class="fa fa-bell"></i>
                                    موجود شد به من اطلاع بده
                                </a>
                            @endif
                        </form>


                        <hr class="product-divider mb-3">

                        <div class="product-footer">
                            <div class="social-links ml-4">
                                <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                                <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                                <a href="#" class="social-link social-pinterest fab fa-pinterest-p"></a>
                            </div>
                            <span class="divider d-lg-show"></span>
                            <div class="product-action">
                                <a href="#" class="btn-product btn-wishlist ml-6">
                                    <i class="d-icon-heart ml-1"></i>
                                    افزودن به علاقه مندی
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab tab-nav-simple product-tabs">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#product-tab-description">توضیحات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#product-tab-reviews">نظرات</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active in" id="product-tab-description">
                        <div class="row mt-6">
                            <div class="col-md-12">
                                {!! $product->expert_review !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="product-tab-reviews">
                        <div class="row">
                            <div class="col-lg-12 comments pt-2 pb-10 border-no">
                                <ul class="comments-list">
                                    @foreach($product->comments as $comment)
                                        <li>
                                            <div class="comment">
                                                <figure class="comment-media">
                                                    <a href="#">
                                                        <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                             alt="avatar">
                                                    </a>
                                                </figure>
                                                <div class="comment-body">
                                                    <div class="comment-user">
                                                        <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($comment->created_at)->format("%d - %m - %Y") }}</span>
                                                        <h4><a href="#">{{ $comment->user->name }}</a></h4>
                                                    </div>
                                                    <div class="comment-content mb-2">
                                                        <p>{{ $comment->content }}</p>
                                                    </div>
                                                    <a href="#" data-comment-id="{{ $comment->id }}" data-user="{{ $comment->user->name }}" class="btn btn-primary btn-sm mt-3 reply-comment">پاسخ</a>
                                                </div>
                                            </div>
                                            @if($comment->reply->count()>0)
                                                <ul>
                                                    @foreach($comment->reply as $reply)
                                                        <li>
                                                            <div class="comment">
                                                                <figure class="comment-media">
                                                                    <a href="#">
                                                                        <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                                             alt="avatar">
                                                                    </a>
                                                                </figure>
                                                                <div class="comment-body">
                                                                    <div class="comment-user">
                                                                        <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($reply->created_at)->format("%d - %m - %Y") }}</span>
                                                                        <h4><a href="#">{{ $reply->user->name }}</a></h4>
                                                                    </div>
                                                                    <div class="comment-content mb-2">
                                                                        <p>{{ $reply->content }}.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a href="#" data-comment-id="{{ $reply->id }}" data-user="{{ $reply->user->name }}" class="btn btn-primary btn-sm mt-3 reply-comment">پاسخ
                                                            </a>
                                                        </li>
                                                        @if($comment->reply->count()>0)
                                                            <ul>
                                                                @foreach($reply->reply as $reply)
                                                                    <li class="me-10">
                                                                        <div class="comment">
                                                                            <figure class="comment-media">
                                                                                <a href="#">
                                                                                    <img src="{{ asset("profile-assets/images/avatar/6.jpg") }}"
                                                                                         alt="avatar">
                                                                                </a>
                                                                            </figure>
                                                                            <div class="comment-body">
                                                                                <div class="comment-user">
                                                                                    <span class="comment-date">{{ \Morilog\Jalali\Jalalian::forge($reply->created_at)->format("%d - %m - %Y") }}</span>
                                                                                    <h4><a href="#">{{ $reply->user->name }}</a></h4>
                                                                                </div>
                                                                                <div class="comment-content mb-2">
                                                                                    <p>{{ $reply->content }}.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="reply col-12">
                                <div class="title-wrapper text-right">
                                    <h3 class="title title-simple text-right text-normal mb-5">ثبت نظر</h3>
                                </div>
                                <form action="{{ route("profile.product.comment.store",["product"=>$product->slug]) }}" method="post" id="comment-form">
                                    <div class="col-12">
                                        <div class="messages border border-primary p-3 rounded"  style="display:none">

                                        </div>
                                    </div>
                                    @csrf
                                    <textarea name="content" id="content" class="form-control mb-4" placeholder="متن *" required></textarea>
                                    <button type="submit" class="btn btn-primary btn-rounded btn-sm btn-submit-comment">ارسال</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="pt-3 mt-10">
                <h2 class="title justify-content-center">محصولات مرتبط</h2>

                <div class="owl-carousel owl-theme owl-nav-full row cols-2 cols-md-3 cols-lg-4"
                     data-owl-options="{
							'items': 5,
							'nav': false,
							'loop': false,
                            'rtl': true,
							'dots': true,
							'margin': 20,
							'responsive': {
								'0': {
									'items': 2
								},
								'768': {
									'items': 3
								},
								'992': {
									'items': 4,
									'dots': false,
									'nav': true
								}
							}
						}">
                    @foreach($relatedProducts as $related)
                    <div class="product">
                        <figure class="product-media">
                            <a href="{{ route("products.show",["product" => $related->slug]) }}">
                                <img src="{{ asset("product-thumbnails/md")."/".$related->thumbnail }}" alt="product" width="280" height="315">
                            </a>
                            <div class="product-action">
                                <a href="{{ route("products.show",["product" => $related->slug]) }}" class="btn-product">نمایش</a>
                            </div>
                        </figure>
                        <div class="product-details text-right">
                            @if($related->categories->count() > 0)
                                <div class="product-cat">
                                    <a href="#">{{ $related->categories[0]->title }}</a>
                                </div>
                            @endif
                            <h3 class="product-name">
                                <a href="{{ route("products.show",["product" => $related->slug]) }}">{{ $related->title }}</a>
                            </h3>
                            <div class="product-price">
                                <span class="price">{{ number_format($related->price)." تومان" }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </section>
        </div>
    </div>
@endsection
@section("script")
    <script>
		let selectCount = 0;
		$(document).ready(function (){
            $('#add-to-cart').prop('disabled', "disabled");
            selectCount = $(".product-details select").length;
            if(selectCount == 0){
                $("#add-to-cart").attr("disabled",false)
            }
 		});
        // let selectCount = $(".product-details select").length;
        let price = 0;
        $(document).on("change", ".product-details select", (function (event) {
			selectedCount = $(".product-details select>option:not([value='-1']):selected").length;
			if(parseInt(selectedCount) === parseInt(selectCount)){
				$("#add-to-cart").attr("disabled",false)
			}
            price = $('option:selected', this).attr("data-price");
            if(price != 0){
                $("#product-price").html(addCommas(price) + " تومان");
            }
        }));

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
        let data_reply_id,data_user,content;
        let submit = $(".btn-submit-comment");
        $(document).on("click",".reply-comment",(function( event ) {
            $('html, body').animate({
                scrollTop: $("#comment-form").offset().top-200
            }, 2000);
            data_reply_id = $(this).attr("data-comment-id");
            data_user = $(this).attr("data-user");
            $(".messages").html("<strong class='text-dark'>"+"پاسخ به  "+ data_user + "</strong>").append('<button class="btn btn-danger btn-sm me-3 mb-0 cancel-reply">انصراف</button>').show();
        }));
        $(document).on("click",".cancel-reply",(function( event ) {
            data_reply_id = null;
            data_user = null;
            $(".messages").html("").hide();
        }));
        $(document).on("submit","#comment-form",(function( event ) {
            content = $("#content").val();
            var formData = {
                content: content,
                reply_id: data_reply_id,
                "_token": "{{ csrf_token() }}",
            };
            console.log(formData)
            // console.log("formData: "+formData);
            event.preventDefault();
            if(content){
                $.ajax({
                    type:'POST',
                    url:"{{ route("profile.product.comment.store",["product"=>$product->slug]) }}",
                    data: formData,
                    success:function(data) {
                        $("#content").val("");
                        submit.attr("disabled",true);
                        $(".messages").html("<h6>نظر شما با موفقیت ثبت شد. تا تایید شدن آن صبر کنید.</h6><h6>لطفا درخواست دیگری ارسال نکنید</h6>").show();
                        data_reply_id = null;
                        data_user = null;
                        submit.html("در حال ثبت ... (لطفا منتظر بمانید)");
                        setTimeout(function (e) {
                            submit.attr("disabled",false);
                            submit.html("ارسال");
                            $(".messages").html("").hide();
                        },4000)
                    }
                });
            } else {
                $(".messages").html("نظر خود را وارد کنید").show();
            }
        }));
    </script>
@endsection

