<section class="instagram-section pb-8 my-4 appear-animate">
    <div class="container">
        <h2 class="title title-simple text-normal title-center mb-6">ما را در اینستاگرام دنبال کنید</h2>
        <div class="row grid">
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #ced0cf;"
                        data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-1.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #dddcda;"
                        data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.2s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-2.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #eaebe9;"
                        data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.2s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-3.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #b4b4b6;"
                        data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.3s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-4.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-4 col-sm-8 height-x2">
                <div class="testimonial-wrapper">
                    <h3 class="title title-simple font-weight-semi-bold text-normal">نظرات مشتریان در باره فنجان ایرانی</h3>
                    <div class="owl-carousel owl-theme row cols-1 appear-animate"
                         data-animation-options="{
                                        'delay': '.2s'
                                    }" data-owl-options="{
                                        'items': 1,
                                        'nav': false,
                                        'dots': true,
                                        'loop': false
                                    }">
                        <div class="testimonial testimonial-centered">
                            <div class="testimonial-info">
                                <figure class="testimonial-author-thumbnail">
                                    <img src="{{ asset("avatar/avatar.png") }}" alt="user" width="40" height="40" />
                                </figure>
                                <blockquote class="comment">
                                    “ من بیشتر از یک ساله که با فنجان ایرانی آشنا شدم و مشتری ثابت این فروشگاه هستم. هم قیمت ها مناسبه هم کیفیت قهوه هاشون عالیه واقعا. ”
                                </blockquote>
                                <cite>
                                    محسن
                                </cite>
                            </div>
                        </div>
                        <div class="testimonial testimonial-centered">
                            <div class="testimonial-info">
                                <figure class="testimonial-author-thumbnail">
                                    <img src="{{ asset("avatar/avatar.png") }}" alt="user" width="40" height="40" />
                                </figure>
                                <blockquote class="comment">“ ما یکی از کافی شاپ های غرب تهران هستیم که مواد اولیه و دانه های قهوه مون رو از فنجان ایرانی تهیه می کنیم. به جرات میتونم بگم بالاترین کیفیت قهوه و رست واقعا تخصصی رو دارن ارائه می کنن ”</blockquote>
                                <cite>
                                    کیمیا
                                </cite>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #3b414d;"
                        data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-4.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #d1cecb;"
                        data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.2s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-3.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #e3e7ea;"
                        data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.2s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-2.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-item col-lg-2 col-sm-4 col-6 height-x1">
                <figure class="instagram appear-animate" style="background-color: #162021;"
                        data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.3s'
                                }">
                    <a target="_blank" href="https://instagram.com/fenjane_irani">
                        <img src="{{ asset("front/demo4/images/instagram/coffee-instagram-1.jpg") }}" alt="Instagram" width="180"
                             height="180" />
                    </a>
                </figure>
            </div>
            <div class="grid-space col-1"></div>
        </div>
    </div>
</section>
