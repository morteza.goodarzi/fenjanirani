<section class="mt-10 pt-1 appear-animate">
    <div class="container">
        <h2 class="title title-line title-underline with-link pt-4">پست های جدید<a href="#">بیشتر<i
                    class="d-icon-arrow-left mr-1"></i></a></h2>
        <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2 cols-1" data-owl-options="{
                            'items': 3,
                            'margin': 20,
                            'loop': false,
                            'nav': false,
                            'dots': true,
                            'responsive': {
                                '0': {
                                    'items': 1
                                },
                                '576': {
                                    'items': 2
                                },
                                '992': {
                                    'items': 3,
                                    'dots': false
                                }
                            }
                        }">
            @foreach($latestPosts as $post)
                <div class="post post-sm overlay-dark">
                    <figure class="post-media">
                        <a href="{{ route("post.show",["post" => $post->slug]) }}">
                            <img src="{{ asset("post-thumbnails/md")."/".$post->thumbnail }}" width="380" height="200" alt="post" />
                        </a>
                        <span class="overlay-category">@if($post->categories->count() > 0 ) {{ $post->categories[0]->title }} @else - @endif</span>
                    </figure>
                    <div class="post-details">
                        <div class="post-meta">
                            در <a href="#" class="post-date">{{ $post->created_at }}</a>
                            | <a href="#" class="post-comment"><span>{{ $post->comments_count }}</span>نظر</a>
                        </div>
                        <h4 class="post-title"><a href="{{ route("post.show",["post" => $post->slug]) }}">{{ $post->title }}</a></h4>
                        <a href="{{ route("post.show",["post" => $post->slug]) }}" class="btn btn-link btn-underline btn-underline">ادامه مطلب<i class="d-icon-arrow-left"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
