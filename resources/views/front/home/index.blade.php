@extends("layouts.front.riode")
@section("header")
    <title>فروشگاه اینترنتی فنجان ایرانی</title>
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="فروشگاه فنجان ایرانی، بزرگترین عرضه کننده قهوه در ایران، تمام مراحل تهیه قهوه از مزرعه تا روی میز مشتری توسط فروشگاه قنجان ایرانی انجام می شود.">
    <meta name="author" content="فنجان ایرانی">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://fenjanirani.com" />
    <meta property="og:image" content="https://fenjanirani.com" />
    <meta property="og:description" content="فروشگاه فنجان ایرانی، بزرگترین عرضه کننده قهوه در ایران، تمام مراحل تهیه قهوه از مزرعه تا روی میز مشتری توسط فروشگاه قنجان ایرانی انجام می شود.">
    <meta property="og:title" content="فروشگاه اینترنتی فنجان ایرانی" />
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <style>header{border-bottom:none}</style>
@endsection
@section("content")
    <div class="page-content">
        @include("front.home.introduction")
        @include("front.home.products")
{{--        @include("front.home.categories")--}}
{{--        @include("front.home.video-banner")--}}
        @include("front.home.posts")
        @include("front.home.instagram")
    </div>
@endsection
