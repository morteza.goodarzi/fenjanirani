<section class="banner video-banner"
         style="background-image: url({{ asset("front/demo4/images/categories/banner-2.png") }}); background-color: #7ca7bf;">
    <div class="banner-content ml-3 mr-3 text-uppercase text-center appear-animate"
         data-animation-options="{
                        'name': 'fadeInUpShorter',
                        'delay': '.3s'
                    }">
        <h4 class="banner-subtitle ls-s text-white mb-0">مجموعه فنجان ایرانی</h4>
        <h3 class="banner-title mb-4 font-weight-bold text-white">با ۵ شعبه فعال در تهران</h3>
        <a class="btn-play btn-iframe d-inline-flex justify-content-center align-items-center"
           href="video/memory-of-a-woman.mp4" title="play">
            <i class="d-icon-play-solid"></i>
        </a>
    </div>
</section>
