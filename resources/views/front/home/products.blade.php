<section class="mt-10 appear-animate">
    <div class="container">
        <h2 class="title title-line title-underline with-link">جدیدترین محصولات<a href="#">
                بیشتر
                <i class="d-icon-arrow-left mr-1"></i>
            </a>
        </h2>

        <div class="owl-carousel owl-theme row owl-nav-full cols-2 cols-md-3 cols-lg-4"
             data-owl-options="{
                            'items': 5,
                            'nav': false,
                            'loop': false,
                            'dots': true,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                },
                                '992': {
                                    'items': 4,
                                    'dots': false,
                                    'nav': true
                                }
                            }
                        }">
            @foreach($latestProducts as $product)
                <div class="product">
                    <figure class="product-media">
                        <a href="{{ route("products.show",["product" => $product->slug]) }}" >
                            <img src="{{ asset("product-thumbnails/md")."/".$product->thumbnail }}" alt="Blue Pinafore Denim Dress"
                                 width="280" height="315">
                        </a>
                        <div class="product-label-group">
                            <label class="product-label label-new">جدید</label>
                        </div>
                        <div class="product-action-vertical">
                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                               data-target="#addCartModal" title="Add to cart"><i
                                    class="d-icon-bag"></i></a>
                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                    class="d-icon-heart"></i></a>
                        </div>
                        <div class="product-action">
                            <a href="#" class="btn-product ">مشاهده</a>
                        </div>
                    </figure>
                    <div class="product-details">
                        <div class="product-cat">
                            @if($product->categories->count() > 0)
                                <a href="{{ route("category.product.show",["product" => $product->categories[0]->slug]) }}"> {{ $product->categories[0]->title }} </a>
                            @else
                                -
                            @endif
                        </div>
                        <h3 class="product-name">
                            <a href="{{ route("products.show",["product" => $product->slug]) }}">{{ $product->title }}</a>
                        </h3>
                        <div class="product-price">
                            <span class="price">{{ number_format($product->price)." تومان" }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
