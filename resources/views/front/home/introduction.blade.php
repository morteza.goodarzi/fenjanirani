<section class="intro-section">
    <div class="container">
        <div class="grid row">
            <div class="grid-item col-lg-6 col-md-8 height-x2">
                <div class="intro-slider owl-carousel owl-theme owl-full-height owl-dot-inner owl-dot-white owl-full-height row ltr animation-slider cols-1 gutter-no appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.3s'
                                }" data-owl-options="{
                                    'items': 1,
                                    'nav': false,
                                    'loop': true,
                                    'dots': true,
                                    'autoplay': false,
                                    'animateOut': 'fadeOutLeft'
                                }">
                    <div
                        class="intro-slide intro-slide1 banner banner-radius banner-fixed overlay-dark">
                        <figure>
                            <img src="{{ asset("front/demo4/images/categories/banner-1.jpg") }}" width="580" height="510"
                                 alt="banner" style="background-color: #232024;" />
                            <span class="box-border"></span>
                        </figure>
                        <div class="banner-content y-50">
                            <h4 class="banner-subtitle font-weight-bold text-primary ls-1 slide-animate"
                                data-animation-options="{
                                                'name': 'fadeInRightShorter',
                                                'duration': '1s'
                                            }">فروش عمده</h4>
                            <h3 class="banner-title text-white ls-s font-weight-bold slide-animate"
                                data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.5s',
                                                'duration': '1s'
                                            }">دانه های قهوه</h3>
                            <a href="{{ route("category.product.show",["product" => "دانه-قهوه"]) }}"
                               class="btn btn-link btn-underline btn-white slide-animate"
                               data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '1s',
                                                'duration': '1s'
                                            }"><i class="d-icon-arrow-left"></i>مشاهده</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-6 height-x1">
                <div class="category category-absolute text-dark overlay-light overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }" style="background-color: #fbfbfb">
                    <a href="{{ route("category.product.show",["product" => "turkish-coffee"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/categories/turkish-coffee.jpg") }}" alt="category" width="280"
                                 height="245" />
                            <span class="box-border"></span>
                        </figure>
                    </a>
                    <div class="category-content">
                        <h4 class="category-name"><a href="{{ route("category.product.show",["product" => "turkish-coffee"]) }}">قهوه ترک</a></h4>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-6 height-x1">
                <div class="category category-absolute text-dark overlay-light overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }" style="background-color: #f5f5f5">
                    <a href="{{ route("category.product.show",["product" => "لوازم-جانبی-قهوه"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/categories/3998956.jpg") }}" alt="category" width="280"
                                 height="245" />
                            <span class="box-border"></span>
                        </figure>
                    </a>
                    <div class="category-content">
                        <h4 class="category-name"><a href="{{ route("category.product.show",["product" => "لوازم-جانبی-قهوه"]) }}">لوازم جانبی قهوه</a></h4>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-6 height-x1">
                <div class="category category-absolute text-white overlay-dark overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }" style="background-color: #191d25">
                    <a href="{{ route("category.product.show",["product" => "خرید-قهوه-مناسب-ورزشکاران"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/categories/3998956.jpg") }}" alt="category" width="280"
                                 height="245" />
                            <span class="box-border"></span>
                        </figure>
                    </a>
                    <div class="category-content">
                        <h4 class="category-name"><a href="{{ route("category.product.show",["product" => "خرید-قهوه-مناسب-ورزشکاران"]) }}">قهوه ورزشکاران</a></h4>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-6 height-x1">
                <div class="category category-absolute text-white overlay-dark overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }" style="background-color: #cc8e70">
                    <a href="{{ route("category.product.show",["product" => "اسپرسو"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/categories/espresso.jpg") }}" alt="category" width="280"
                                 height="245" />
                            <span class="box-border"></span>
                        </figure>
                    </a>
                    <div class="category-content">
                        <h4 class="category-name"><a href="{{ route("category.product.show",["product" => "اسپرسو"]) }}">قهوه اسپوسو</a></h4>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-6 col-md-8 height-x1">
                <div class="banner banner-fixed intro-banner1 overlay-light overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.3s'
                                }" style="background-color: #f5f5f5">
                    <figure>
                        <img src="{{ asset("front/demo4/images/products/customize.png") }}" alt="category" width="580"
                             height="245" />
                    </figure>
                    <div class="banner-content text-right y-50">
                        <h4 class="banner-title mb-2 font-weight-bold text-dark">قهوه سفارشی (به زودی)</h4>
                        <p class="mb-0 ls-m">اگر می خواهید میزان درصد روبوستا، عربیکا و کشور سازنده قهوه را <br>خودتان انتخاب کنید، روی لینک زیر کلیک کنید و سفارشتان را ثبت کنید</p>
                        <a href="#" class="btn btn-link btn-underline btn-underline my-4">
                            <i class="d-icon-card"></i>
                             قهوه سفارشی (به زودی)
                        </a>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-xs-6 height-x1">
                <div class="category category-absolute text-dark overlay-light overlay-zoom banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.3s'
                                }" style="background-color: #e2e7f1">
                    <a href="{{ route("category.product.show",["product" => "قهوه-فوری"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/categories/coffee.jpg") }}" alt="category" width="280"
                                 height="245" />
                            <span class="box-border"></span>
                        </figure>
                    </a>
                    <div class="category-content">
                        <h4 class="category-name"><a href="{{ route("category.product.show",["product" => "قهوه-فوری"]) }}">قهوه فوری</a></h4>
                    </div>
                </div>
            </div>
            <div class="grid-item col-lg-3 col-md-4 col-xs-6 height-x1">
                <div class="banner banner-fixed intro-banner2 banner-radius overlay-zoom overlay-dark banner-radius appear-animate"
                     data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.3s'
                                }" style="background-color: #aa4722">
                    <figure>
                        <img src="{{ asset("front/demo4/images/demos/demo4/categories/9.jpg") }}" alt="category" width="280"
                             height="245" />
                        <span class="box-border"></span>
                    </figure>
                    <div class="banner-content w-100 x-50 y-50 text-center pl-2 pr-2">
                        <h4 class="banner-title text-uppercase font-weight-bold text-white ls-s">
                            شرایط نگهداری قهوه
                        </h4>
                        <p class="text-white ls-s mb-0">اگر می خواهید همیشه قهوه با کیفیت بنوشید، این تحقیق علمی را مطالعه کنید.</p>
                        <a class="btn btn-outline-light btn-sm mt-2" href="{{ route("post.show",["post" => "شرایط-نگهداری-قهوه"]) }}">شرایط نگهداری قهوه</a>
                    </div>
                </div>
            </div>
            <div class="grid-space col-1"></div>
        </div>
    </div>
</section>

