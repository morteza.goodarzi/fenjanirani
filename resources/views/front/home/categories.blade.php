<section class="category-section mt-10 pt-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-sm-6 mb-4 h-300">
                <div class="category category-light category-absolute overlay-zoom"
                     style="background-color: #c0c4c8;">
                    <a href="javascript:void(0);">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/products/coffee-maker.webp") }}" alt="category" width="280"
                                 height="320" />
                        </figure>
                    </a>
                    <div class="category-content">
                        <a href="javascript:void(0);"
                           class="btn btn-white font-weight-semi-bold btn-rounded text-capitalize">خرید قهوه ساز (به زودی)</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 mb-4 order-lg-last h-300">
                <div class="category category-light category-absolute overlay-zoom "
                     style="background-color: #1e1e1e;">
                    <a href="{{ route("products.show",["product" => "چای-ماسالا"]) }}">
                        <figure class="category-media">
                            <img src="{{ asset("front/demo4/images/products/chai-masala.webp") }}" alt="category" width="280"
                                 height="245" />
                        </figure>
                    </a>
                    <div class="category-content">
                        <a href="{{ route("products.show",["product" => "چای-ماسالا"]) }}"
                           class="btn btn-white font-weight-semi-bold btn-rounded text-capitalize">خرید چای ماسالا</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 mb-4">
                <div class="banner banner-fixed category-banner overlay-effect4 text-center"
                     style="background-color: #e6e7e7;">
                    <figure>
                        <img src="{{ asset("front/demo4/images/demos/demo4/categories/9.jpg") }}" alt="category" width="280"
                             height="245" />
                    </figure>
                    <div class="banner-content p-3 x-50 y-50 w-100">
                        <h4 class="banner-subtitle text-white font-weight-bold mb-2">ارسال رایگان
                        </h4>
                        <h3 class="banner-title text-primary font-weight-bold text-uppercase">تا یک ماه</h3>
                        <h5 class="text-white font-weight-normal ls-normal text-uppercase">با عضویت در سایت تا یک ماه همه خریدهای شما رایگان ارسال خواهد شد.
                        </h5>
                        <a href="{{ route("login.page") }}"
                           class="btn btn-white btn-link btn-underline text-uppercase">ثبت نام</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
