@extends("layouts.front.riode")
@section("header")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/style.min.css") }}">
    <title>سبد خرید</title>
    <style>
        .price{
            font-weight: 700;
            font-size: 1.6rem;
            color: #222;
        }
        .product-price{display: table-cell}
        .product-remove {
            text-align: left;
            font-size: 1.2rem;
            width: 2.3rem;
            height: 2.3rem;
            border: 1px solid #ccc;
            border-radius: 50%;
            background-color: #fff;
            color: #222;
            cursor: pointer;
        }
        .shop-table{
            width: 100%;
        }
        .shop-table thead,.shop-table tbody tr{
            border-bottom:1px solid #e1e1e1;
        }
        .shop-table thead th{
            padding:1.5em 0;
        }
        .shop-table tbody tr td{
            padding: 1em;
        }
        .shop-table tbody tr td:last-child{
            padding-left: 0;
        }
    </style>
@endsection
@section("content")
    <div class="page-content mt-6">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @include("profile.partials.notifications")

                    <div class="table-responsive">
                        <table class="shop-table cart-table table-hover text-center">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">تصویر</th>
                                <th class="text-center">عنوان محصول</th>
                                <th class="text-center">تعداد</th>
                                <th class="text-center">قیمت کل</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($cartCollection as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td class="product-thumbnail">
                                        <figure>
                                            <a href="{{route('products.show',['product'=>$item->attributes->slug])}}">
                                                <img src="{{ asset("product-thumbnails/sm")."/".$item->attributes->thumbnail }}" width="100px" height="100px" alt="{{ $item->attributes->title }}">
                                            </a>
                                        </figure>
                                    </td>
                                    <td class="product-name">
                                        <div class="product-name-section">
                                            <a href="{{route('products.show',['product'=>$item->attributes->slug])}}">{{ $item->name }}</a>
                                            <div class="cart-option">
                                                @if($item->attributes->option !== null)
                                                    @foreach($item->attributes->option as $key => $options)
                                                        <span class="ms-3">{{$options['option_parent_name']}}: {{$options['option_name']}}</span>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td class="product-quantity">
                                        <div class="input-group">
                                            <button class="quantity-minus d-icon-minus"  onclick="chevronNumber('down',{{ $item->id }})"
                                                    onclick="changeNumberBasket({{ $item->id }})"></button>
                                            <input class=" form-control" type="number" min="1"
                                                   id="numberBasket{{ $item->id }}"
                                                   max="20"
                                                   value="{{ $item->quantity }}"
                                                   onchange="changeNumberBasket({{ $item->id }});">
                                            <button class="quantity-plus d-icon-plus"  onclick="chevronNumber('up',{{ $item->id }})"
                                                    onclick="changeNumberBasket({{ $item->id }})"></button>
                                        </div>
                                    </td>
                                    <td class="product-price">
                                        <span class="amount">
                                        <span id="totalSingleProduct{{ $item->id }}">{{ number_format(\Cart::get($item->id)->getPriceSum()) }}
                                            </span> تومان </span>
                                    </td>
                                    <td class="product-close">
                                        <form action="{{ route('shopping-cart.destroy') }}" method="POST"
                                              style="display: contents;">
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{ $item->id }}" id="cart-id" name="cart-id">
                                            <button type="submit" class="product-remove" title="حذف از سبد خرید">
                                                <i class="fas fa-times"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10">
                                        <h5>سبد خرید شما خالی است</h5>
                                        <p>برای مشاهده محصولات می توانید از لینک های زیر اقدام نمایید.</p>
                                        <a href="{{ route("home") }}" class="btn btn-secondary btn-md btn-rounded mr-1">بازگشت به صفحه اصلی</a>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="cart-coupon-box mb-8 ">
                            <form action="{{ route('discount.update') }}" method="POST"
                                  style="display: contents;">
                                {{ csrf_field() }}
                                <h4 class="title coupon-title text-uppercase ls-m">کد تخفیف</h4>
                                <input type="text" name="coupon_code" class="input-text form-control text-grey ls-m mb-4"
                                       id="coupon_code" value="" placeholder="لطفا کد تخفیف را وارد نمایید">
                                <button type="submit" class="btn btn-md btn-dark btn-rounded btn-outline">ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <aside class="col-md-4 sticky-sidebar-wrapper">
                    <div class="sticky-sidebar" data-sticky-options="{'bottom': 20}">
                        <div class="summary mb-4">
                            <table class="shipping">
                                <tr class="summary-subtotal" style="border-bottom: none;">
                                    <td>
                                        <h4 class="summary-subtitle">قیمت کالا ها</h4>
                                    </td>
                                    <td>
                                        <span class="summary-subtotal-price" id="total_amount">{{ number_format (\Cart::getSubTotal())}} </span><span class="price"> تومان </span>
                                    </td>
                                </tr>
                                <tr class="summary-subtotal">
                                    <td>
                                        <h4 class="summary-subtitle">تخفیف کالاها</h4>
                                    </td>
                                    <td>
                                                <span class="summary-subtotal-price" id="total_amount_discount">{{ number_format(\Cart::getSubTotal() - \Cart::getTotal())}}
                                                    </span><span class="price"> تومان </span>
                                    </td>
                                </tr>
                            </table>
                            <table class="total">
                                <tr class="summary-subtotal">
                                    <td>
                                        <h4 class="summary-subtitle">جمع سبد خرید</h4>
                                    </td>
                                    <td>
                                                <span class="summary-total-price ls-s" id="total_amount_after_discount">{{ number_format(\Cart::getTotal()) }}
                                                    </span><span class="price"> تومان </span>
                                    </td>
                                </tr>
                            </table>
                            @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{Route('profile.shopping-cart.checkout')}}"
                                   class="btn btn-primary btn-rounded btn-checkout mt-5 btn-block">ادامه فرآیند
                                    خرید</a>
                            @else
                                <a href="{{Route('login.page',['redirect_url'=> 'checkout'])}}"
                                   class="btn btn-primary btn-rounded btn-checkout mt-5 btn-block">ادامه فرآیند
                                    خرید</a>
                            @endif

                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
@endsection
@section("script")
    <script>
        function chevronNumber(type,num_basket)
        {
            var number = $("#numberBasket"+num_basket).val();
            if (type==="down" && number>1)
            {
                var num_minus = number - 1;
                $("#numberBasket"+num_basket).val(num_minus);
                changeNumberBasket(num_basket)
            }
            if (type==="up" && number<10)
            {
                var num_positive = parseInt(number )+ 1;
                $("#numberBasket"+num_basket).val(num_positive);
                changeNumberBasket(num_basket)
            }
        }
        function changeNumberBasket(rowId) {
            var quantity = $('#numberBasket' + rowId).val();
            var row = rowId;
            $('#loading').css("display","flex");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{route("shopping-cart.update")}}", // Name of the php files
                data: {'quantity': quantity, 'id': row},
                success: function (html) {
                    var total = html[0][row]['quantity'] * html[0][row]['price'];
                    $("#totalSingleProduct" + row).text(separate(total));
                    $("#totalSingleProduct_sidebar" + row).text(separate(total));
                    $("#quantity_sidebar" + row).text(quantity);
                    $("#total_amount_after_discount").text(separate(html[1]));
                    $("#total_amount").text(separate(html[1]));
                    $("#total_amount_sidebar").text(separate(html[1]));
                    $("#total_amount_top").text(separate(html[1]));
                    $("#count_basket").text(html[2]);
                    $("#total_count_top").text(html[2]);
                    $("#total_amount_discount").text(html[3]);
                    $('#loading').css("display","none");

                },
                error: function (reject) {

                }
            });
        }
        function separate(Number) {
            Number += '';
            Number = Number.replace(',', '');
            x = Number.split('.');
            y = x[0];
            z = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(y))
                y = y.replace(rgx, '$1' + ',' + '$2');
            return y + z;
        }
    </script>
@endsection

