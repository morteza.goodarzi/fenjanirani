@extends("layouts.front.riode")
@section("header")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/style.min.css") }}">
    <title>سبد خرید</title>
    <style>
        textarea{height:145px;resize:none}
        .price{
            font-weight: 700;
            font-size: 1.6rem;
            color: #222;
        }
        .product-price{display: table-cell}
        .product-remove {
            text-align: left;
            font-size: 1.2rem;
            width: 2.3rem;
            height: 2.3rem;
            border: 1px solid #ccc;
            border-radius: 50%;
            background-color: #fff;
            color: #222;
            cursor: pointer;
        }
        .shop-table{
            width: 100%;
        }
        .shop-table thead,.shop-table tbody tr{
            border-bottom:1px solid #e1e1e1;
        }
        .shop-table thead th{
            padding:1.5em 0;
        }
        .shop-table tbody tr td{
            padding: 1em;
        }
        .shop-table tbody tr td:last-child{
            padding-left: 0;
        }
    </style>
@endsection
@section("content")
    <div class="page-content mt-6">
        <form action="{{ route("profile.shopping-cart.payment.send") }}" method="post">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        @include("profile.partials.notifications")
                        <h3 class="title title-simple text-right text-uppercase">اطلاعات تحویل گیرنده </h3>
                        <div class="row">
                            <div class="col-xs-6 mb-4">
                                <label for="first-name">نام <b class="red-star">*</b></label>
                                <input type="text" value="{{$user->name}}" class="form-control" name="receiver_name" id="first-name" required=""/>
                            </div>
                            <div class="col-xs-6 mb-4">
                                <label for="last-name">نام خانوادگی <b class="red-star">*</b></label>
                                <input type="text"  value="{{$user->family}}" class="form-control" id="last-name" name="receiver_family" required=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 mb-4">
                                <label for="province">استان <b class="red-star">*</b></label>
                                <select name="province" class="form-control city" id="province">
                                    @foreach($provinces as $province)
                                        <option @if($province->id == $user->province) selected @endif value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6 mb-4">
                                <label>شهر<b class="red-star">*</b></label>
                                <div class="">
                                    <select name="city_id" class="form-control" id="city">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 mb-4">
                                <label for="address-select">آدرس محل تحویل <b class="red-star">*</b></label>
                                @if($address->count() > 0)
                                    <select name="receiver_address" class="form-control" id="address-select">
                                        @foreach($address as $item)
                                            <option value="{{ $item->id }}">{{ $item->address }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <input type="text"  value="{{$user->address}}" class="form-control" id="address-select" name="receiver_address" required=""
                                           placeholder=""/>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <label>کد پستی <b class="red-star">*</b></label>
                                <input type="text" value="{{$user->postal_code}}" class="form-control postal_code" name="receiver_postal_code" required=""/>
                            </div>
                            <div class="col-xs-6">
                                <label>کد ملی <b class="red-star">*</b></label>
                                <input type="text" value="{{$user->national_code}}" class="form-control national_code" name="receiver_national_code" required=""/>
                            </div>
                            <div class="col-12 mt-4">
                                <p>در مورد زمان تحویل کالا، یا توضیحات ویژگی های خاص محصول مدنظر شما، می توانید این قسمت را پر کنید.</p>
                                <label for="description">توضیحات</label>
                                <textarea class="form-control" id="description" name="description" placeholder="توضیحات"></textarea>
                            </div>
                        </div>
                    </div>
                    <aside class="col-md-4 sticky-sidebar-wrapper">
                        <div class="sticky-sidebar" data-sticky-options="{'bottom': 20}">
                            <div class="summary mb-4">
                                <h4 class="">روش پرداخت</h4>
                                <div class="payment accordion radio-type">
                                    <div class="custom-radio mb-6">
                                        <input checked type="radio" name="payment-method" value="1" id="payment-method">
                                        <label for="payment-method">زرین پال (درگاه واسط بانکی)</label>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="mt-4 mb-2">زمان تحویل</h5>
                                <p><strong>تهران: </strong> حداکثر 24 ساعت بعد از سفارش</p>
                                <p><strong>شهرستان ها: </strong> حداکثر 48 ساعت بعد از سفارش</p>
                                <hr>
                                <table class="shipping">
                                    <tr class="summary-subtotal" style="border-bottom: none;">
                                        <td>
                                            <h4 class="summary-subtitle">قیمت کالا ها</h4>
                                        </td>
                                        <td>
                                            <span class="summary-subtotal-price" id="total_amount">{{ number_format (\Cart::getSubTotal())}} </span><span class="price"> تومان </span>
                                        </td>
                                    </tr>
                                    <tr class="summary-subtotal">
                                        <td>
                                            <h4 class="summary-subtitle">تخفیف کالاها</h4>
                                        </td>
                                        <td>
                                                    <span class="summary-subtotal-price" id="total_amount_discount">{{ number_format(\Cart::getSubTotal() - \Cart::getTotal())}}
                                                        </span><span class="price"> تومان </span>
                                        </td>
                                    </tr>
                                </table>
                                <table class="total">
                                    <tr class="summary-subtotal">
                                        <td>
                                            <h4 class="summary-subtitle">مبلغ قابل پرداخت</h4>
                                        </td>
                                        <td>
                                                    <span class="summary-total-price ls-s" id="total_amount_after_discount">{{ number_format(\Cart::getTotal()) }}
                                                        </span><span class="price"> تومان </span>
                                        </td>
                                    </tr>
                                </table>
                                <button type="submit" class="btn btn-primary btn-rounded btn-checkout mt-5 btn-block">ثبت سفارش و پرداخت</button>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("script")
    <script>
        let province = {{ $user->province }};
        $(document).ready(function (){
            $.ajax({
                type:'POST',
                url:"{{ route("profile.cities.ajax") }}",
                beforeSend: function() {
                    $("#loading").show();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "province_id": province
                },
                success:function(data) {
                    $("#loading").hide();
                    $("#city").html(data).find("option[value='{{ $user->city }}']").attr("selected",true);
                }
            });
        });
        $(document).on("change","#province",(function( event ) {
            province = $(this).val();
            $.ajax({
                type:'GET',
                url:"{{ route("profile.cities.ajax") }}",
                beforeSend: function() {
                    $("#loading").show();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "province_id": province
                },
                success:function(data) {
                    $("#loading").hide();
                    $("#city").html(data);
                }
            });
        }));
    </script>
@endsection

