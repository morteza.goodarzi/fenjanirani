<footer class="footer appear-animate">
    <div class="footer-top p-relative pb-4" style="background-image: url({{ asset("front/demo4/images/demos/demo-tea/footer-bg.webp") }});">
        <div class="container">
            <div class="footer-newsletter">
                <div class="row">
                    <div class="col-md-6 mb-6">
                        <div class="newsletter-left text-center">
                            <h3 class="newsletter-title font-weight-bold text-white">درخواست مشاوره
                            </h3>
                            <p class="newsletter-desc font-weight-normal text-white ls-normal">برای ثبت درخواست مشاوره تلفن تماس خود را وارد کنید</p>
                            <form action="{{ route("consultation.store") }}" method="post"
                                  class="input-wrapper input-wrapper-round input-wrapper-inline justify-content-center">
                                @csrf
                                <input type="text"
                                       class="form-control font-primary font-italic form-bold border-no"
                                       name="mobile" id="mobile" placeholder="تلفن تماس" required="">
                                <button class="btn btn-dark border-no ls-m" type="submit">ثبت درخواست مشاوره</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="newsletter-right text-center">
                            <h3 class="newsletter-title font-weight-bold text-white ls-0">سوالات پیش از خرید</h3>
                            <p class="newsletter-desc font-weight-normal text-white ls-normal">با تیم پشتیبانی ما در واتساپ در تماس باشید
                            </p>
                            <a href="#" class="btn btn-dark btn-ellipse border-no ls-m">
                                <span>ارسال پیام در واتساپ</span>
                            </a>
                        </div>
                    </div>
                </div>
                <figure class="img-tea-cup floating p-absolute" data-options="{'inverX':true, 'invertY':true}"
                        data-floating-depth="0.4">
                    <img src="{{ asset("front/demo4/images/demos/demo-tea/cup.webp") }}" class="layer appear-animate" alt="cup" width="494"
                         height="297" data-animation-options="{
                                'name': 'fadeInLeftShorter',
                                'delay': '.5s'
                            }">
                </figure>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="widget">
                                <h4 class="widget-title">خدمات مشتریان</h4>
                                <ul class="widget-body">
                                    <li><a href="{{ route("page.show",["page" => "privacy"]) }}">حریم خصوصی</a></li>
                                    <li><a href="{{ route("page.show",["page" => "privacy"]) }}">پاسخ به پرسش های متداول</a></li>
                                    <li><a href="{{ route("page.show",["page" => "rules"]) }}">رویه های بازگرداندن کالا</a></li>
                                    <li><a href="{{ route("page.show",["page" => "privacy"]) }}">شرایط استفاده</a></li>
                                    <li><a href="#">گزارش باگ</a></li>
                                </ul>
                            </div>
                            <!-- End of Widget -->
                        </div>
                        <div class="col-md-4">
                            <div class="widget">
                                <h4 class="widget-title">لینک های مفید</h4>
                                <ul class="widget-body">
                                    <li><a href="{{ route("category.product.show",["product" => "اسپرسو"]) }}">خرید قهوه اسپرسو</a></li>
                                    <li><a href="{{ route("post.show",["post" => "شرایط-نگهداری-قهوه"]) }}">شرایط نگهداری قهوه</a></li>
                                    <li><a href="{{ route("products.show",["product" => "چای-ماسالا"]) }}">خرید چای ماسالا</a></li>
                                    <li><a href="{{ route("category.product.show",["product" => "خرید-قهوه-مناسب-ورزشکاران"]) }}">خرید قهوه برای ورزشکاران</a></li>
                                    <li><a href="{{ route("category.product.show",["product" => "قهوه-فوری"]) }}">خرید قهوه فوری</a></li>
                                    <li><a href="{{ route("category.post.show",["post" => "the-best-coffee-in-the-world"]) }}">بهترین قهوه های جهان</a></li>
                                </ul>
                            </div>
                            <!-- End of Widget -->
                        </div>
                        <div class="col-md-4">
                            <div class="widget">
                                <h4 class="widget-title">محصولات ویژه</h4>
                                <ul class="widget-body">
                                    <li><a href="{{ route("category.product.show",["product" => "خرید-قهوه-مناسب-برنامه-نویسان"]) }}">خرید قهوه مناسب برنامه نویسان</a></li>
                                    <li><a href="{{ route("products.show",["product" => "موکاپات-استیل-بیالتی"]) }}">خرید موکاپات استیل بیالتی</a></li>
                                    <li><a href="{{ route("products.show",["product" => "قهوه-اسپرسو-فول-کافئین"]) }}">خرید قهوه فول کائین</a></li>
                                    <li><a href="{{ route("products.show",["product" => "قهوه-گلد-آلمان"]) }}">خرید قهوه گلد آلمان</a></li>
                                    <li><a href="{{ route("products.show",["product" => "بانکه-قهوه-طرح-بشکه-نفت"]) }}">خرید بانکه قهوه</a></li>
                                    <li><a href="{{ route("category.product.show",["product" => "turkish-coffee"]) }}">خرید قهوه ترک</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget">
                        <h4 class="widget-title">خبرنامه</h4>
                        <div class="widget-body widget-newsletter">
                            <p>برای دریافت جدیدترین خبرها ایمیل خود را ثبت کنید</p>
                            <form method="post" action="{{ route("newsletter.store") }}" class="input-wrapper input-wrapper-inline">
                                @csrf
                                <input type="email" class="form-control" name="email" id="email"
                                       placeholder="ایمیل ..." required />
                                <button class="btn btn-primary btn-sm btn-rounded btn-icon-right"
                                        type="submit">ثبت</button>
                            </form>
                        </div>
                    </div>
                    <div class=" d-flex align-items-center justify-content-between">
                            <a href="#" class="btn btn-sm btn-instagram">
                                ما را در ایناستاگرام دنبال کنید
                            </a>
                        <figure class="payment">
                            <img src="{{ asset("front/demo4/images/demos/demo4/payment.png") }}" alt="payment" width="135" height="24" />
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container justify-content-center">
            <p class="copyright">تمامی حقوق برای شرکت شاهان گستر مهام ایرانیان (فنجان ایرانی) محفوظ است.</p>
        </div>
    </div>
</footer>
