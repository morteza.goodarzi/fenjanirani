<header class="header">
    <div class="header-middle sticky-header fix-top sticky-content">
        <div class="container">
            <div class="header-left">
                <a href="#" class="mobile-menu-toggle mr-0">
                    <i class="d-icon-bars2"></i>
                </a>
                <a href="{{ route("home") }}" class="logo d-lg-show">
                    <img src="{{ asset("panel/assets/images/logo/logo-light.png") }}" alt="logo" width="154" height="43" />
                </a>
                <!-- End Logo -->

                <nav class="main-nav mr-4">
                    <ul class="menu justify-content-center">
                        @foreach($frontMenus as $menu)
                        <li>
                            <a href="{{ $menu->link }}">{{ $menu->title }}</a>
                            @if($menu->child->count() > 0)
                                <div class="megamenu">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul>
                                                @foreach($menu->child as $child)
                                                    <li><a href="{{ $child->link }}">{{ $child->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                        @endforeach
                        {{--<li>
                            <a href="demo4-shop.html">محصولات</a>
                            <div class="megamenu">
                                <div class="row">
                                    <div class="col-6 col-sm-4 col-md-4 col-lg-3">
                                        <h4 class="menu-title">قهوه فوری</h4>
                                        <ul>
                                            <li><a href="shop-classic-filter.html">قهوه گلد برزیل</a></li>
                                            <li><a href="shop-left-toggle-sidebar.html">قهوه گلد آلمان</a>
                                            </li>
                                            <li><a href="shop-right-toggle-sidebar.html">قهوه گلد ویتنام</a></li>
                                            <li><a href="shop-navigation-filter.html">اسپرسو فوری آلمان</a></li>

                                            <li><a href="shop-off-canvas-filter.html">نسکافه</a>
                                            </li>
                                            <li><a href="shop-horizontal-filter.html">وایت چاکلت</a>
                                            </li>
                                            <li><a href="shop-top-banner.html">هات چاکلت</a></li>
                                            <li><a href="shop-inner-top-banner.html">چای ماسالا</a></li>
                                            <li><a href="shop-with-bottom-block.html">کافی میکس</a></li>
                                            <li><a href="shop-category-in-page-header.html">کاپوچینو</a>
                                        </ul>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4 col-lg-3">
                                        <h4 class="menu-title">قهوه اسپرسو</h4>
                                        <ul>
                                            <li><a href="shop-grid-3cols.html">قهوه اسپرسو کیلویی</a></li>
                                            <li><a href="shop-grid-4cols.html">قهوه اسپرسو کافئین بالا</a></li>
                                            <li><a href="shop-grid-5cols.html">قهوه اسپرسو برای چربی سوزی</a></li>
                                            <li><a href="shop-grid-6cols.html">قهوه اسپرسو ربوستا</a></li>
                                            <li><a href="shop-grid-7cols.html">قهوه اسپرسو عربیکا</a></li>
                                            <li><a href="shop-grid-8cols.html">قهوه اسپرسو میکس فنجان ایرانی</a></li>
                                            <li><a href="shop-list-mode.html">قهوه اسپرسو تلخ</a></li>
                                            <li><a href="shop-pagination.html">قهوه اسپرسو لایت</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4 col-lg-3">
                                        <h4 class="menu-title">لوازم جانبی قهوه</h4>
                                        <ul>
                                            <li><a href="shop-category-grid-shop.html">قهوه ساز خانگی</a>
                                            </li>
                                            <li><a href="shop-category%2bproducts.html">قهوه ساز صنعتی</a>
                                            </li>
                                            <li><a href="shop-default-1.html">فرنچ پرس</a></li>
                                            <li><a href="shop-default-2.html">موکاپات</a>
                                            </li>
                                            <li><a href="shop-default-3.html">ماگ</a>
                                            </li>
                                            <li><a href="shop-default-4.html">تراورماگ</a>
                                            <li><a href="shop-default-4.html">قهوه جوش</a>
                                            </li>
                                            <li><a href="shop-default-5.html">ظرف نگهداری قهوه</a></li>
                                            <li><a href="shop-default-5.html">دله</a></li>
                                            <li><a href="shop-default-5.html">مت تمپر</a></li>
                                        </ul>
                                    </div>
                                    <div
                                        class="col-6 col-sm-4 col-md-4 col-lg-3 menu-banner menu-banner1 banner banner-fixed">
                                        <figure>
                                            <img src="{{ asset("front/demo4/images/shop/coffee.jpg") }}" alt="Menu banner" width="221"
                                                 height="330" />
                                        </figure>
                                    </div>
                                    <!-- End Megamenu -->
                                </div>
                            </div>
                        </li>--}}
                    </ul>
                </nav>
                <span class="divider d-xl-show ml-4"></span>
                <div class="header-search hs-toggle d-xl-show">
                    <a href="#" class="search-toggle" title="search-toggle">
                        <i class="d-icon-search"></i>
                    </a>
                    <form action="{{ route("search.post") }}" class="input-wrapper" method="post">
                        @csrf
                        <input type="text" class="form-control" name="search" autocomplete="off"
                               placeholder="جستجو ..." required />
                        <button class="btn btn-search" type="submit">
                            <i class="d-icon-search"></i>
                        </button>
                    </form>
                </div>
                <!-- End of Header Search -->
            </div>
            <div class="header-center d-flex d-lg-none flex-1 justify-content-center">
                <a href="{{ route("home") }}" class="logo mr-0">
                    <img src="{{ asset("panel/assets/images/logo/logo-light.png") }}" alt="logo" width="154" height="43" />
                </a>
            </div>
            <div class="header-right ltr">
                <div class="dropdown cart-dropdown type2 me-1 me-sm-4 rtl">
                    <a href="{{ route("shopping-cart.index") }}" class="btn cart-link">
                        <i class="d-icon-bag"><span class="cart-count">{{ \Cart::getTotalQuantity()}}</span></i>
                        <span class="d-none d-xl-block mr-1">سبد خرید</span>
                    </a>
                    <div class="dropdown-box rtl">
                        @if(\Cart::getTotalQuantity()>0)
                        <div class="products scrollable">
                            @foreach(\Cart::getContent() as $item)
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="{{route('products.show',['product'=>$item->attributes->slug])}}">
                                        <img src="{{ asset("product-thumbnails/sm")."/".$item->attributes->thumbnail }}" alt="product" width="80"
                                             height="88" />
                                    </a>
                                    <form action="{{ route('shopping-cart.destroy') }}" method="POST"
                                          style="display: contents;">
                                        @csrf
                                        <input type="hidden" value="{{ $item->id }}" id="cart-id" name="cart-id">
                                        <button type="submit" class="btn btn-link btn-close" title="حذف از سبد خرید">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </form>
                                </figure>
                                <div class="product-detail">
                                    <a href="{{route('products.show',['product'=>$item->attributes->slug])}}" class="product-name">{{ $item->name }}</a>
                                    <div class="price-box">
                                        <span class="product-quantity" id="quantity_sidebar{{$item->id}}">{{$item->quantity}}</span>
                                        <span class="product-price" id="totalSingleProduct_sidebar{{$item->id}}">{{number_format($item->price)}}</span><span class="price"> تومان </span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="cart-total">
                            <label>جمع کل:</label>
                            <span class="price" id="total_amount_sidebar">{{ number_format(\Cart::getTotal()) }}&nbsp;</span><span class="price"> تومان </span>
                        </div>
                        <div class="cart-action">
                            <a href="{{ route("shopping-cart.index") }}" class="btn btn-dark btn-link">نمایش سبد خرید</a>
                            <a href="{{ route("profile.shopping-cart.checkout") }}" class="btn btn-dark"><span>تکمیل سفارش</span></a>
                        </div>
                        @else
                            <div class="col-md-12 mb-4">
                                <div class="alert alert-warning alert-simple alert-inline">
                                    <h4 class="alert-title"></h4>
                                    محصولی در سبد خرید وجود ندارد
                                    <button type="button" class="btn btn-link btn-close">
                                        <i class="d-icon-times"></i>
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <a href="{{ route("profile.index") }}" class="btn login-link">
                        <span class="d-none d-xl-block mr-1">پنل کاربری</span>
                        <i class="d-icon-user"></i>
                    </a>
                @else
                    <a href="{{ route("login.page") }}" class="btn login-link">
                        <span class="d-none d-xl-block mr-1">ورود / عضویت</span>
                        <i class="d-icon-user"></i>
                    </a>
                @endif
            </div>
        </div>

    </div>
</header>
