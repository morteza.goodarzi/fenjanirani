<!-- Preload Font -->
<link rel="preload" href="{{ asset("front/demo4/fonts/riode115b.ttf") }}" as="font" type="font/woff2" crossorigin="anonymous">
<link rel="preload" href="{{ asset("front/demo4/vendor/fontawesome-free/webfonts/fa-solid-900.woff2") }}" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ asset("front/demo4/vendor/fontawesome-free/webfonts/fa-brands-400.woff2") }}" as="font" type="font/woff2"
      crossorigin="anonymous">
