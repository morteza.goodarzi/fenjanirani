<div class="mobile-menu-wrapper">
    <div class="mobile-menu-overlay">
    </div>
    <a class="mobile-menu-close" href="#"><i class="d-icon-times"></i></a>
    <div class="mobile-menu-container scrollable">
        <form action="#" class="input-wrapper" method="post">
            @csrf
            <input type="text" class="form-control" name="search" autocomplete="off"
                   placeholder="جستجو" required />
            <button class="btn btn-search" type="submit">
                <i class="d-icon-search"></i>
            </button>
        </form>
        <ul class="mobile-menu mmenu-anim">
            @foreach($frontMenus as $frontMenu)
            <li>
                <a href="{{ $frontMenu->link }}">{{ $frontMenu->title }}</a>
                @if($frontMenu->child->count() > 0)
                    <ul>
                        @foreach($frontMenu->child as $frontChild)
                            <li><a href="{{ $frontChild->link }}">{{ $frontChild->title }}</a></li>
                        @endforeach
                    </ul>
                @endif
            </li>
            @endforeach
        </ul>
    </div>
</div>
