@foreach($products as $product)
    @if($preview == "grid")
        <div class="product-wrap">
            <div class="product">
                <figure class="product-media">
                    <a href="{{ route("products.show",["product"=>$product->slug]) }}">
                        <img src="{{ asset("product-thumbnails/md")."/".$product->thumbnail }}" class="img-product-cat" alt="product">
                    </a>
                    <!--                                    <div class="product-label-group">
                                                            <label class="product-label label-new">جدید</label>
                                                        </div>-->
                    <div class="product-action-vertical">
                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                           data-target="#addCartModal" title="Add to cart"><i
                                    class="d-icon-bag"></i></a>
                        <a href="#" class="btn-product-icon btn-wishlist"
                           title="Add to wishlist"><i class="d-icon-heart"></i></a>
                    </div>
                    <div class="product-action">
                        <a href="{{ route("products.show",["product"=>$product->slug]) }}" class="btn-product ">نمایش</a>
                    </div>
                </figure>
                <div class="product-details">
                    @if(count($product->categories)>0)
                        <div class="product-cat">
                            <a href="{{ route("products.show",["product"=>$product->categories[0]->slug]) }}">{{ $product->categories[0]->title }}</a>
                        </div>
                    @endif
                    <h3 class="product-name">
                        <a href="{{ route("products.show",["product"=>$product->slug]) }}">{{ $product->title }}</a>
                    </h3>
                    <div class="product-price">
                        @if($product->exist)
                            <ins class="new-price">{{ number_format($product->price)." تومان" }}</ins>
                        @else
                            <ins class="new-price no-exist">ناموجود</ins>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-lg-12 main-content">
            <div class="product-lists product-wrapper">
                <div class="product product-list">
                    <figure class="product-media">
                        <a href="{{ route("products.show",["product"=>$product->slug]) }}">
                            <img src="{{ asset("product-thumbnails/md")."/".$product->thumbnail }}" alt="product" class="img-product-cat">
                        </a>
                    </figure>
                    <div class="product-details">
                        @if(count($product->categories)>0)
                            <div class="product-cat">
                                <a href="{{ route("category.product.show",["category"=>$product->categories[0]->slug]) }}">{{ $product->categories[0]->title }}</a>
                            </div>
                        @endif
                        <h3 class="product-name">
                            <a href="{{ route("products.show",["product"=>$product->slug]) }}">{{ $product->name }}</a>
                        </h3>
                        <div class="product-price">
                            @if($product->exist)
                                <span class="price">{{ number_format($product->price)." تومان" }}</span>
                            @else
                                <span class="price no-exist">ناموجود</span>
                            @endif
                        </div>
                        <p class="product-short-desc">
                            {!! strip_tags(mb_substr(html_entity_decode($product->overview),0,500)) !!}
                        </p>
                        <div class="product-action">
                            <a href="{{ route("products.show",["product"=>$product->slug]) }}" class="btn-dark btn btn-sm ml-3"><i class="d-icon-bag"></i>مشاهده محصول</a>
                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach
{{--</div>--}}
