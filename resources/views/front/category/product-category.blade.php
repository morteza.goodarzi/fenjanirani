@extends("layouts.front.riode")
@section("header")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/vendor/nouislider/nouislider.min.css") }}">
    <title>{{ $category->seo_title }}</title>
    <meta name="description" content="{{ $category->seo_description }}">
    @if($category->canonical)
        <link rel=“canonical” href=“{{ $category->canonical }}” />
    @endif
@endsection
@section("content")
    <div class="page-header rtl" style="background-color: #3C63A4;">
        <h3 class="page-subtitle mb-3">دسته بندی های محصولات</h3>
        @if($category)
        <h1 class="page-title">{{ $category->title }}</h1>
        @endif
        <ul class="breadcrumb">
            <li><a href="{{ route("home") }}"><i class="d-icon-home"></i></a></li>
            <li class="delimiter">/</li>
            @if($category)
                <li><a href="javascript:void(0)">دسته بندی های محصولات</a></li>
                <li class="delimiter">/</li>
                <li>{{ $category->title }}</li>
            @else
                <li>دسته بندی های محصولات</li>
            @endif
        </ul>
    </div>
    <div class="page-content mb-10 pb-6">
        <div class="container">
            <div class="row gutter-lg main-content-wrap">
                <aside
                        class="col-lg-3 rtl sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay"></div>
                    <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    <div class="sidebar-content">
                        <div class="sticky-sidebar" data-sticky-options="{'top': 10}">
                            <div class="filter-actions mb-4">
                                <a href="#"
                                   class="sidebar-toggle-btn toggle-remain btn btn-outline btn-primary btn-rounded btn-icon-left d-none d-md-block"><i
                                            class="d-icon-arrow-right"></i>فیلتر</a>
                                <a href="#" class="filter-clean">&nbsp;</a>
                            </div>
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title collapsed">دسته بندی های برتر</h3>
                                <ul class="widget-body filter-items search-ul">
                                    @foreach($topCategories as $topCategory)
                                        <li><i class="float-left">({{ $topCategory->products_count }})</i><a href="{{ route("category.product.show",["product"=>$topCategory->slug]) }}">{{ $topCategory->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title collapsed">فیلتر بر اساس قیمت</h3>
                                <div class="widget-body mt-3">
                                    <form action="#">
                                        <div class="filter-price-slider"></div>
                                        <div class="filter-actions">
                                            <div class="filter-price-text mb-4">قیمت:
                                                <span class="filter-price-range"></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @if(count($brands) > 0)
                            @php $i=0 @endphp
                            <form id="filter-form" action="" method="post">
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title collapsed">برند</h3>
                                    <div class="widget-body">
                                        @csrf
                                        @foreach($brands as $brand)
                                            @php $i++ @endphp
                                            <div class="form-checkbox mb-6">
                                                <input data-id="{{ $brand->id }}" type="checkbox" name="input-value[{{$brand->id}}]" class="custom-checkbox filter-brand" value="{{$brand->id}}" id="brand-value{{$i}}">
                                                <label class="form-control-label ls-s" for="brand-value{{$i}}">{{ $brand->name }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </form>
                            @endif
                            @php $i=0 @endphp
                            <form id="filter-form" action="" method="post">
                            @foreach($category->key as $key)
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title collapsed">{{ $key->name }}</h3>
                                <div class="widget-body">
                                    @csrf
                                    @foreach($key->values as $value)
                                        @php $i++ @endphp
                                        <div class="form-checkbox mb-6">
                                            <input data-parent="{{ $key->id }}" data-id="{{ $value->id }}" type="checkbox" name="input-value[{{$value->id}}]" class="custom-checkbox filter-item" value="{{$value->id}}" id="input-value{{$i}}">
                                            <label class="form-control-label ls-s" for="input-value{{$i}}">{{ $value->value }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                            </form>
                        </div>
                    </div>
                </aside>
                <div class="col-lg-9 main-content rtl">
                    <nav class="toolbox sticky-toolbox sticky-content fix-top">
                        <div class="toolbox-left">
                            <a href="#"
                               class="toolbox-item left-sidebar-toggle btn btn-sm btn-outline btn-primary
											btn-rounded btn-icon-right  d-lg-none">فیلتر<i
                                        class="d-icon-arrow-right"></i></a>
                            <div class="toolbox-item toolbox-sort select-box text-dark">
                                <label>مرتب سازی :</label>
                                <select name="order" class="form-control" id="order">
                                    <option value="latest" selected="selected">جدیدترین</option>
                                    <option value="oldest">قدیمی ترین</option>
                                    <option value="name">نام</option>
                                    <option value="price-asc">قیمت - سعودی</option>
                                    <option value="price-desc">قیمت - نزولی</option>
                                </select>
                            </div>
                        </div>
                        <div class="toolbox-right">
                            <div class="toolbox-item toolbox-show select-box text-dark">
                                <label>نمایش :</label>
                                <select name="count" class="form-control" id="count">
                                    <option value="12">12</option>
                                    <option value="24">24</option>
                                    <option value="36">36</option>
                                </select>
                            </div>
                            <div class="toolbox-item toolbox-layout">
                                <a href="javascript:void(0);" class="d-icon-mode-grid btn-layout active preview-grid ms-1"></a>
                                <a href="javascript:void(0);" class="d-icon-mode-list btn-layout preview-list"></a>
                            </div>
                        </div>
                    </nav>
                    <div class="row cols-2 cols-sm-3 product-wrapper products-area">
                        <span class="filter-loading" style="position:absolute;left: 50%;top: 50%;transform: translate(-50%,-50%);">
                            <i class="fa fa-spin fa-spinner fa-3x text-centers"></i>
                        </span>
                        @include("front.category.products")
                    </div>
                    <div id="pagination">
                        {{$products->links()}}
                    </div>
<!--                    <nav class="toolbox toolbox-pagination">
                        <p class="show-info">Showing 12 of 56 Products</p>
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
                                   aria-disabled="true">
                                    <i class="d-icon-arrow-left"></i>Prev
                                </a>
                            </li>
                            <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
                            <li class="page-item">
                                <a class="page-link page-link-next" href="#" aria-label="Next">
                                    Next<i class="d-icon-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset("front/demo4/vendor/nouislider/nouislider.min.js") }}"></script>
    <script>
        let valueIds=null;
        let brandIds=null;
        let maxPrice=null;
        let minPrice=null;
        let order="latest";
        let page=1;
        let pagination=12;
        let preview="grid";
        $(document).ready(function (){
            $("aside .widget-body").css("display","none");
            $('#pagination a').on('click', function(e){
                e.preventDefault();
                page = $(this).html();
                $(this).parent().parent().find(".active").removeClass("active");
                $(this).parent().addClass("active");
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
                $(this).parent().find(".disabled").removeClass("disabled");
            });
            Riode.priceSlider = function (e, t) {
                "object" == typeof noUiSlider && Riode.$(e).each(function () {
                    var e = this;
                    noUiSlider.create(e, $.extend(!0, {
                        start: [ {{$minPrice}}, {{$maxPrice}}],
                        handles: 1,
                        step: 10,
                        range: {min: {{$minPrice}}, max: {{$maxPrice}}}
                    }, t)), e.noUiSlider.on("update", function (t, i) {
                        t = t.map(function (e) {
                            return parseInt(e)+" تومان";
                        })
                        minPrice = $(".noUi-handle-lower").attr("aria-valuetext");
                        maxPrice = $(".noUi-handle-upper").attr("aria-valuetext");
                        $(e).parent().find(".filter-price-range").text(t.join(" - "));
                        filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
                    })
                })
            }


            $(document).on("change",".filter-item",(function( event ) {
                valueIds = $(".filter-item:checked").map(function(){
                    return $(this).val();
                }).get();
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
                // productData = new FormData(this);
                // productData.append("product_name",product_name.val());
            }));
            $(document).on("click",".preview-list",(function( event ) {
                $(this).addClass("active");
                $(".preview-grid").removeClass("active");
                preview = "list";
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
            }));
            $(document).on("click",".preview-grid",(function( event ) {
                $(this).addClass("active");
                $(".preview-list").removeClass("active");
                preview = "grid";
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
            }));
            $(document).on("change",".filter-brand",(function( event ) {
                brandIds = $(".filter-brand:checked").map(function(){
                    return $(this).val();
                }).get();
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
            }));
            $(document).on("change","#count",(function( event ) {
                pagination = $(this).val();
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
            }));
            $(document).on("change","#order",(function( event ) {
                order = $(this).val();
                filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page);
            }));
        });
        function filter(brandIds,valueIds,minPrice,maxPrice,pagination,order,preview,page){
            let data = {
                    "valueIds":valueIds,
                    "brandIds":brandIds,
                    "minPrice":minPrice,
                    "maxPrice":maxPrice,
                    "order":order,
                    "pagination":pagination,
                    "preview":preview,
                    "page":page,
                };
            $.ajax({
                type:'GET',
                url:"{{route("category.product.show",["product"=>$category->slug])}}",
                beforeSend: function() {
                    $("#loading").show();
                },
                data: data,
                success:function(data) {
                    $("#loading").hide("fast");
                    $(".products-area").html(data.html)
                }
            });
        }
    </script>
@endsection
