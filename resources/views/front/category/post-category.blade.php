@extends("layouts.front.riode")
@section("header")
    <link rel="stylesheet" type="text/css" href="{{ asset("front/demo4/css/demo4.min.css") }}">
    @if($category->title)
        @if($category->seo_title)
            <title>{{ $category->seo_title }}</title>
        @else
            <title>{{ $category->title }}</title>
        @endif
        @if(!$category->indexable)
            <meta name="robots" content="noindex">
        @endif
        @if(!$category->canonical)
            <link rel=“canonical” href=“{{ $category->canonical }}” />
        @endif
        <meta name="description" content="{{ $category->seo_description }}">
        <meta property="og:title" content="{{ $category->title }}" />
        <meta property="og:type" content="category" />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:image" content="{{ asset(config("upload_image_path.seo-image"))."/lg/".$category->seo_image }}" />
    @else
        <title>بلاگ | فنجان ایرانی</title>
    @endif
@endsection
@section("content")
    <div class="page-header bg-primary">
        @if(!$category->title)
            <h1 class="page-title">بلاگ</h1>
        @else
            <h1 class="page-title">{{ $category->title }}</h1>
        @endif
        <ul class="breadcrumb">
            <li><a href="{{ route("home") }}"><i class="d-icon-home"></i></a></li>
            <li class="delimiter">/</li>
            <li>بلاگ</li>
        </ul>
    </div>
    <div class="page-content">
        <div class="container">
            <section class="mt-10 pt-3">
                <div class="row gutter-lg">
                    <div class="col-lg-9">
                        <div class="posts post-grid row">
                            @foreach($posts as $post)
                                <div class="grid-items col-sm-6">
                                    <article class="post mb-3">
                                        <figure class="post-media overlay-zoom">
                                            <a href="{{ route("post.show",["post"=>$post->slug]) }}">
                                                <img src="{{ asset("post-thumbnails/lg")."/".$post->thumbnail }}" width="430" height="300"
                                                     alt="post" />
                                            </a>
                                        </figure>
                                        <div class="post-details">
                                            <div class="post-meta">
                                                <a href="#" class="post-date">{{ \Morilog\Jalali\Jalalian::forge($post->created_at)->format("%Y - %m - %d") }}</a>
                                                | <a href="#" class="post-comment"><span>{{ $post->comments_count }}</span> نظر</a>
                                            </div>
                                            <h4 class="post-title"><a href="{{ route("post.show",["post"=>$post->slug]) }}">{{ $post->title }}</a>
                                            </h4>
                                            <a href="{{ route("post.show",["post"=>$post->slug]) }}"
                                               class="btn btn-link btn-underline btn-primary">بیشتر<i
                                                    class="d-icon-arrow-right"></i></a>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                        {{ $posts->links() }}
                    </div>
                    <aside class="col-lg-3 right-sidebar sidebar-fixed sticky-sidebar-wrapper">
                        <div class="sidebar-overlay">
                            <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                        </div>
                        <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                        <div class="sidebar-content">
                            <div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}">
                                <div class="widget widget-search border-no mb-2">
                                    <form action="{{ route("post.search") }}" method="get" class="input-wrapper input-wrapper-inline btn-absolute">
                                        <input type="text" class="form-control" name="field" autocomplete="off"
                                               placeholder="جستجو" required />
                                        <button class="btn btn-search btn-link" type="submit">
                                            <i class="d-icon-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="widget widget-collapsible border-no">
                                    <h3 class="widget-title">دسته بندی بلاگ</h3>
                                    <ul class="widget-body filter-items search-ul">
                                        @foreach($topCategories as $topCategory)
                                            <li><i class="float-left">({{ $topCategory->posts_count }})</i> <a href="{{ route("category.post.show",["category"=>$topCategory->slug]) }}">{{ $topCategory->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title">پست های ویژه</h3>
                                    <div class="widget-body">
                                        <div class="post-col">
                                            @foreach($recentPosts as $recentPost)
                                                <div class="post post-list-sm">
                                                    <figure class="post-media">
                                                        <a href="{{ route("post.show",["post"=>$recentPost->slug]) }}">
                                                            <img src="{{ asset("post-thumbnails/sm")."/".$recentPost->thumbnail }}" width="90" height="90"
                                                                 alt="post" />
                                                        </a>
                                                    </figure>
                                                    <div class="post-details">
                                                        <h4 class="post-title"><a href="{{ route("post.show",["post"=>$recentPost->slug]) }}">{{ $recentPost->title }}</a></h4>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </section>
        </div>
    </div>
@endsection



