<?php

namespace App\Repositories\Menu;

use App\Models\MenuModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class MenuRepository extends BaseRepository implements MenuRepositoryInterface{

    public Model $model;
    public function __construct( MenuModel $model)
    {
        $this->model = $model;
    }

    public function generateForFront()
    {
        $this->generateCacheKey(__FUNCTION__);
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () {
            return $this->model->whereNull("parent_id")->with("child")->get();
        });
    }
}
