<?php

namespace App\Repositories\Page;

use App\Models\PageModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class PageRepository extends BaseRepository implements PageRepositoryInterface{

    public Model $model;
    public function __construct( PageModel $model)
    {
        $this->model = $model;
    }

}
