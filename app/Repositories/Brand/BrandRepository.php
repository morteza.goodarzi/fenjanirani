<?php

namespace App\Repositories\Brand;

use App\Models\BrandModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface{

    public Model $model;
    public function __construct( BrandModel $model)
    {
        $this->model = $model;
    }

}
