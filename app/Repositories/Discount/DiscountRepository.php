<?php

namespace App\Repositories\Discount;

use App\Models\DiscountModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class DiscountRepository extends BaseRepository implements DiscountRepositoryInterface{

    public Model $model;
    public function __construct( DiscountModel $model)
    {
        $this->model = $model;
    }

}
