<?php

namespace App\Repositories\Ticket;

use App\Models\TicketModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class TicketRepository extends BaseRepository implements TicketRepositoryInterface{

    public Model $model;
    public function __construct( TicketModel $model)
    {
        $this->model = $model;
    }


    public function myTicket(int $userId,int $status = null)
    {
        if($status){
            $this->generateCacheKey(__FUNCTION__);
            return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($status,$userId) {
                return $this->model->where("user_id","=",$userId)->where("status","=",$status)->get();
            });
        }
        $this->generateCacheKey(__FUNCTION__);
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($userId) {
            return $this->model->where("user_id","=",$userId)->get();
        });
    }
}
