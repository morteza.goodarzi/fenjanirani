<?php

namespace App\Repositories\Ticket;

use App\Repositories\Base\BaseRepositoryInterface;

interface TicketRepositoryInterface extends BaseRepositoryInterface {
    public function myTicket(int $userId,int $status = null);
}
