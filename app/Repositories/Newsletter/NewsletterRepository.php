<?php

namespace App\Repositories\Newsletter;

use App\Models\NewsletterModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class NewsletterRepository extends BaseRepository implements NewsletterRepositoryInterface{

    public Model $model;
    public function __construct( NewsletterModel $model)
    {
        $this->model = $model;
    }

}
