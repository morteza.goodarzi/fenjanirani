<?php

namespace App\Repositories\ProductKey;

use App\Models\ProductKeyModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProductKeyRepository extends BaseRepository implements ProductKeyRepositoryInterface{

    public Model $model;
    public function __construct( ProductKeyModel $model)
    {
        $this->model = $model;
    }

}
