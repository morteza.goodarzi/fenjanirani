<?php

namespace App\Repositories\TicketMessage;

use App\Models\TicketMessageModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class TicketMessageRepository extends BaseRepository implements TicketMessageRepositoryInterface{

    public Model $model;
    public function __construct( TicketMessageModel $model)
    {
        $this->model = $model;
    }

}
