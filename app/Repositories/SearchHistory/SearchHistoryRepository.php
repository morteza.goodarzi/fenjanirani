<?php

namespace App\Repositories\SearchHistory;

use App\Models\SearchHistoryModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class SearchHistoryRepository extends BaseRepository implements SearchHistoryRepositoryInterface{

    public Model $model;
    public function __construct( SearchHistoryModel $model)
    {
        $this->model = $model;
    }

    public function create(string $data)
    {
        $search = $this->model->where("search_text",$data)->first();
        if($search instanceof SearchHistoryModel){
            return $search->increment("count");
        }
        return $this->model->create(["search_text" => $data,"count" => 1]);
    }
}
