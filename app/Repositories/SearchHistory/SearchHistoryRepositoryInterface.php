<?php

namespace App\Repositories\SearchHistory;

use App\Repositories\Base\BaseRepositoryInterface;

interface SearchHistoryRepositoryInterface extends BaseRepositoryInterface {
    public function create(string $data);
}
