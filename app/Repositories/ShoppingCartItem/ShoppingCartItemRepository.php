<?php

namespace App\Repositories\ShoppingCartItem;

use App\Models\ShoppingCartItemModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartItemRepository extends BaseRepository implements ShoppingCartItemRepositoryInterface{

    public Model $model;
    public function __construct( ShoppingCartItemModel $model)
    {
        $this->model = $model;
    }

}
