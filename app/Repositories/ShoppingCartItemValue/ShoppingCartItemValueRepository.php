<?php

namespace App\Repositories\ShoppingCartItemValue;

use App\Models\ShoppingCartItemValueModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartItemValueRepository extends BaseRepository implements ShoppingCartItemValueRepositoryInterface{

    public Model $model;
    public function __construct( ShoppingCartItemValueModel $model)
    {
        $this->model = $model;
    }

}
