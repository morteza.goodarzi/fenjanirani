<?php

namespace App\Repositories\ShoppingCart;

use App\Models\ShoppingCartModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartRepository extends BaseRepository implements ShoppingCartRepositoryInterface{

    public Model $model;
    public function __construct( ShoppingCartModel $model)
    {
        $this->model = $model;
    }

}
