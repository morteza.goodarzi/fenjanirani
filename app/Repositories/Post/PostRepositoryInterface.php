<?php

namespace App\Repositories\Post;

use App\Models\CategoryModel;
use App\Models\PostModel;
use App\Repositories\Base\BaseRepositoryInterface;

interface PostRepositoryInterface extends BaseRepositoryInterface {
    public function related(array $categoryIds,PostModel $post,int $count);
    public function category(CategoryModel $category,int $paginate);
    public function special(int $count);
    public function highlight(int $count);
    public function search(string $search,int $count);
}
