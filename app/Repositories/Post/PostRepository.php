<?php

namespace App\Repositories\Post;

use App\Models\CategoryModel;
use App\Models\PostModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class PostRepository extends BaseRepository implements PostRepositoryInterface {

    public Model $model;
    public function __construct(PostModel $model)
    {
        $this->model = $model;
    }

    public function related(array $categoryIds,PostModel $post,int $count)
    {
        $this->generateCacheKey(__FUNCTION__);
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($categoryIds,$post,$count) {
            return $this->model->whereHas("categories",function ($q) use ($categoryIds,$post,$count){
                $q->whereIn("id",$categoryIds);
            })->where("id","!=",$post->id)->latest()->take($count)->get();
        });
    }

    public function category(CategoryModel $category,int $paginate)
    {
        $this->generateCacheKey(__FUNCTION__);
        if($category->title){
            return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($category,$paginate) {
                return $this->model->whereHas("categories",function ($q) use ($category,$paginate){
                    $q->where("id",$category->id);
                })->latest()->paginate($paginate);
            });
        }
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($paginate) {
            return $this->model->latest()->paginate($paginate);
        });
    }

    public function special(int $count)
    {
        $this->generateCacheKey(__FUNCTION__);
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($count) {
            return $this->model->where("is_special","=",1)->take($count)->get();
        });
    }

    public function highlight(int $count)
    {
        $this->generateCacheKey(__FUNCTION__);
        return Cache::remember($this->cacheKey,$this->cacheDuration , function () use ($count) {
            return $this->model->where("highlight","=",1)->take($count)->get();
        });
    }
    public function search(string $search,int $count)
    {
        return $this->model->where("title","like","%".$search."%")->with("categories")->paginate($count);
    }
}
