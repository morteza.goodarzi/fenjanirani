<?php

namespace App\Repositories\ProductDetail;

use App\Models\ProductModel;
use App\Repositories\Base\BaseRepositoryInterface;

interface ProductDetailRepositoryInterface extends BaseRepositoryInterface {
    public function selectable(ProductModel $product);
    public function getProductDetailByValue(array $option, ProductModel $product);
}
