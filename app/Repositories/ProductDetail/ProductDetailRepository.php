<?php

namespace App\Repositories\ProductDetail;

use App\Models\ProductDetailModel;
use App\Models\ProductModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProductDetailRepository extends BaseRepository implements ProductDetailRepositoryInterface{

    public Model $model;
    public function __construct( ProductDetailModel $model)
    {
        $this->model = $model;
    }

    public function selectable(ProductModel $product)
    {
        return $this->model->where("product_id","=",$product->id)->with("value.key")->where("selectable",1)->get()->sortBy("key_id");
    }

    public function getProductDetailByValue(array $option, ProductModel $product)
    {
        return $this->model->where("product_id","=",$product->id)->whereIn("product_value_id",$option)->with("value.key")->get();
    }
}
