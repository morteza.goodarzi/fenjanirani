<?php

namespace App\Repositories\Address;

use App\Models\AddressModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class AddressRepository extends BaseRepository implements AddressRepositoryInterface{

    public Model $model;
    public function __construct( AddressModel $model)
    {
        $this->model = $model;
    }

}
