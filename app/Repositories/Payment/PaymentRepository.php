<?php

namespace App\Repositories\Payment;

use App\Models\PaymentModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class PaymentRepository extends BaseRepository implements PaymentRepositoryInterface{

    public Model $model;
    public function __construct( PaymentModel $model)
    {
        $this->model = $model;
    }

}
