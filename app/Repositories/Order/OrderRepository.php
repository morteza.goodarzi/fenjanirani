<?php

namespace App\Repositories\Order;

use App\Models\OrderModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface{

    public Model $model;
    public function __construct( OrderModel $model)
    {
        $this->model = $model;
    }

}
