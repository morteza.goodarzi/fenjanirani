<?php

namespace App\Repositories\UserCode;

use App\Models\UserCodeModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class UserCodeRepository extends BaseRepository implements UserCodeRepositoryInterface{

    public Model $model;
    public function __construct( UserCodeModel $model)
    {
        $this->model = $model;
    }

}
