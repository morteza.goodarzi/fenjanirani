<?php

namespace App\Repositories\CartItem;

use App\Models\CartItemModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class CartItemRepository extends BaseRepository implements CartItemRepositoryInterface{

    public Model $model;
    public function __construct( CartItemModel $model)
    {
        $this->model = $model;
    }

}
