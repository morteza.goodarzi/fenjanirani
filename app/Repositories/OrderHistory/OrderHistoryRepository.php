<?php

namespace App\Repositories\OrderHistory;

use App\Models\OrderHistoryModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class OrderHistoryRepository extends BaseRepository implements OrderHistoryRepositoryInterface{

    public Model $model;
    public function __construct( OrderHistoryModel $model)
    {
        $this->model = $model;
    }

}
