<?php

namespace App\Repositories\City;

use App\Models\CityModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class CityRepository extends BaseRepository implements CityRepositoryInterface{

    public Model $model;
    public function __construct( CityModel $model)
    {
        $this->model = $model;
    }

}
