<?php

namespace App\Repositories\Consultation;

use App\Models\ConsultationModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ConsultationRepository extends BaseRepository implements ConsultationRepositoryInterface{

    public Model $model;
    public function __construct( ConsultationModel $model)
    {
        $this->model = $model;
    }

}
