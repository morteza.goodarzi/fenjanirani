<?php

namespace App\Repositories\ProductValue;

use App\Models\ProductValueModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProductValueRepository extends BaseRepository implements ProductValueRepositoryInterface{

    public Model $model;
    public function __construct( ProductValueModel $model)
    {
        $this->model = $model;
    }

}
