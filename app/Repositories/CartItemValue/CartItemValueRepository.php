<?php

namespace App\Repositories\CartItemValue;

use App\Models\CartItemValueModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class CartItemValueRepository extends BaseRepository implements CartItemValueRepositoryInterface{

    public Model $model;
    public function __construct( CartItemValueModel $model)
    {
        $this->model = $model;
    }

}
