<?php

namespace App\Repositories\Product;

use App\Models\ProductModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface{

    public Model $model;
    public function __construct( ProductModel $model)
    {
        $this->model = $model;
    }

}
