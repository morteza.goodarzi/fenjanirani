<?php

namespace App\Repositories\Province;

use App\Models\ProvinceModel;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProvinceRepository extends BaseRepository implements ProvinceRepositoryInterface{

    public Model $model;
    public function __construct( ProvinceModel $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->orderBy("name","asc")->get();
    }
}
