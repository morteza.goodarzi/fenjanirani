<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(request()->user()->id == Auth::user()->id){
            return true;
        }
        return false;
    }

    public function rules():array
    {
        return [
            'name' => 'required|max:120',
            'family' => 'required|max:120',
//            'mobile' => [
//                'required',
//                'regex:/^09(1[0-9]|9[0-2]|2[0-2]|0[1-5]|41|3[0,3,5-9])\d{7}$/',
//                Rule::unique('users')->ignore(Auth::user()->id)
//            ],
            'email' => [
                'required',
                'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
                Rule::unique('users')->ignore(Auth::user()->id)
            ],
            'birth_date' => 'required|date',
            'province' => 'required',
            'city' => 'required',
        ];
    }

    public function messages():array
    {
        return [
            'name.required' => 'نام را وارد کنید',
            'name.max' => 'طول فیلد نام بیش از حد مجاز است',
            'family.required' => 'نام خانوادگی را وارد کنید',
            'family.max' => 'طول فیلد نام خانوادگی بیش از حد مجاز است',
            'mobile.required' => 'موبایل را وارد کنید',
            'mobile.regex' => 'فرمت موبایل اشتباه است',
            'email.required' => 'ایمیل را وارد کنید',
            'email.regex' => 'فرمت فیلد ایمیل اشتباه است',
            'birth_date.required' => 'تاریخ تولد را وارد کنید',
            'birth_date.date' => 'فرمت فیلد تاریخ تولد اشتباه است.',
        ];
    }
}
