<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'slug' => 'required|unique:pages,slug',
//            'mobile'=>new MobileRule
        ];
        if (in_array($this->method(), ['PUT', 'POST']) && $this->route()->parameter('page')) {
            $rules['slug'] = [
                'required',
                Rule::unique('pages')->ignore($this->route()->parameter('page')->id),
            ];
        }
        return $rules;
    }
}
