<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attachment' => 'sometimes|mimes:jpeg,png,jpg,gif|max:1024',
            'subject' => 'required',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
            "attachment.mimes" => "فرمت فایل انتخاب شده مجاز نیست",
            "attachment.max" => "سایز فایل بیش از حد مجاز است",
            "subject.required" => "موضوع تیکت اجباری است",
            "message.required" => "پیام اجباری است",
        ];
    }
}
