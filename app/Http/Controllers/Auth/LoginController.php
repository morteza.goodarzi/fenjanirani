<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Sms;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\User\UserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PROFILE;
    protected UserService $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->userService = App::make(UserService::class);

    }

    public function showLoginPage(Request $request)
    {
        $redirectUrl = $request->input("redirect-url");
        return view("front.auth.login",compact("redirectUrl"));
    }

    public function profileLogin(Request $request)
    {
        $mobile = $request->input("mobile");
        $verificationCode = rand(1000,9999);
        $redirectUrl = $request->input("redirect-url");
        Session::put('mobile', $mobile);
        Session::put('verificationCode', $verificationCode);
        Sms::SendByPattern("xev7na8aephuknv",$mobile,"verification-code",$verificationCode);
        return view('front.auth.verification-code',compact('mobile','redirectUrl'));
    }

    public function doLogin(Request $request)
    {
        $redirect_url = $request->input("redirect-url");
        if(Session::get('verificationCode') == intval($request->input("user_verification_code"))) {
            $user = $this->userService->first(["mobile","=",Session::get('mobile')]);
            if($user instanceof User)
            {
                return $this->customAuthenticate($user,$redirect_url);
            }
            else
            {
                $user = new User();
                $user->mobile = Session::get('mobile');
                $user->last_login_ip = $request->ip();
                $user->save();
                return $this->customAuthenticate($user,$redirect_url);
            }
        }
    }

    public function customAuthenticate(User $user,$redirectUrl)
    {
        Auth::login($user);
        if($redirectUrl){
            return redirect($redirectUrl);
        }
        return redirect()->route("profile.index");
    }
}
