<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\NewsletterModel;
use App\Services\Newsletter\NewsletterService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\NewsletterRequest;

class NewsletterController extends Controller
{

    public NewsletterService $newsletterService;

    public function __construct()
    {
        $this->newsletterService = App::make(NewsletterService::class);
    }

    public function index()
    {
        $newsletters = $this->newsletterService->paginate(10);
        return view("panel.newsletters.index",compact("newsletters"));
    }

    public function create()
    {
        return view("panel.newsletters.new");
    }

    public function edit(NewsletterModel $newsletter)
    {
        return view("panel.newsletters.edit",compact("newsletter"));
    }

    public function store(NewsletterRequest $request, NewsletterModel $newsletter = null)
    {
        $newsletterData = $request->all();

        $newsletter ? $this->newsletterService->save($newsletterData,$newsletter->id) : $newsletter = $this->newsletterService->save($newsletterData);

        // there is a situation that newsletter has not changed, but gallery request changed. this situation NewsletterObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($newsletter){
            return redirect()->back()->with('save',true);
        }
        return abort(500);
    }

    public function destroy(NewsletterModel $newsletter)
    {
        if( $this->newsletterService->delete($newsletter) ){
            return redirect()->route("admin.newsletters.index")->with("delete",true);
        }
    }

}
