<?php

namespace App\Http\Controllers;

use App\Helpers\Images;
use App\Services\Category\CategoryService;
use Illuminate\Support\Facades\Request;
use App\Models\BrandModel;
use App\Services\Brand\BrandService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\BrandRequest;

class BrandController extends Controller
{

    public BrandService $brandService;
    public CategoryService $categoryService;

    public function __construct()
    {
        $this->brandService = App::make(BrandService::class);
        $this->categoryService = App::make(CategoryService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->brandService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.brands.buttons')->with("row",$row);
                })
                ->addColumn('category', function ($row) {
                    return view('panel.brands.categories')->with("row",$row);
                })
                ->addColumn('image', function ($row) {
                    return '<img class="thumbnail" src="'. asset("brand-thumbnails/sm/".$row->image).'">';
                })
                ->addIndexColumn()
                ->rawColumns(['image','action','category'])
                ->make(true);
        }
        return view("panel.brands.index");
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return view("panel.brands.new",compact("categories"));
    }

    public function edit(BrandModel $brand)
    {
        $categories = $this->categoryService->all();
        return view("panel.brands.edit",compact("brand","categories"));
    }

    public function store(BrandRequest $request, BrandModel $brand = null)
    {
        $brandData = $request->all();

        if(array_key_exists("image",$brandData)){
            $image = new Images();
            $imageName = $image->uploadFile($request,"image",config("upload_image_path.brand-thumbnail"));
            $brandData["image"] = $imageName;
        }

        if(array_key_exists("seo_image",$brandData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"seo_image",config("upload_image_path.seo-image"));
            $brandData["seo_image"] = $thumbnailName;
        }

        if(!array_key_exists("indexable",$brandData)){
            $brandData["indexable"] = 0;
        }

        $brand ? $this->brandService->save($brandData,$brand->id) : $brand = $this->brandService->save($brandData);

        if(array_key_exists("category",$brandData)){
            $this->categoryService->syncCategory($brand,$brandData["category"]);
        } else {
            $this->categoryService->detachCategory($brand);
        }

        // there is a situation that brand has not changed, but gallery request changed. this situation BrandObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($brand){
            return redirect()->route('admin.brands.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(BrandModel $brand)
    {
        if( $this->brandService->delete($brand) ){
            return redirect()->route("admin.brands.index")->with("delete",true);
        }
    }

}
