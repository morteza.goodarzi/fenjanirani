<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\CartItemValueModel;
use App\Services\CartItemValue\CartItemValueService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\CartItemValueRequest;

class CartItemValueController extends Controller
{

    public CartItemValueService $cartitemvalueService;

    public function __construct()
    {
        $this->cartitemvalueService = App::make(CartItemValueService::class);
    }

    public function index()
    {
        $cartitemvalues = $this->cartitemvalueService->paginate(10);
        return view("panel.cartitemvalues.index",compact("cartitemvalues"));
    }

    public function create()
    {
        return view("panel.cartitemvalues.new");
    }

    public function edit(CartItemValueModel $cartitemvalue)
    {
        return view("panel.cartitemvalues.edit",compact("cartitemvalue"));
    }

    public function store(CartItemValueRequest $request, CartItemValueModel $cartitemvalue = null)
    {
        $cartitemvalueData = $request->all();

        $cartitemvalue ? $this->cartitemvalueService->save($cartitemvalueData,$cartitemvalue->id) : $cartitemvalue = $this->cartitemvalueService->save($cartitemvalueData);

        // there is a situation that cartitemvalue has not changed, but gallery request changed. this situation CartItemValueObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($cartitemvalue){
            return redirect()->route('admin.cartitemvalues.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(CartItemValueModel $cartitemvalue)
    {
        if( $this->cartitemvalueService->delete($cartitemvalue) ){
            return redirect()->route("admin.cartitemvalues.index")->with("delete",true);
        }
    }

}
