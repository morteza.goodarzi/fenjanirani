<?php

namespace App\Http\Controllers;

use App\Helpers\Convert;
use App\Helpers\Images;
use App\Models\ProductValueModel;
use App\Services\Brand\BrandService;
use App\Services\Category\CategoryService;
use App\Services\Gallery\GalleryService;
use App\Services\ProductDetail\ProductDetailService;
use App\Services\ProductKey\ProductKeyService;
use App\Services\ProductValue\ProductValueService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\ProductModel;
use App\Services\Product\ProductService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{

    public ProductService $productService;
    public CategoryService $categoryService;
    public BrandService $brandService;
    public ProductKeyService $productKeyService;
    public ProductDetailService $productDetailService;
    public ProductValueService $productValueService;
    public GalleryService $galleryService;

    public function __construct()
    {
        $this->productService = App::make(ProductService::class);
        $this->categoryService = App::make(CategoryService::class);
        $this->brandService = App::make(BrandService::class);
        $this->productKeyService = App::make(ProductKeyService::class);
        $this->productDetailService = App::make(ProductDetailService::class);
        $this->productValueService = App::make(ProductValueService::class);
        $this->galleryService = App::make(GalleryService::class);
    }

    public function index()
    {

        if(request()->ajax()){
            return datatables()->of($this->productService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.products.buttons')->with("row",$row);
                })
                ->addColumn('category', function ($row) {
                    return view('panel.products.categories')->with("row",$row);
                })
                ->editColumn('price', function ($row) {
                    return number_format($row->price);
                })
                ->editColumn('thumbnail', function ($row) {
                    return view('panel.products.thumbnail')->with("row",$row);
                })
                ->addIndexColumn()
                ->rawColumns(['brand','action','category'])
                ->make(true);
        }
        return view("panel.products.index");
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        $brands     = $this->brandService->all();
        $keys       = $this->productKeyService->all();
        return view("panel.products.new",compact("categories","brands","keys"));
    }

    public function edit(ProductModel $product)
    {
        $categories = $this->categoryService->all();
        $brands     = $this->brandService->all();
        $keys       = $this->productKeyService->all();

        $values=ProductValueModel::all();
        return view("panel.products.edit",compact("product","categories","brands","keys","values"));
    }

    public function store(ProductRequest $request, ProductModel $product = null)
    {
        $productData = $request->all();
        $productData["price"] = Convert::priceFormat($productData["price"]);
        if($request->file("thumbnail")){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"thumbnail",config("upload_image_path.product-thumbnail"));
            $productData["thumbnail"] = $thumbnailName;
        }

        if(array_key_exists("seo_image",$productData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"seo_image",config("upload_image_path.seo-image"));
            $productData["seo_image"] = $thumbnailName;
        }

        if(!array_key_exists("indexable",$productData)){
            $productData["indexable"] = 0;
        }
        if(!array_key_exists("exist",$productData)){
            $productData["exist"] = 0;
        }
        if(!array_key_exists("commentable",$productData)){
            $productData["commentable"] = 0;
        }

        $product ? $this->productService->save($productData,$product->id) : $product = $this->productService->save($productData);

        if(array_key_exists("category",$productData)){
            $this->categoryService->syncCategory($product,$productData["category"]);
        } else {
            $this->categoryService->detachCategory($product);
        }

        if(array_key_exists("galleries",$productData)){
            $this->galleryService->uploadGallery($product,$productData["galleries"]);
        }

        $this->saveProductDetails($productData,$product);


        // there is a situation that product has not changed, but gallery request changed. this situation ProductObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($product){
            return redirect()->route('admin.products.index')->with('save',true);
        }
        return abort(500);
    }

    public function saveProductDetails(array $request,ProductModel $product = null)
    {
        if(array_key_exists("product-value",$request)){
            $records = [];
            $productDetailSelectable=[];
            $valueIds = $this->productDetailService->get(["product_id","=",$product->id])->pluck("product_value_id");
            $values = $request["product-value"];
//            if($valueIds->toArray() != $values){
                $this->detachProductDetails($product);
                $count = count($values);
                for($i=0;$i<=$count-1;$i++) {
                    $selectable = false;
                    if(array_key_exists($i,$request["selectable"])) {
                        $selectable = true;
                    }
                    $productDetailSelectable[$i] = $selectable;
                }
                for ($i=0;$i <= $count-1;$i++){
                    $records[$i]["product_value_id"] = $values[$i];
                    $records[$i]["price"] = $request["product-price"][$i];
                    $records[$i]["product_id"] = $product->id;
                    $records[$i]["selectable"] = $productDetailSelectable[$i];

                }

//            array_unshift($records,"");
//            unset($records[0]);
//            }
            return $this->productDetailService->createMany($records);
        }
    }

    public function detachProductDetails(ProductModel $product)
    {
        return $this->productDetailService->deleteByCondition(["product_id","=",$product->id]);
    }

    public function show(ProductModel $product)
    {
        $details = $this->productDetailService->selectable($product);
        $categoryIds = $product->categories->pluck('id')->toArray();
        $relatedProducts = ProductModel::whereHas('categories', function ($query) use ($categoryIds) {
            return $query->whereIn('id', $categoryIds);
        })->where('id',"!=", $product->id)->inRandomOrder()->limit(8)->get();
        return view("front.products.index",compact("product","details","relatedProducts"));
    }

    public function destroy(ProductModel $product)
    {
        if( $this->productService->delete($product) ){
            return redirect()->route("admin.products.index")->with("delete",true);
        }
    }

    public function storeComment(Request $request,ProductModel $product):bool
    {
        $commentData = $request->all();
        Auth::check() ? $commentData["user_id"] = Auth::id() : $commentData["user_id"] = null;
        $commentData["status"] = config("comment-status.initialRegistration");
        if($product->comments()->create($commentData)){
            return true;
        }
        return false;
    }
}
