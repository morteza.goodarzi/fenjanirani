<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShoppingCartItemValueModel;
use App\Services\ShoppingCartItemValue\ShoppingCartItemValueService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ShoppingCartItemValueRequest;

class ShoppingCartItemValueController extends Controller
{

    public ShoppingCartItemValueService $shoppingcartitemvalueService;

    public function __construct()
    {
        $this->shoppingcartitemvalueService = App::make(ShoppingCartItemValueService::class);
    }

    public function index()
    {
        $shoppingcartitemvalues = $this->shoppingcartitemvalueService->paginate(10);
        return view("panel.shoppingcartitemvalues.index",compact("shoppingcartitemvalues"));
    }

    public function create()
    {
        return view("panel.shoppingcartitemvalues.new");
    }

    public function edit(ShoppingCartItemValueModel $shoppingcartitemvalue)
    {
        return view("panel.shoppingcartitemvalues.edit",compact("shoppingcartitemvalue"));
    }

    public function store(ShoppingCartItemValueRequest $request, ShoppingCartItemValueModel $shoppingcartitemvalue = null)
    {
        $shoppingcartitemvalueData = $request->all();

        $shoppingcartitemvalue ? $this->shoppingcartitemvalueService->save($shoppingcartitemvalueData,$shoppingcartitemvalue->id) : $shoppingcartitemvalue = $this->shoppingcartitemvalueService->save($shoppingcartitemvalueData);

        // there is a situation that shoppingcartitemvalue has not changed, but gallery request changed. this situation ShoppingCartItemValueObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($shoppingcartitemvalue){
            return redirect()->route('admin.shoppingcartitemvalues.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ShoppingCartItemValueModel $shoppingcartitemvalue)
    {
        if( $this->shoppingcartitemvalueService->delete($shoppingcartitemvalue) ){
            return redirect()->route("admin.shoppingcartitemvalues.index")->with("delete",true);
        }
    }

}
