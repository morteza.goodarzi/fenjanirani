<?php

namespace App\Http\Controllers;

use App\Helpers\Images;
use App\Http\Requests\CategoryRequest;
use App\Models\BrandModel;
use App\Models\CategoryModel;
use App\Models\PostModel;
use App\Models\ProductKeyModel;
use App\Models\ProductModel;
use App\Services\Category\CategoryService;
use App\Services\Post\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;

class CategoryController extends Controller
{

    protected CategoryService $categoryService;
    protected PostService $postService;

    public function __construct()
    {
        $this->categoryService = App::make(CategoryService::class);
        $this->postService = App::make(PostService::class);
    }

    public function paginate()
    {
        return $this->categoryService->paginate(10);
    }

    public function index(Builder $builder)
    {
        if(request()->ajax()){
            return datatables()->of($this->categoryService->all())
            ->editColumn('thumbnail', function ($data) {
                return '<img class="thumbnail" src="'. asset("category-thumbnails/sm/".$data->thumbnail).'">';
            })
            ->addColumn('action', function ($row) {
                return view('panel.categories.buttons')->with("row",$row);
            })
            ->addColumn('category', function ($row) {
                return view('panel.categories.categories')->with("row",$row);
            })
            ->addIndexColumn()
            ->rawColumns(['thumbnail','action'])
            ->make(true);
        }
        return view("panel.categories.index");
    }

    public function edit(CategoryModel $category)
    {
        $categories = $this->categoryService->all();
        return view("panel.categories.edit",compact("categories","category"));
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return view("panel.categories.new",compact("categories"));
    }

    public function store(CategoryRequest $request, CategoryModel $category = null)
    {
        $categoryData = $request->all();
        if(array_key_exists("thumbnail",$categoryData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"thumbnail",config("upload_image_path.category-thumbnail"));
            $categoryData["thumbnail"] = $thumbnailName;
        }

        if(array_key_exists("seo_image",$categoryData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"seo_image",config("upload_image_path.seo-image"));
            $categoryData["seo_image"] = $thumbnailName;
        }

        if(!array_key_exists("indexable",$categoryData)){
            $categoryData["indexable"] = 0;
        }

        $category ? $this->categoryService->save($categoryData,$category->id) : $category = $this->categoryService->save($categoryData);

        if(array_key_exists("parent",$categoryData)){
            $this->categoryService->syncCategory($category,$categoryData["parent"]);
        } else {
            $this->categoryService->detachCategory($category);
        }
        if($category){
            return redirect()->route('admin.categories.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(CategoryModel $category)
    {
        if( $this->categoryService->delete($category) ){
            return redirect()->route("admin.categories.index")->with("delete",true);
        }
    }

    public function showPost(CategoryModel $category)
    {
        $posts = $this->postService->category($category,10);
        $topHighlightPosts = $this->postService->latest(6);
        $topCategories = $this->categoryService->popular(6);
        $recentPosts = $this->postService->latest(4);
        return view("front.category.post-category",compact("category","posts","topHighlightPosts","topCategories","recentPosts"));
    }

    public function showProduct(Request $request,string $slug)
    {
        $category = CategoryModel::where("slug",$slug)->with("key.values")->with("key",function ($query){
            $query->orderBy("priority","desc");
        })->firstOrFail();
        $brands = BrandModel::whereHas("categories",function ($query) use ($category){
            return $query->where("category_model_id",$category->id);
        })->get();
        if($request->ajax()){
            DB::enableQueryLog();
            $brandIds = $request->input("brandIds");
            $valueIds = $request->input("valueIds");
            $minPrice = (int)$request->input("minPrice");
            $maxPrice = (int)$request->input("maxPrice");
            $order = $request->input("order");
            $pagination = $request->input("pagination");
            $productQuery = ProductModel::query();
            $productQuery->with("categories")->whereHas("categories",function ($query) use ($category){
                $query->where("category_model_id",$category->id);
            });
            if($brandIds){
                $productQuery->whereIn("brand_id",$brandIds);
            }
            if($valueIds){
                $productQuery->whereHas("details",function ($query) use ($valueIds){
                    return $query->whereIn("product_value_id",$valueIds);
                });
            }
            if($minPrice && $maxPrice){
                $productQuery->whereBetween("price",[$minPrice,$maxPrice]);
            }
            if($order == "latest"){
                $productQuery->latest();
            } else if($order == "oldest"){
                $productQuery->oldest();
            } else if($order == "name"){
                $productQuery->orderBy("name","asc");
            } else if($order == "price-desc"){
                $productQuery->orderBy("price","desc");
            } else if($order == "price-asc"){
                $productQuery->orderBy("price","asc");
            }
            $productQuery->paginate($pagination);
            $products = $productQuery->get();

            $preview="grid";
            if($request->input("preview")=="list") $preview = "list";
            $view = view('front.category.products',compact('products',"preview"))->render();
            return response()->json(['html'=>$view]);
        }
        $products = ProductModel::with("categories")->whereHas("categories",function ($query) use ($category){
            $query->where("category_model_id",$category->id);
        })->latest()->paginate(20);
        $minPrice = $products->min("price");
        $maxPrice = $products->max("price");
        if($minPrice == $maxPrice){
            $maxPrice++;
        }
        if(!$minPrice) $minPrice =0;
        if(!$maxPrice) $maxPrice =1;
        $preview="grid";
        if($request->input("preview")=="list") $preview = "list";
        $topCategories = CategoryModel::withCount("products")->orderBy("products_count","desc")->get();
        return view("front.category.product-category",compact("category","products","topCategories","preview","minPrice","maxPrice","brands"));
    }
}
