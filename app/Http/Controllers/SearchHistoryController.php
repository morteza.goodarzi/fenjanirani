<?php

namespace App\Http\Controllers;

use App\Models\BrandModel;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use App\Services\Post\PostService;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use App\Models\SearchHistoryModel;
use App\Services\SearchHistory\SearchHistoryService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\SearchHistoryRequest;
use Illuminate\Support\Facades\DB;

class SearchHistoryController extends Controller
{

    public searchHistoryService $searchHistoryService;
    public PostService $postService;
    public ProductService $productService;

    public function __construct()
    {
        $this->searchHistoryService = App::make(SearchHistoryService::class);
        $this->postService = App::make(PostService::class);
        $this->productService = App::make(ProductService::class);
    }

    public function index()
    {
        $searchHistories = $this->searchHistoryService->paginate(10);
        return view("panel.search-histories.index",compact("searchHistories"));
    }

    public function create()
    {
        return view("panel.searchHistories.new");
    }

    public function edit(SearchHistoryModel $searchHistory)
    {
        return view("panel.searchHistories.edit",compact("searchHistory"));
    }

//    public function store(SearchHistoryRequest $request)
//    {
//        $searchHistoryData = $request->all();
//        $search = $this->searchHistoryService->find(["search_text","=",$request->input("search_text")]);
//        if($search){
//            $search->increment("count");
//        } else {
//            $this->searchHistoryService->save($searchHistoryData);
//        }
//        return redirect()->route('admin.searchHistories.index')->with('save',true);
//
//    }

    public function destroy()
    {
        if( $this->searchHistoryService->truncate() ){
            return redirect()->route("admin.searchHistories.index")->with("delete",true);
        }
    }

    public function show(SearchHistoryRequest $request)
    {
        $searchData = $request->all();
        $search = $request->input("search_text");
        $searchObject = $this->searchHistoryService->find(["search_text","=",$search]);
        if($searchObject){
            $searchObject->increment("count");
        } else {
            $this->searchHistoryService->save($searchData);
        }
        $posts = $this->postService->search($search,16);
        return view("front.category.search",compact("search","posts"));
    }
    public function search(Request $request)
    {
        $search = $request->input("search");
        $this->searchHistoryService->save($search);
        if($request->ajax()){
            $brandIds = $request->input("brandIds");
            $valueIds = $request->input("valueIds");
            $minPrice = (int)$request->input("minPrice");
            $maxPrice = (int)$request->input("maxPrice");
            $order = $request->input("order");
            $pagination = $request->input("pagination");
            $productQuery = ProductModel::query();
            if($brandIds){
                $productQuery->whereIn("brand_id",$brandIds);
            }
            if($search){
                $productQuery->where("title","like","%".$search."%");
            }
            if($valueIds){
                $productQuery->whereHas("details",function ($query) use ($valueIds){
                    return $query->whereIn("product_value_id",$valueIds);
                });
            }
            if($minPrice && $maxPrice){
                $productQuery->whereBetween("price",[$minPrice,$maxPrice]);
            }
            if($order == "latest"){
                $productQuery->latest();
            } else if($order == "oldest"){
                $productQuery->oldest();
            } else if($order == "name"){
                $productQuery->orderBy("name","asc");
            } else if($order == "price-desc"){
                $productQuery->orderBy("price","desc");
            } else if($order == "price-asc"){
                $productQuery->orderBy("price","asc");
            }
            $productQuery->paginate($pagination);
            $products = $productQuery->get();

            $preview="grid";
            if($request->input("preview")=="list") $preview = "list";
            $view = view('front.category.products',compact('products',"preview"))->render();
            return response()->json(['html'=>$view]);
        }
        $products = ProductModel::with("categories")->latest()->where("title","like","%".$search."%")->paginate(12);
        $minPrice = $products->min("price");
        $maxPrice = $products->max("price");
        if($minPrice == $maxPrice){
            $maxPrice++;
        }
        if(!$minPrice) $minPrice =0;
        if(!$maxPrice) $maxPrice =1;
        $preview="grid";
        if($request->input("preview")=="list") $preview = "list";
        $topCategories = CategoryModel::withCount("products")->orderBy("products_count","desc")->get();
        return view("front.category.product-search",compact("products","topCategories","preview","minPrice","maxPrice","search"));
        }
}
