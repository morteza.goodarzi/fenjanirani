<?php

namespace App\Http\Controllers;

use App\Models\OrderModel;
use App\Services\Order\OrderService;
use App\Services\OrderHistory\OrderHistoryService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public OrderService $orderService;
    public OrderHistoryService $orderHistoryService;
    public function __construct()
    {
        $this->orderService = App::make(OrderService::class);
        $this->orderHistoryService = App::make(OrderHistoryService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->orderService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.orders.buttons')->with("row",$row);
                })
                ->addColumn('user', function ($row) {
                    return $row->user->name;
                })
                ->addColumn('status', function ($row) {
                    return view('panel.orders.status')->with("row",$row);
                })
                ->addIndexColumn()
                ->rawColumns(['user','action'])
                ->make(true);
        }
        return view("panel.orders.index");
    }

    public function create()
    {
        return view("panel.orders.new");
    }

    public function edit(OrderModel $order)
    {
        return view("panel.orders.edit",compact("order"));
    }

    public function store(OrderRequest $request, OrderModel $order = null)
    {
        $orderData = $request->all();

        $order ? $this->orderService->save($orderData,$order->id) : $order = $this->orderService->save($orderData);

        // there is a situation that order has not changed, but gallery request changed. this situation OrderObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($order){
            return redirect()->route('admin.orders.index')->with('save',true);
        }
        return abort(500);
    }

    public function changeStatus(OrderModel $order, int $status)
    {
        $historyData = [
            "from_status" => $order->status,
            "to_status" => $status,
            "order_id" => $order->id,
            "order_date"=>Carbon::now()->toDateTimeString()
        ];
        $this->orderHistoryService->save($historyData);
        if($this->orderService->changeStatus($status,$order)){
            return redirect()->route("admin.orders.edit",["order"=>$order->id])->with("save",true);
        }
    }

    public function destroy(OrderModel $order)
    {
        if( $this->orderService->delete($order) ){
            return redirect()->route("admin.orders.index")->with("delete",true);
        }
    }

    public function orderProfile(Request $request)
    {
        $orders = [];
        $status = $request->input("status");
        $condition = ["user_id" => Auth::user()->id];
        if($status == config("order-status.unPaid")){
            $condition["status"] = config("order-status.unPaid");
        }
        else if($status == config("order-status.delivered")){
            $condition["status"] = config("order-status.delivered");
        }
        else if($status == config("order-status.preparing")){
            $condition["status"] = config("order-status.preparing");
        }
        else if($status == config("order-status.sending")){
            $condition["status"] = config("order-status.sending");
        }
        else{
            unset($condition["status"]);
        }
        $orders = $this->orderService->getMultipleCondition($condition);
        return view("profile.orders.index",compact("orders"));
    }

    public function orderProfileDetail(OrderModel $order)
    {
        return view("profile.orders.detail",compact("order"));
    }

}
