<?php

namespace App\Http\Controllers;

use App\Models\ProductKeyModel;
use App\Services\ProductKey\ProductKeyService;
use App\Models\ProductValueModel;
use App\Services\ProductValue\ProductValueService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ProductValueRequest;

class ProductValueController extends Controller
{

    public ProductValueService $productValueService;
    public ProductKeyService $productKeyService;

    public function __construct()
    {
        $this->productValueService = App::make(ProductValueService::class);
        $this->productKeyService = App::make(ProductKeyService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->productValueService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.product-values.buttons')->with("row",$row);
                })
                ->addColumn('category', function ($row) {
                    return view('panel.product-values.categories')->with("row",$row);
                })
                ->addColumn('key', function ($row) {
                    return $row->key->name;
                })
                ->addIndexColumn()
                ->rawColumns(['action','category',"key"])
                ->make(true);
        }
        return view("panel.product-values.index");
    }

    public function create()
    {
        $keys = $this->productKeyService->all();
        return view("panel.product-values.new",compact("keys"));
    }

    public function edit(ProductValueModel $value)
    {
        $keys = $this->productKeyService->all();
        return view("panel.product-values.edit",compact("value","keys"));
    }

    public function store(ProductValueRequest $request, ProductValueModel $value = null)
    {
        $productValueData = $request->all();
        $value ? $this->productValueService->save($productValueData,$value->id) : $value = $this->productValueService->save($productValueData);

        // there is a situation that productValue has not changed, but gallery request changed. this situation ProductValueObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($value){
            return redirect()->route('admin.values.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ProductValueModel $value)
    {
        if( $this->productValueService->delete($value) ){
            return redirect()->route("admin.values.index")->with("delete",true);
        }
    }

    public function getByKeyAjax(Request $request)
    {
        if($request->ajax()){
            $keyId = $request->input("key_id");
            $values = $this->productValueService->get(["product_key_id","=",$keyId]);
            $view = view('panel.products.values-ajax',compact('values'))->render();
            return response()->json(['html'=>$view]);
        }
        abort("404");
    }
}
