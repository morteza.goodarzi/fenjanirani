<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\Post\PostService;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public PostService $postService;
    public ProductService $productService;

    public function __construct()
    {
        $this->postService = App::make(PostService::class);
        $this->productService = App::make(ProductService::class);
    }
    public function index()
    {
        $latestPosts = $this->postService->latest(8);
        $latestProducts = $this->productService->latest(8);
        return view('front.home.index',compact("latestPosts","latestProducts"));
    }
}
