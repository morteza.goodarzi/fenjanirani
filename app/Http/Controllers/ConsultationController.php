<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\ConsultationModel;
use App\Services\Consultation\ConsultationService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ConsultationRequest;

class ConsultationController extends Controller
{

    public ConsultationService $consultationService;

    public function __construct()
    {
        $this->consultationService = App::make(ConsultationService::class);
    }

    public function index()
    {
        $consultations = $this->consultationService->paginate(10);
        return view("panel.consultations.index",compact("consultations"));
    }

    public function create()
    {
        return view("panel.consultations.new");
    }

    public function edit(ConsultationModel $consultation)
    {
        return view("panel.consultations.edit",compact("consultation"));
    }

    public function store(ConsultationRequest $request, ConsultationModel $consultation = null)
    {
        $consultationData = $request->all();

        $consultation ? $this->consultationService->save($consultationData,$consultation->id) : $consultation = $this->consultationService->save($consultationData);

        // there is a situation that consultation has not changed, but gallery request changed. this situation ConsultationObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($consultation){
            return redirect()->back()->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ConsultationModel $consultation)
    {
        if( $this->consultationService->delete($consultation) ){
            return redirect()->route("admin.consultations.index")->with("delete",true);
        }
    }

}
