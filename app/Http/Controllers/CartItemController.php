<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\CartItemModel;
use App\Services\CartItem\CartItemService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\CartItemRequest;

class CartItemController extends Controller
{

    public CartItemService $cartitemService;

    public function __construct()
    {
        $this->cartitemService = App::make(CartItemService::class);
    }

    public function index()
    {
        $cartitems = $this->cartitemService->paginate(10);
        return view("panel.cartitems.index",compact("cartitems"));
    }

    public function create()
    {
        return view("panel.cartitems.new");
    }

    public function edit(CartItemModel $cartitem)
    {
        return view("panel.cartitems.edit",compact("cartitem"));
    }

    public function store(CartItemRequest $request, CartItemModel $cartitem = null)
    {
        $cartitemData = $request->all();

        $cartitem ? $this->cartitemService->save($cartitemData,$cartitem->id) : $cartitem = $this->cartitemService->save($cartitemData);

        // there is a situation that cartitem has not changed, but gallery request changed. this situation CartItemObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($cartitem){
            return redirect()->route('admin.cartitems.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(CartItemModel $cartitem)
    {
        if( $this->cartitemService->delete($cartitem) ){
            return redirect()->route("admin.cartitems.index")->with("delete",true);
        }
    }

}
