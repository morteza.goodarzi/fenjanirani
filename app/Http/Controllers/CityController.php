<?php

namespace App\Http\Controllers;

use App\Models\CityModel;
use App\Services\City\CityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Requests\CityRequest;

class CityController extends Controller
{

    public CityService $cityService;

    public function __construct()
    {
        $this->cityService = App::make(CityService::class);
    }

    public function index()
    {
        $citys = $this->cityService->paginate(10);
        return view("panel.citys.index",compact("citys"));
    }

    public function create()
    {
        return view("panel.citys.new");
    }

    public function edit(CityModel $city)
    {
        return view("panel.citys.edit",compact("city"));
    }

    public function store(CityRequest $request, CityModel $city = null)
    {
        $cityData = $request->all();

        $city ? $this->cityService->save($cityData,$city->id) : $city = $this->cityService->save($cityData);

        // there is a situation that city has not changed, but gallery request changed. this situation CityObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($city){
            return redirect()->route('admin.citys.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(CityModel $city)
    {
        if( $this->cityService->delete($city) ){
            return redirect()->route("admin.citys.index")->with("delete",true);
        }
    }

    public function getCitiesAjax(Request $request)
    {
        $province_id = $request->input("province_id");
        $cities = $this->cityService->get(["province_id","=",$province_id]);
        return view("profile.profile.cities-dropdown",compact("cities"));
    }

}
