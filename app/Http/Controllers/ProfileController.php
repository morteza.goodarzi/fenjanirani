<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\ProvinceModel;
use App\Services\Address\AddressService;
use App\Services\Province\ProvinceService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    protected ProvinceService $provinceService;
    protected UserService $userService;

    public function __construct()
    {
        $this->provinceService = App::make(ProvinceService::class);
        $this->userService = App::make(UserService::class);
    }

    public function index()
    {
        return view("profile.dashboard.index");
    }

    public function profileEdit()
    {
        $user = Auth::user();
        $provinces = ProvinceModel::all();
        return view("profile.profile.edit",compact("provinces","user"));
    }

    public function updateProfile(UserRequest $request)
    {
        if($this->userService->save($request->all(),$request->user()->id)){
            return redirect()->back()->with("success",true);
        }
    }
}
