<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayClass;
use App\Helpers\Images;
use App\Http\Requests\PostRequest;
use App\Models\PostModel;
use App\Repositories\Post\PostRepositoryInterface;
use App\Services\Category\CategoryService;
use App\Services\Comment\CommentService;
use App\Services\Gallery\GalleryService;
use App\Services\Post\PostService;
use App\Services\Product\ProductService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;
use Yajra\DataTables\Html\Builder;

class PostController extends Controller
{

    public PostService $postService;
    public CategoryService $categoryService;
    public GalleryService $galleryService;
    public CommentService $commentService;
    public ProductService $productService;

    public function __construct()
    {
        $this->postService = App::make(PostService::class);
        $this->categoryService = App::make(CategoryService::class);
        $this->galleryService = App::make(GalleryService::class);
        $this->commentService = App::make(CommentService::class);
        $this->productService = App::make(ProductService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->postService->all())
                ->editColumn('created_at', function ($data) {
                    return \Morilog\Jalali\Jalalian::forge($data->created_at)->format('%Y/%m/%d');
                })
                ->addColumn('action', function ($row) {
                    return view('panel.posts.buttons')->with("row",$row);
                })
                ->addColumn('category', function ($row) {
                    return view('panel.posts.categories')->with("row",$row);
                })
                ->addColumn('username', function ($row) {
                    return $row->user->name;
                })
                ->addIndexColumn()
                ->rawColumns(['thumbnail','action','category'])
                ->make(true);
        }
        return view("panel.posts.index");
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return view("panel.posts.new",compact("categories"));
    }

    public function edit(PostModel $post)
    {
        $categories = $this->categoryService->all();
        return view("panel.posts.edit",compact("post","categories"));
    }

    public function store(PostRequest $request, PostModel $post = null)
    {
        $postData = $request->all();
        $postData["user_id"] = Auth::id();

        if($request->file("thumbnail")){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"thumbnail",config("upload_image_path.post-thumbnail"));
            $postData["thumbnail"] = $thumbnailName;
        }

        if(array_key_exists("seo_image",$postData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"seo_image",config("upload_image_path.seo-image"));
            $postData["seo_image"] = $thumbnailName;
        }
        if(!array_key_exists("indexable",$postData)){
            $postData["indexable"] = 0;
        }
        if(!array_key_exists("highlight",$postData)){
            $postData["highlight"] = 0;
        }
        if(!array_key_exists("is_special",$postData)){
            $postData["is_special"] = 0;
        }

        $post ? $this->postService->save($postData,$post->id) : $post = $this->postService->save($postData);

        // there is a situation that post has not changed, but gallery request changed. this situation PostObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        //bayad dorost beshe
//        if(array_key_exists("galleries",$postData)){
//            $this->galleryService->uploadGallery($post,$postData["galleries"]);
//        }
        if(array_key_exists("category",$postData)){
            $this->categoryService->syncCategory($post,$postData["category"]);
        } else {
            $this->categoryService->detachCategory($post);
        }

        if($post){
            return redirect()->route('admin.posts.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(PostModel $post)
    {
        if( $this->postService->delete($post) ){
            return redirect()->route("admin.posts.index")->with("delete",true);
        }
    }

    public function show(PostModel $post)
    {
        $categoryIds = ArrayClass::flatten($post->categories->pluck("id")->toArray());
        $relatedPosts = $this->postService->related($categoryIds,$post,8);
        $categories = $this->categoryService->popular(6);
        $popularProducts = $this->productService->latest(4);
        return view("front.post.index",compact("post","relatedPosts","categories","popularProducts"));
    }

    public function storeComment(Request $request,PostModel $post):bool
    {
        $commentData = $request->all();
        Auth::check() ? $commentData["user_id"] = Auth::id() : $commentData["user_id"] = null;
        $commentData["status"] = config("comment-status.initialRegistration");
        if($post->comments()->create($commentData)){
            return true;
        }
        return false;
    }

}
