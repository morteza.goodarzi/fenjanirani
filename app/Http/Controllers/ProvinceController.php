<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\ProvinceModel;
use App\Services\Province\ProvinceService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ProvinceRequest;

class ProvinceController extends Controller
{

    public ProvinceService $provinceService;

    public function __construct()
    {
        $this->provinceService = App::make(ProvinceService::class);
    }

    public function index()
    {
        $provinces = $this->provinceService->paginate(10);
        return view("panel.provinces.index",compact("provinces"));
    }

    public function create()
    {
        return view("panel.provinces.new");
    }

    public function edit(ProvinceModel $province)
    {
        return view("panel.provinces.edit",compact("province"));
    }

    public function store(ProvinceRequest $request, ProvinceModel $province = null)
    {
        $provinceData = $request->all();

        $province ? $this->provinceService->save($provinceData,$province->id) : $province = $this->provinceService->save($provinceData);

        // there is a situation that province has not changed, but gallery request changed. this situation ProvinceObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($province){
            return redirect()->route('admin.provinces.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ProvinceModel $province)
    {
        if( $this->provinceService->delete($province) ){
            return redirect()->route("admin.provinces.index")->with("delete",true);
        }
    }

}
