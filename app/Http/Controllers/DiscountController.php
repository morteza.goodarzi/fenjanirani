<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DiscountModel;
use App\Services\Discount\DiscountService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\DiscountRequest;
use Illuminate\Support\Facades\Session;

class DiscountController extends Controller
{

    public DiscountService $discountService;

    public function __construct()
    {
        $this->discountService = App::make(DiscountService::class);
    }

    public function index()
    {
        $discounts = $this->discountService->paginate(10);
        return view("panel.discounts.index",compact("discounts"));
    }

    public function create()
    {
        return view("panel.discounts.new");
    }

    public function edit(DiscountModel $discount)
    {
        return view("panel.discounts.edit",compact("discount"));
    }

    public function store(DiscountRequest $request, DiscountModel $discount = null)
    {
        $discountData = $request->all();

        $discount ? $this->discountService->save($discountData,$discount->id) : $discount = $this->discountService->save($discountData);

        // there is a situation that discount has not changed, but gallery request changed. this situation DiscountObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($discount){
            return redirect()->route('admin.discounts.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(DiscountModel $discount)
    {
        if( $this->discountService->delete($discount) ){
            return redirect()->route("admin.discounts.index")->with("delete",true);
        }
    }

    public function update(Request $request)
    {
        Session::put('discount',$request->coupon_code);
        $discount = DiscountModel::where('code',$request->coupon_code)->where('from_date', "<=",date('Y-m-d H:i:s'))->where("end_date", ">=", date('Y-m-d H:i:s'))->where('status',1)->first();
        if($discount instanceof DiscountModel)
        {

            $cartTotal = \Cart::getSubTotal() - \Cart::getTotal();
            //dd($cartTotal);
            $percent = $discount->percent;
            Session::put('discount_percent',$percent);
            $percent_struct ='-'.$percent.'%';
            $message_discount = $percent." درصد تخفیف ";
            Session::put('message_discount',$message_discount);
            $condition = new \Darryldecode\Cart\CartCondition([
                'name' => 'TAX 12% VAT',
                'type' => 'tax' ,
                'target' => 'total',
                'value' => $percent_struct
            ]);
            $conditions = \Cart::getConditions()->put($condition->getName(), $condition)->toArray();
            \Cart::condition($conditions);
            return redirect()->route('shopping-cart.index');
        }
        return redirect()->route('shopping-cart.index')->with("cart.error",true);
    }

}
