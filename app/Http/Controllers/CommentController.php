<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Models\CommentModel;
use App\Services\Comment\CommentService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{

    public CommentService $commentService;

    public function __construct()
    {
        $this->commentService = App::make(CommentService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->commentService->all())
                ->editColumn('created_at', function ($row) {
                    return \Morilog\Jalali\Jalalian::forge($row->created_at)->format('%Y/%m/%d');
                })
                ->editColumn('commentable_type', function ($row) {
                    if($row->commentable_type == "App\Models\PostModel"){
                        return "پست";
                    } else if($row->commentable_type == "App".DIRECTORY_SEPARATOR."Models".DIRECTORY_SEPARATOR."ProductModel"){
                        return "محصول";
                    }
                    return "-";
                })
                ->editColumn('commentable_title', function ($row) {
                    return $row->commentable->title;
                })
                ->addColumn('persian_status', function ($row) {
                    return $row->persian_status;
                })
                ->addColumn('action', function ($row) {
                    return view('panel.comments.buttons')->with("row",$row);
                })
                ->addColumn('username', function ($row) {
                    return $row->user->name;
                })
                ->addIndexColumn()
                ->rawColumns(['action','username',"commentable","commentable_title"])
                ->make(true);
        }
        return view("panel.comments.index");
    }

    public function create()
    {
        return view("panel.comments.new");
    }

    public function edit(CommentModel $comment)
    {
        return view("panel.comments.edit",compact("comment"));
    }

    public function store(CommentRequest $request, CommentModel $comment = null)
    {
        $commentData = $request->all();
        Auth::check() ? $commentData["user_id"] = Auth::id() : $commentData["user_id"] = null;
        $commentData["status"] = config("comment-status.initialRegistration");
        $commentData["commentable_type"] = $namespace;
        $model = $namespace::find($request["commentable_id"]);
        $comment ? $this->commentService->save($commentData,$model,$comment->id) : $comment = $this->commentService->save($commentData,$model);

        // there is a situation that comment has not changed, but gallery request changed. this situation CommentObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($comment){
            return true;
        }
        return abort(500);
    }

    public function destroy(CommentModel $comment)
    {
        if( $this->commentService->delete($comment) ){
            return redirect()->route("admin.comments.index")->with("delete",true);
        }
    }

    public function show(CommentModel $comment)
    {
        return view("panel.comments.view",compact("comment"));
    }
}
