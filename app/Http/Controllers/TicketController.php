<?php

namespace App\Http\Controllers;

use App\Helpers\Images;
use App\Helpers\StringGenerator;
use App\Services\TicketMessage\TicketMessageService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Models\TicketModel;
use App\Services\Ticket\TicketService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\TicketRequest;
use Morilog\Jalali\Jalalian;

class TicketController extends Controller
{

    public TicketService $ticketService;
    public TicketMessageService $ticketMessageService;

    public function __construct()
    {
        $this->ticketService = App::make(TicketService::class);
        $this->ticketMessageService = App::make(TicketMessageService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->ticketService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.tickets.buttons')->with("row",$row);
                })
                ->addColumn('user', function ($row) {
                    return $row->user->name." ".$row->user->family;
                })
                ->editColumn('created_at', function ($row) {
                    return Jalalian::forge($row->created_at)->format("%d - %m - %Y");
                })
                ->addColumn('status', function ($row) {
                    return view('panel.tickets.status')->with("row",$row);
                })
                ->addColumn('department_name', function ($row) {
                    return view('panel.tickets.department')->with("row",$row);
                })
                ->addIndexColumn()
                ->rawColumns(['user','action'])
                ->make(true);
        }
        return view("panel.tickets.index");
    }

    public function create()
    {
        return view("panel.tickets.new");
    }

    public function edit(TicketModel $ticket)
    {
        return view("panel.tickets.edit",compact("ticket"));
    }

    public function store(TicketRequest $request, TicketModel $ticket = null)
    {
        $ticketData = $request->all();

        $ticketData["code"] = uniqid(StringGenerator::make(3)).Auth::user()->id;
        $ticketData["user_id"] = Auth::user()->id;
        $ticketData["is_admin"] = 0;
        $ticketData["status"] = config("ticket-status.awaiting-answer");
        if(!array_key_exists("sms_notifiable",$ticketData)){
            $ticketData["sms_notifiable"] = 0;
        }
        $ticket ? $this->ticketService->save($ticketData,$ticket->id) : $ticket = $this->ticketService->save($ticketData);
        // there is a situation that ticket has not changed, but gallery request changed. this situation TicketObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($ticket){
            $ticketData["ticket_id"] = $ticket->id;

            if($request->file("attachment")){
                $image = new Images();
                $attachmentName = $image->uploadFile($request,"attachment",config("upload_image_path.ticket-attachment"));
                $ticketData["attachment"] = $attachmentName;
            }

            $this->ticketMessageService->save($ticketData);
            return redirect()->route('profile.ticket.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(TicketModel $ticket)
    {
        if( $this->ticketService->delete($ticket) ){
            return redirect()->route("admin.tickets.index")->with("delete",true);
        }
    }

    public function profileTicket(\Illuminate\Http\Request $request)
    {
        $status = $request->input("status");
        $tickets = $this->ticketService->getMyTicket(Auth::user()->id,$status);
        return view("profile.tickets.index",compact("tickets"));
    }

    public function profileTicketDetail(TicketModel $ticket)
    {
        return view("profile.tickets.detail",compact("ticket"));
    }

    public function profileNewTicket()
    {
        return view("profile.tickets.new");
    }

    public function detail(TicketModel $ticket)
    {
        return view("panel.tickets.detail",compact("ticket"));
    }

    public function close(TicketModel $ticket)
    {
        $ticket->status = config("ticket-status.rejected");
        $ticket->save();
        return redirect()->route("admin.ticket.index")->with("save",true);
    }
}
