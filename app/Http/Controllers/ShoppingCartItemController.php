<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShoppingCartItemModel;
use App\Services\ShoppingCartItem\ShoppingCartItemService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ShoppingCartItemRequest;

class ShoppingCartItemController extends Controller
{

    public ShoppingCartItemService $shoppingcartitemService;

    public function __construct()
    {
        $this->shoppingcartitemService = App::make(ShoppingCartItemService::class);
    }

    public function index()
    {
        $shoppingcartitems = $this->shoppingcartitemService->paginate(10);
        return view("panel.shoppingcartitems.index",compact("shoppingcartitems"));
    }

    public function create()
    {
        return view("panel.shoppingcartitems.new");
    }

    public function edit(ShoppingCartItemModel $shoppingcartitem)
    {
        return view("panel.shoppingcartitems.edit",compact("shoppingcartitem"));
    }

    public function store(ShoppingCartItemRequest $request, ShoppingCartItemModel $shoppingcartitem = null)
    {
        $shoppingcartitemData = $request->all();

        $shoppingcartitem ? $this->shoppingcartitemService->save($shoppingcartitemData,$shoppingcartitem->id) : $shoppingcartitem = $this->shoppingcartitemService->save($shoppingcartitemData);

        // there is a situation that shoppingcartitem has not changed, but gallery request changed. this situation ShoppingCartItemObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($shoppingcartitem){
            return redirect()->route('admin.shoppingcartitems.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ShoppingCartItemModel $shoppingcartitem)
    {
        if( $this->shoppingcartitemService->delete($shoppingcartitem) ){
            return redirect()->route("admin.shoppingcartitems.index")->with("delete",true);
        }
    }

}
