<?php

namespace App\Http\Controllers;

use App\Helpers\Images;
use App\Services\Category\CategoryService;
use App\Services\Comment\CommentService;
use App\Services\Gallery\GalleryService;
use App\Services\Post\PostService;
use Illuminate\Support\Facades\Request;
use App\Models\PageModel;
use App\Services\Page\PageService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\PageRequest;

class PageController extends Controller
{


    public PageService $pageService;
    public CategoryService $categoryService;
    public GalleryService $galleryService;
    public CommentService $commentService;

    public function __construct()
    {
        $this->pageService = App::make(PageService::class);
        $this->categoryService = App::make(CategoryService::class);
        $this->galleryService = App::make(GalleryService::class);
        $this->commentService = App::make(CommentService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->pageService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.pages.buttons')->with("row",$row);
                })
                ->addColumn('layout', function ($row) {
                    return view('panel.pages.layout')->with("row",$row);
                })
                ->addIndexColumn()
                ->rawColumns(['parent','action'])
                ->make(true);
        }
        return view("panel.pages.index");
    }

    public function create()
    {
        return view("panel.pages.new");
    }

    public function edit(PageModel $page)
    {
        return view("panel.pages.edit",compact("page"));
    }

    public function store(PageRequest $request, PageModel $page = null)
    {
        $pageData = $request->all();

        if(array_key_exists("seo_image",$pageData)){
            $image = new Images();
            $thumbnailName = $image->uploadFile($request,"seo_image",config("upload_image_path.seo-image"));
            $pageData["seo_image"] = $thumbnailName;
        }
        if(!array_key_exists("indexable",$pageData)){
            $pageData["indexable"] = 0;
        }

        if(!array_key_exists("display_header",$pageData)){
            $pageData["display_header"] = 0;
        }
        if(!array_key_exists("display_footer",$pageData)){
            $pageData["display_footer"] = 0;
        }

        $page ? $this->pageService->save($pageData,$page->id) : $page = $this->pageService->save($pageData);

        // there is a situation that page has not changed, but gallery request changed. this situation PageObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request
        if(array_key_exists("gallery",$pageData)){
            $this->galleryService->uploadGallery($page,$pageData["gallery"]);
        }

        if($page){
            return redirect()->route('admin.page.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(PageModel $page)
    {
        if( $this->pageService->delete($page) ){
            return redirect()->route("admin.pages.index")->with("delete",true);
        }
    }

    public function show(PageModel $page)
    {
        return view("front.page.index",compact("page"));
    }

}
