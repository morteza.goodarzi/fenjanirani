<?php

namespace App\Http\Controllers;

use App\Services\Category\CategoryService;
use Illuminate\Support\Facades\Request;
use App\Models\ProductKeyModel;
use App\Services\ProductKey\ProductKeyService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ProductKeyRequest;
use Yajra\DataTables\Html\Builder;

class ProductKeyController extends Controller
{

    public ProductKeyService $productKeyService;
    public CategoryService $categoryService;

    public function __construct()
    {
        $this->productKeyService = App::make(ProductKeyService::class);
        $this->categoryService = App::make(CategoryService::class);
    }

    public function index()
    {
        if(request()->ajax()){
            return datatables()->of($this->productKeyService->all())
                ->addColumn('action', function ($row) {
                    return view('panel.product-keys.buttons')->with("row",$row);
                })
                ->addColumn('category', function ($row) {
                    return view('panel.product-keys.categories')->with("row",$row);
                })
                ->addIndexColumn()
                ->rawColumns(['action','category'])
                ->make(true);
        }
        return view("panel.product-keys.index");
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return view("panel.product-keys.new",compact("categories"));
    }

    public function edit(ProductKeyModel $key)
    {
        $categories = $this->categoryService->all();
        return view("panel.product-keys.edit",compact("key","categories"));
    }

    public function store(ProductKeyRequest $request, ProductKeyModel $key = null)
    {
        $productKeyData = $request->all();

        $key ? $this->productKeyService->save($productKeyData,$key->id) : $key = $this->productKeyService->save($productKeyData);

        if(array_key_exists("category",$productKeyData)){
            $this->categoryService->syncCategory($key,$productKeyData["category"]);
        } else {
            $this->categoryService->detachCategory($key);
        }

        // there is a situation that key has not changed, but gallery request changed. this situation ProductKeyObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($key){
            return redirect()->route('admin.keys.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ProductKeyModel $key)
    {
        if( $this->productKeyService->delete($key) ){
            return redirect()->route("admin.keys.index")->with("delete",true);
        }
    }

}
