<?php

namespace App\Http\Controllers;

use App\Helpers\Zarinpal;
use App\Models\CartItemModel;
use App\Models\CartItemValueModel;
use App\Models\DiscountModel;
use App\Models\ProductDetailModel;
use App\Models\ProductModel;
use App\Services\Address\AddressService;
use App\Services\Order\OrderService;
use App\Services\ProductDetail\ProductDetailService;
use App\Services\ProductValue\ProductValueService;
use App\Services\Province\ProvinceService;
use App\Services\ShoppingCartItem\ShoppingCartItemService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Models\ShoppingCartModel;
use App\Services\ShoppingCart\ShoppingCartService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ShoppingCartRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ShoppingCartController extends Controller
{

    public ShoppingCartService $shoppingCartService;
    public ProductDetailService $productDetailService;
    public UserService $userService;
    public AddressService $addressService;
    public ProvinceService $provinceService;
    public ShoppingCartItemService $shoppingCartItemService;
    public OrderService $orderService;

    public function __construct()
    {
        $this->shoppingCartService = App::make(ShoppingCartService::class);
        $this->productDetailService = App::make(ProductDetailService::class);
        $this->userService = App::make(UserService::class);
        $this->addressService = App::make(AddressService::class);
        $this->provinceService = App::make(ProvinceService::class);
        $this->shoppingCartItemService = App::make(ShoppingCartItemService::class);
        $this->orderService = App::make(OrderService::class);
    }

    public function index()
    {
        $cartCollection = \Cart::getContent();
        return view('front.shopping-carts.index')->with(['cartCollection' => $cartCollection]);
    }

    public function create()
    {
        return view("panel.shopping-carts.new");
    }

    public function edit(ShoppingCartModel $shoppingCart)
    {
        return view("panel.shopping-carts.edit",compact("shoppingCart"));
    }

    public function store(ShoppingCartRequest $request, ShoppingCartModel $shoppingCart = null)
    {
        $shoppingCartData = $request->all();

        $shoppingCart ? $this->shoppingCartService->save($shoppingCartData,$shoppingCart->id) : $shoppingCart = $this->shoppingCartService->save($shoppingCartData);

        // there is a situation that shopping-cart has not changed, but gallery request changed. this situation ShoppingCartObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($shoppingCart){
            return redirect()->route('admin.shopping-carts.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(Request $request)
    {
        \Cart::remove($request->input("cart-id"));
        return redirect()->route("shopping-cart.index");
    }
    public function addToCart(Request $request,ProductModel $product){
        $totalPrice = 0;
        if($request->has("option"))
        {
            $option = $request->input("option");
            $productDetails = $this->productDetailService->getProductDetailByValue($option,$product);
            $totalPrice = $productDetails->sum("price");
            foreach($productDetails as $detail)
            {
                $row = array();
                $row['option_id'] = $detail->product_value_id;
                $row['option_parent'] = $detail->value->key->id;
                $row['option_name'] = $detail->value->value;
                $row['option_parent_name'] = $detail->value->key->name;
                $row['price'] = $detail->price;
                $attributes['option'][] = $row;
            }
        }
        $attributes['thumbnail'] = $product->thumbnail;
        $attributes['slug'] = $product->slug;
        \Cart::add(array(
            'id' => $product->id,
            'name' => $product->title,
            'price' => $product->price + $totalPrice ,
            'quantity' => 1,
            'attributes' => $attributes
        ));
        return redirect()->route("shopping-cart.index");
    }

    public function update(Request $request)
    {
        \Cart::update($request->id,
            array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
            ));

        $cartCollection = \Cart::getContent();
        $cartTotalCount = \Cart::getTotalQuantity();

        $percent = 0;
        $percent_struct ='-'.$percent.'%';
        $condition = new \Darryldecode\Cart\CartCondition([
            'name' => 'TAX 12% VAT',
            'type' => 'tax' ,
            'target' => 'total',
            'value' => $percent_struct
        ]);

        $cartTotalDiscount = \Cart::getSubTotal() - \Cart::getTotal();
        $cartTotal =  \Cart::getTotal();
        $conditions = \Cart::getConditions()->put($condition->getName(), $condition)->toArray();
        \Cart::condition($conditions);
        return response()->json([$cartCollection,$cartTotal,$cartTotalCount,$cartTotalDiscount]);
    }

    public function checkout()
    {
        $user = $this->userService->getMe();
        $address = $this->addressService->get(["user_id","=",$user->id]);
        $provinces = $this->provinceService->all();
        return view("front.shopping-carts.checkout",compact("user","address","provinces"));
    }

    public function sendPayment(Request $request)
    {
        $user = $this->userService->getMe();
        $shoppingCartData = $request->all();
        $shoppingCartData["total_price"] = \Cart::getTotal();
//        $shoppingCartData["discount_code"] = "code";
        $shoppingCartData["user_id"] = $user->id;

        // save shopping-cart & shopping-cart-item & shopping-cart-item-value
        $shoppingCart = $this->shoppingCartService->save($shoppingCartData);
        $this->shoppingCartItemService->save($shoppingCart);

        // save order
        $orderData = $request->all();
        $orderData["refuse_id"] = 123123;
        $orderData["shopping_cart_id"] = $shoppingCart->id;
        $orderData["tracking_code"] = "IRK75465";
        $orderData["user_id"] = $user->id;
        $this->orderService->save($orderData);

        //send to payment
        if($request->input("payment_type") == 1){ // Zarinpal
            $zarinpal = new zarinpal();
            $res = $zarinpal->pay($shoppingCart->total_price,$user->email,$user->mobile);
            return redirect('https://www.zarinpal.com/pg/StartPay/' . $res);
        }
    }
}
