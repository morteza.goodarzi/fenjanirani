<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\ProductDetailModel;
use App\Services\ProductDetail\ProductDetailService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ProductDetailRequest;

class ProductDetailController extends Controller
{

    public ProductDetailService $productdetailService;

    public function __construct()
    {
        $this->productdetailService = App::make(ProductDetailService::class);
    }

    public function index()
    {
        $productdetails = $this->productdetailService->paginate(10);
        return view("panel.productdetails.index",compact("productdetails"));
    }

    public function create()
    {
        return view("panel.productdetails.new");
    }

    public function edit(ProductDetailModel $productdetail)
    {
        return view("panel.productdetails.edit",compact("productdetail"));
    }

    public function store(ProductDetailRequest $request, ProductDetailModel $productdetail = null)
    {
        $productdetailData = $request->all();

        $productdetail ? $this->productdetailService->save($productdetailData,$productdetail->id) : $productdetail = $this->productdetailService->save($productdetailData);

        // there is a situation that productdetail has not changed, but gallery request changed. this situation ProductDetailObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($productdetail){
            return redirect()->route('admin.productdetails.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(ProductDetailModel $productdetail)
    {
        if( $this->productdetailService->delete($productdetail) ){
            return redirect()->route("admin.productdetails.index")->with("delete",true);
        }
    }

}
