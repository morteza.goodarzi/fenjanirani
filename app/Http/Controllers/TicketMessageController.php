<?php

namespace App\Http\Controllers;

use App\Helpers\Images;
use App\Models\TicketModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Models\TicketMessageModel;
use App\Services\TicketMessage\TicketMessageService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\TicketMessageRequest;

class TicketMessageController extends Controller
{

    public TicketMessageService $ticketMessageService;

    public function __construct()
    {
        $this->ticketMessageService = App::make(TicketMessageService::class);
    }

    public function index()
    {
        $ticketMessages = $this->ticketMessageService->paginate(10);
        return view("panel.ticket-messages.index",compact("ticketMessages"));
    }

    public function create()
    {
        return view("panel.ticket-messages.new");
    }

    public function edit(TicketMessageModel $ticketMessage)
    {
        return view("panel.ticket-messages.edit",compact("ticketMessage"));
    }

    public function store(TicketMessageRequest $request,TicketModel $ticket, TicketMessageModel $ticketMessage = null)
    {
        $ticketMessageData = $request->all();

        $ticketMessageData["user_id"] = Auth::user()->id;
        $ticketMessageData["ticket_id"] = $ticket->id;
        if($request->input("is_admin")){
            $ticketMessageData["is_admin"] = 1;
        }
        if($request->file("attachment")){
            $image = new Images();
            $attachmentName = $image->uploadFile($request,"attachment",config("upload_image_path.ticket-attachment"));
            $ticketMessageData["attachment"] = $attachmentName;
        }

        $ticketMessage ? $this->ticketMessageService->save($ticketMessageData,$ticketMessage->id) : $ticketMessage = $this->ticketMessageService->save($ticketMessageData);

        // there is a situation that ticketMessage has not changed, but gallery request changed. this situation TicketMessageObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($ticketMessage){
            if($request->has("is_admin")){
                $ticket->status = config("ticket-status.answered");
            } else {
                $ticket->status = config("ticket-status.awaiting-answer");
            }
            $ticket->save();
            return redirect()->back()->with('save',true);
        }
        return abort(500);
    }

    public function destroy(TicketMessageModel $ticketMessage)
    {
        if( $this->ticketMessageService->delete($ticketMessage) ){
            return redirect()->route("admin.ticket-messages.index")->with("delete",true);
        }
    }

}
