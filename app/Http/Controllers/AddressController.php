<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Models\AddressModel;
use App\Services\Address\AddressService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\AddressRequest;

class AddressController extends Controller
{

    public AddressService $addressService;

    public function __construct()
    {
        $this->addressService = App::make(AddressService::class);
    }

    public function index()
    {
        $address = $this->addressService->paginate(10);
        return view("profile.address.index",compact("address"));
    }

    public function create()
    {
        return view("profile.address.new");
    }

    public function edit(AddressModel $address)
    {
        return view("profile.address.edit",compact("address"));
    }

    public function store(AddressRequest $request, AddressModel $address = null)
    {
        $addressData = $request->all();
        $addressData["user_id"] = Auth::user()->id;
        $address ? $this->addressService->save($addressData,$address->id) : $address = $this->addressService->save($addressData);

        // there is a situation that address has not changed, but gallery request changed. this situation AddressObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($address){
            return redirect()->route('profile.address.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(AddressModel $address)
    {
        if( $this->addressService->delete($address) ){
            return redirect()->route("profile.address.index")->with("delete",true);
        }
    }

    public function address()
    {
        $address = $this->addressService->get(["user_id","=",Auth::user()->id]);
        return view("profile.address.index",compact("address"));
    }
}
