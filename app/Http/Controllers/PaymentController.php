<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Models\PaymentModel;
use App\Services\Payment\PaymentService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\PaymentRequest;

class PaymentController extends Controller
{

    public PaymentService $paymentService;

    public function __construct()
    {
        $this->paymentService = App::make(PaymentService::class);
    }

    public function index()
    {
        $payments = $this->paymentService->paginate(10);
        return view("panel.payments.index",compact("payments"));
    }

    public function create()
    {
        return view("panel.payments.new");
    }

    public function edit(PaymentModel $payment)
    {
        return view("panel.payments.edit",compact("payment"));
    }

    public function store(PaymentRequest $request, PaymentModel $payment = null)
    {
        $paymentData = $request->all();

        $payment ? $this->paymentService->save($paymentData,$payment->id) : $payment = $this->paymentService->save($paymentData);

        // there is a situation that payment has not changed, but gallery request changed. this situation PaymentObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($payment){
            return redirect()->route('admin.payments.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(PaymentModel $payment)
    {
        if( $this->paymentService->delete($payment) ){
            return redirect()->route("admin.payments.index")->with("delete",true);
        }
    }

    public function paymentProfile()
    {
        $payments = $this->paymentService->get(["user_id","=",Auth::user()->id]);
        return view("profile.payments.index",compact("payments"));
    }

    public function verify(Request $request)
    {
//        $MerchantID = '5e682ada-3b69-11e8-aaf3-005056a205be';
//        $Authority =$request->get('Authority') ;
//
//        $Amount = 100;
//        if ($request->get('Status') == 'OK') {
//            $client = new nusoap_client('https://www.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
//            $client->soap_defencoding = 'UTF-8';
//
//            //در خط زیر یک درخواست به زرین پال ارسال می کنیم تا از صحت پرداخت کاربر مطمئن شویم
//            $result = $client->call('PaymentVerification', [
//                [
//                    //این مقادیر را به سایت زرین پال برای دریافت تاییدیه نهایی ارسال می کنیم
//                    'MerchantID'     => $MerchantID,
//                    'Authority'      => $Authority,
//                    'Amount'         => $Amount,
//                ],
//            ]);
//
//            if ($result['Status'] == 100) {
//                return 'پرداخت با موفقیت انجام شد.';
//
//            } else {
//                return 'خطا در انجام عملیات';
//            }
//        }
//        else
//        {
//            return 'خطا در انجام عملیات';
//        }
    }
}
