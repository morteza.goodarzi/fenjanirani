<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\UserCodeModel;
use App\Services\UserCode\UserCodeService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\UserCodeRequest;

class UserCodeController extends Controller
{

    public UserCodeService $usercodeService;

    public function __construct()
    {
        $this->usercodeService = App::make(UserCodeService::class);
    }

    public function index()
    {
        $usercodes = $this->usercodeService->paginate(10);
        return view("panel.usercodes.index",compact("usercodes"));
    }

    public function create()
    {
        return view("panel.usercodes.new");
    }

    public function edit(UserCodeModel $usercode)
    {
        return view("panel.usercodes.edit",compact("usercode"));
    }

    public function store(UserCodeRequest $request, UserCodeModel $usercode = null)
    {
        $usercodeData = $request->all();

        $usercode ? $this->usercodeService->save($usercodeData,$usercode->id) : $usercode = $this->usercodeService->save($usercodeData);

        // there is a situation that usercode has not changed, but gallery request changed. this situation UserCodeObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($usercode){
            return redirect()->route('admin.usercodes.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(UserCodeModel $usercode)
    {
        if( $this->usercodeService->delete($usercode) ){
            return redirect()->route("admin.usercodes.index")->with("delete",true);
        }
    }

}
