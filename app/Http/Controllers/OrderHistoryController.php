<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\OrderHistoryModel;
use App\Services\OrderHistory\OrderHistoryService;
use Illuminate\Support\Facades\App;
use App\Http\Requests\OrderHistoryRequest;

class OrderHistoryController extends Controller
{

    public OrderHistoryService $orderhistoryService;

    public function __construct()
    {
        $this->orderhistoryService = App::make(OrderHistoryService::class);
    }

    public function index()
    {
        $orderhistorys = $this->orderhistoryService->paginate(10);
        return view("panel.orderhistorys.index",compact("orderhistorys"));
    }

    public function create()
    {
        return view("panel.orderhistorys.new");
    }

    public function edit(OrderHistoryModel $orderhistory)
    {
        return view("panel.orderhistorys.edit",compact("orderhistory"));
    }

    public function store(OrderHistoryRequest $request, OrderHistoryModel $orderhistory = null)
    {
        $orderhistoryData = $request->all();

        $orderhistory ? $this->orderhistoryService->save($orderhistoryData,$orderhistory->id) : $orderhistory = $this->orderhistoryService->save($orderhistoryData);

        // there is a situation that orderhistory has not changed, but gallery request changed. this situation OrderHistoryObserver has not performed updated method.
        // so, we should check gallery request in the controller.
        // if request has any gallery then upload it and save to db and sync galleriables
        // and also category request

        if($orderhistory){
            return redirect()->route('admin.orderhistorys.index')->with('save',true);
        }
        return abort(500);
    }

    public function destroy(OrderHistoryModel $orderhistory)
    {
        if( $this->orderhistoryService->delete($orderhistory) ){
            return redirect()->route("admin.orderhistorys.index")->with("delete",true);
        }
    }

}
