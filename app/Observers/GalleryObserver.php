<?php

namespace App\Observers;

use App\Models\GalleryModel;
use App\Services\Gallery\GalleryService;

class GalleryObserver
{

    protected GalleryService $galleryService;

    public function __construct(GalleryService $galleryService)
    {
        $this->galleryService = $galleryService;
    }

    /**
     * Handle the PostModel "created" event.
     *
     * @param GalleryModel $gallery
     * @return void
     */
    public function created(GalleryModel $gallery)
    {

    }

    /**
     * Handle the PostModel "created" event.
     *
     * @param GalleryModel $gallery
     */
    public function updated(GalleryModel $gallery)
    {

    }

    /**
     * Handle the PostModel "deleting" event.
     *
     * @param GalleryModel $gallery
     * @return void
     */
    public function deleting(GalleryModel $gallery)
    {
        //detach all gallery of all entity if exist
        $this->galleryService->detachSingleGalleries($gallery);
    }
}
