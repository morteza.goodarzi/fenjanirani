<?php

namespace App\Observers;

use App\Models\BrandModel;
use App\Services\Brand\BrandService;
use App\Services\Category\CategoryService;
use Illuminate\Support\Facades\App;

class BrandObserver
{
    protected CategoryService $categoryService;
    protected BrandService $brandService;

    public function __construct()
    {
        $this->categoryService = App::make(CategoryService::class);
        $this->brandService = App::make(BrandService::class);
    }
    public function deleting(BrandModel $brand)
    {
        // delete thumbnail of post if exist
        $this->brandService->deleteImageFile($brand);

        // delete seo_image of event if exist
        $this->brandService->deleteSeoImage($brand);

        // detach all category of post
        $this->categoryService->detachCategory($brand);

    }
}
