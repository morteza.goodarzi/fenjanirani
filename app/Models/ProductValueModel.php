<?php

namespace App\Models;

use App\Traits\Categorizable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductValueModel extends Model
{
    use HasFactory,Categorizable;
    protected $table="product_values";
    protected $primaryKey="id";
    protected $fillable=[
        "product_key_id","value"
    ];
    public $timestamps=false;

    public function key()
    {
        return $this->belongsTo(ProductKeyModel::class,'product_key_id');
    }

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("categories")->with("key");
        });
    }
}
