<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderHistoryModel extends Model
{
    use HasFactory;
    protected $table = "order_status_history";
    protected $fillable = [
        "order_id","from_status","to_status"
    ];
    public function getFromStatusPersianAttribute($value)
    {
        if($this->attributes["from_status"] == config("order-status.unPaid"))
            return "پرداخت نشده";
        elseif($this->attributes["from_status"] == config("order-status.paid"))
            return "پرداخت شده";
        if($this->attributes["from_status"] == config("order-status.preparing"))
            return "در حال آماده سازی";
        if($this->attributes["from_status"] == config("order-status.sending"))
            return "در حال ارسال";
        if($this->attributes["from_status"] == config("order-status.delivered"))
            return "تحویل داده شده";
    }
    public function getToStatusPersianAttribute($value)
    {
        if($this->attributes["to_status"] == config("order-status.unPaid"))
            return "پرداخت نشده";
        elseif($this->attributes["to_status"] == config("order-status.paid"))
            return "پرداخت شده";
        if($this->attributes["to_status"] == config("order-status.preparing"))
            return "در حال آماده سازی";
        if($this->attributes["to_status"] == config("order-status.sending"))
            return "در حال ارسال";
        if($this->attributes["to_status"] == config("order-status.delivered"))
            return "تحویل داده شده";
    }
}
