<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartItemModel extends Model
{
    use HasFactory;
    protected $table="shopping_cart_items";
    protected $primaryKey = "id";
    protected $fillable = [
        "product_id","shopping_cart_id","quantity"
    ];
}
