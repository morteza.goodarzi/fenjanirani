<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartModel extends Model
{
    use HasFactory;
    protected $table="shopping_carts";
    protected $primaryKey="id";
    protected $fillable = [
        "user_id","total_price","discount_code_id","status","city_id"
    ];
    public $timestamps=true;
    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("User");
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }

    public function cartItem()
    {
        return $this->hasMany(CartItemModel::class,"shopping_cart_id");
    }
}
