<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductDetailModel extends Model
{
    use HasFactory;
    protected $table = "product_details";
    protected $guarded = "id";
    protected $primaryKey = "id";
    protected $casts = [
        'selectable' => 'boolean',
    ];
    public function setSelectableAttribute($value): bool
    {
        return $this->attributes['selectable'] = !!$value;
    }
    public function getSelectableAttribute($value):string
    {
        if($value){
            return "بله";
        } else {
            return "خیر";
        }
    }
    public function product(): belongsTo
    {
        return $this->belongsTo(ProductModel::class,'product_id');
    }

    public function value()
    {
        return $this->belongsTo(ProductValueModel::class,"product_value_id");
    }

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("value");
        });
    }
}
