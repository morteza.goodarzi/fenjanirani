<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketMessageModel extends Model
{
    use HasFactory;
    protected $table = "ticket_messages";
    protected $primaryKey = "id";
    protected $fillable = [
        "is_admin","user_id","message","ticket_id","attachment"
    ];
    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }
    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("user");
        });
    }
}
