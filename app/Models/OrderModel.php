<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    use HasFactory;
    protected $table="orders";
    protected $primaryKey="id";
    protected $fillable = [
        "user_id","refuse_id","shopping_cart_id","tracking_code","status","description","receiver_name","receiver_family","receiver_address","receiver_national_code","receiver_postal_code"
    ];

    protected $appends = array('persianStatus');

    public function shoppingCart()
    {
        return $this->belongsTo(ShoppingCartModel::class,"shopping_cart_id");
    }
    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }
    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("shoppingCart")->with("user")->with("history");
        });
    }

    public function history()
    {
        return $this->hasMany(OrderHistoryModel::class,"order_id");
    }

    public function getPersianStatusAttribute($value)
    {
        if($this->attributes["status"] == config("order-status.paid"))
            return "پرداخت شده";
        elseif($this->attributes["status"] == config("order-status.unPaid"))
            return "پرداخت نشده";
        elseif($this->attributes["status"] == config("order-status.delivered"))
            return "تحویل داده شده";
        elseif($this->attributes["status"] == config("order-status.preparing"))
            return "در حال پردازش";
        elseif($this->attributes["status"] == config("order-status.sending"))
            return "در حال ارسال";
        else
            return "error";
    }
}
