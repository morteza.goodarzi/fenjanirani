<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketModel extends Model
{
    use HasFactory;
    protected $table = "tickets";
    protected $primaryKey = "id";
    protected $fillable = ["subject","priority","user_id","code","department_id","status","sms_notifiable"];

    protected $casts = [
        'sms_notifiable' => 'boolean',
    ];

    public function setSmsNotifiableAttribute($value): bool
    {
        return $this->attributes['sms_notifiable'] = !!$value;
    }

    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }

    public function messages()
    {
        return $this->hasMany(TicketMessageModel::class,"ticket_id");
    }

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("user")->with("messages");
        });
    }

    public function getPersianStatusAttribute($value):string
    {
        if($this->attributes["status"] == config("ticket-status.awaiting-answer"))
            return "در انتظار پاسخ";
        elseif($this->attributes["status"] == config("ticket-status.answered"))
            return "پاسخ داده شده";
        elseif($this->attributes["status"] == config("ticket-status.rejected"))
            return "رد شده";
        else
            return "error";
    }

}
