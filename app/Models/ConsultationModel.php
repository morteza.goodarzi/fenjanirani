<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultationModel extends Model
{
    use HasFactory;
    protected $table = "consultations";
    protected $primaryKey = "id";
    protected $fillable = ["mobile"];
}
