<?php

namespace App\Models;

use App\Traits\Categorizable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    use HasFactory,Categorizable;
    protected $table = "brands";
    protected $primaryKey = "id";
    protected $fillable = [
        "title","image","slug","content","canonical","seo_title","seo_description","seo_image","indexable"
    ];

    protected $casts = [
        'indexable' => 'boolean',
    ];

    public function setIndexableAttribute($value): bool
    {
        return $this->attributes['indexable'] = !!$value;
    }

    public function getIndexableAttribute($value):string
    {
        if($value){
            return "بله";
        } else {
            return "خیر";
        }
    }

    public function setContentAttribute($value)
    {
        $this->attributes["content"] = htmlspecialchars($value);
    }

    public function getContentAttribute($value)
    {
        return html_entity_decode($value);
    }

    public function products()
    {
        return $this->hasMany(ProductModel::class,"brand_id");
    }

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("categories")->orderBy("id","desc");
        });
    }
}
