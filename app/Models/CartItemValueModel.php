<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItemValueModel extends Model
{
    use HasFactory;
    protected $table="shopping_cart_item_values";
    protected $primaryKey="id";
    protected $fillable=[
        "product_key_id","product_value_id","cart_item_id"
    ];

    public function value()
    {
        return $this->belongsTo(ProductValueModel::class,"product_value_id");
    }
}
