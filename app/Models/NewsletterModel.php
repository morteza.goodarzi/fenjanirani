<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsletterModel extends Model
{
    use HasFactory;
    protected $table = "newsletters";
    protected $primaryKey = "id";
    protected $fillable = [
        "email"
    ];
}
