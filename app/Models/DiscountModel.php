<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountModel extends Model
{
    use HasFactory;
    protected $table = 'discount_code';
    protected $primaryKey = 'id';
    protected $fillable =[
        'discount_code','status','percent','from_date','to_date'
    ];
    public $timestamps = true;
}
