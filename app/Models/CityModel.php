<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    use HasFactory;
    protected $table        = "cities";
    protected $guarded      = "id";
    protected $primaryKey   = "id";
    public $timestamps=false;
    public function province()
    {
        return $this->belongsTo(ProvinceModel::class,"province_id");
    }
    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("province")->orderBy("id","desc");
        });
    }
}
