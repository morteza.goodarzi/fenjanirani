<?php

namespace App\Models;

use App\Traits\Categorizable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductKeyModel extends Model
{
    use HasFactory,Categorizable;
    protected $table="product_keys";
    protected $primaryKey="id";
    protected $fillable=[
        "name","priority"
    ];
    public $timestamps=false;

    public function values()
    {
        return $this->hasMany(ProductValueModel::class,"product_key_id");
    }

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("categories");
        });
    }
}
