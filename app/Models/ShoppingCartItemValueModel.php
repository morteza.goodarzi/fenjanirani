<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartItemValueModel extends Model
{
    use HasFactory;
    protected $table="shopping_cart_item_values";
    protected $primaryKey = "id";
    protected $fillable = [
        "cart_item_id","product_key_id","product_value_id","price"
    ];
}
