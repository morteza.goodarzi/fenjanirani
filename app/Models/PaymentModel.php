<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentModel extends Model
{
    use HasFactory;
    protected $table        = "payments";
    protected $primaryKey   = "id";
    protected $guarded      = "id";
    public $timestamps=false;
    protected $appends = array('persianStatus');

    public function order()
    {
        return $this->belongsTo(OrderModel::class,"order_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }


    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("user")->with("order");
        });
    }

    public function getPersianStatusAttribute($value)
    {
        if($this->attributes["status"] == config("payment-status.initialize"))
            return "بدون وضعیت";
        elseif($this->attributes["status"] == config("payment-status.success"))
            return "پرداخت شده";
        elseif($this->attributes["status"] == config("payment-status.failed"))
            return "خطا در پرداخت";
        else
            return "error";
    }

    public function getBankNameAttribute($value)
    {
        if($this->attributes["bank_id"] == 1)
            return "بانک ملت";
        elseif($this->attributes["bank_id"] == 2)
            return "بانک سپهر";
        elseif($this->attributes["bank_id"] == 3)
            return "ملی";
        elseif($this->attributes["bank_id"] == 4)
            return "پاسارگاد";
        elseif($this->attributes["bank_id"] == 5)
            return "زرین پال";

        else
            return "error";
    }
}
