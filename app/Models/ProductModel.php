<?php

namespace App\Models;

use App\Traits\Categorizable;
use App\Traits\Commentable;
use App\Traits\Galleriable;
use App\Traits\HasActivity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductModel extends Model
{
    use HasFactory,Categorizable,Commentable,Galleriable,HasActivity;
    protected $table = "products";
    protected $primaryKey = "id";
    protected $fillable = [
        "title","slug","price","warranty","expert_review","overview","brand_id","comment_count","indexable","commentable","thumbnail","canonical","seo_title","seo_description","seo_image"
    ];

    protected $casts = [
        'indexable' => 'boolean',
        'commentable' => 'boolean',
        'exist' => 'boolean',
    ];

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("details")->with("user")->with("brand")->with("categories")->with("galleries")->with("comments");
        });
    }

    public function details(): HasMany
    {
        return $this->hasMany(ProductDetailModel::class,'product_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }
    public function brand()
    {
        return $this->belongsTo(BrandModel::class,"brand_id");
    }

    public function setIndexableAttribute($value): bool
    {
        return $this->attributes['indexable'] = !!$value;
    }

    public function getCommentableAttribute($value):string
    {
        if($value){
            return "بله";
        } else {
            return "خیر";
        }
    }

    public function setCommentableAttribute($value): bool
    {
        return $this->attributes['commentable'] = !!$value;
    }

    public function getIndexableAttribute($value):string
    {
        if($value){
            return "بله";
        } else {
            return "خیر";
        }
    }

    public function setExistAttribute($value): bool
    {
        return $this->attributes['commentable'] = !!$value;
    }

    public function getExistAttribute($value):string
    {
        if($value){
            return "موجود";
        } else {
            return "ناموجود";
        }
    }

    public function setOverviewAttribute($value)
    {
        $this->attributes["overview"] = htmlspecialchars($value);
    }

    public function getOverviewAttribute($value)
    {
        return html_entity_decode($value);
    }

    public function setExpertReviewAttribute($value)
    {
        $this->attributes["expert_review"] = htmlspecialchars($value);
    }

    public function getExpertReviewAttribute($value)
    {
        return html_entity_decode($value);
    }
}
