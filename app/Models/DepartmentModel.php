<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model
{
    use HasFactory;
    protected $table="department_contact_us";
    protected $primaryKey="id";
    protected $guarded="id";
    public $timestamps=false;
}
