<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItemModel extends Model
{
    use HasFactory;
    protected $table="shopping_cart_items";
    protected $primaryKey="id";
    protected $fillable=[
        "product_id","shopping_cart_id","quantity"
    ];

    protected static function booted()
    {
        static::addGlobalScope('relation', function (Builder $builder) {
            $builder->with("itemValue")->with("product");
        });
    }

    public function itemValue()
    {
        return $this->hasMany(CartItemValueModel::class,"cart_item_id");
    }
    public function product()
    {
        return $this->belongsTo(ProductModel::class,"product_id");
    }
}
