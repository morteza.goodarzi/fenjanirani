<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProvinceModel extends Model
{
    use HasFactory;
    protected $table        = "provinces";
    protected $guarded      = "id";
    protected $primaryKey   = "id";
    public $timestamps=false;
     public function city()
    {
        return $this->hasMany(CityModel::class,"province_id");
    }
}
