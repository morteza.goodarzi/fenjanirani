<?php

namespace App\Console\Commands;

use App\Traits\Commandable;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;

class generateController extends Command
{
    use Commandable;

    protected $signature = 'make:controller.service {name}';
    protected $description = 'make controller with common methods like this: php artisan make:controller.service Post';

    protected Filesystem $files;
    protected string $content;

    public function __construct()
    {
        parent::__construct();
        $this->files = App::make(Filesystem::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->content = $this->files->get($this->getControllerPath());
        $contents = $this->getStubContents($this->content,$this->getStubVariables());
        $path = $this->getSourceFilePath();
        if (!$this->files->exists($path)) {
            $this->files->put($path, $contents);
            $this->info("File : {$path} created");
        } else {
            $this->info("File : {$path} already exits");
        }
    }
    public function getControllerPath ()
    {
        return base_path(). '/stubs/controller.service.stub';
    }
    public function getSourceFilePath()
    {
        return base_path('App\\Http\\Controllers') .'\\'. $this->argument('name')."Controller.php";
    }
    public function getStubVariables()
    {
        return [
            'name'        => $this->getSingularClassName($this->argument('name'))."Controller",
            'method'      => strtolower($this->argument('name')),
            'directory'      => $this->getSingularClassName($this->argument('name')),
        ];
    }
}
