<?php

namespace App\Console\Commands;

use App\Traits\Commandable;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;

class generateService extends Command
{
    use Commandable;
    protected $signature = 'make:service {name}';
    protected $description = 'make common service methods like this: php artisan make:service Post';

    protected Filesystem $files;
    protected string $content;


    public function __construct()
    {
        parent::__construct();
        $this->files = App::make(Filesystem::class);
    }

    public function handle()
    {
        $directoryPath = base_path('App\\Services') .'\\' .$this->argument("name");
        $this->makeDirectory($directoryPath);

        //generate content of repository
        $this->content = $this->files->get($this->getServicePath());
        $contents = $this->getStubContents($this->content,$this->getServiceVariables());
        $path = $this->getSourceFilePath();
        if (!$this->files->exists($path)) {
            $this->files->put($path, $contents);
            $this->info("File : {$path} created");
        } else {
            $this->info("File : {$path} already exits");
        }
    }
    public function getServicePath ()
    {
        return base_path(). '/stubs/service.stub';
    }
    public function getSourceFilePath()
    {
        return base_path('App\\Services') .'\\' .$this->argument('name') .'\\'. $this->argument('name')."Service.php";
    }
    public function getServiceVariables()
    {
        return [
            'namespace'         => 'App\\Services\\'.$this->getSingularClassName($this->argument('name')),
            'name'              => $this->getSingularClassName($this->argument('name'))."Service",
            'model'             => $this->getSingularClassName($this->argument('name'))."Model",
            'method'            => strtolower($this->argument('name')),
            'directory'         => $this->getSingularClassName($this->argument('name')),
        ];
    }
}
