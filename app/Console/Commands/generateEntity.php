<?php

namespace App\Console\Commands;

use App\Traits\Commandable;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;

class generateEntity extends Command
{
    use Commandable;
    protected $signature = 'make:entity {name}';

    protected $description = 'generate repository,service,controller.service like this: php artisan make:entity Post';

    protected Filesystem $files;
    public function __construct()
    {
        parent::__construct();
        $this->files = App::make(Filesystem::class);
    }

    public function handle()
    {
        $name = $this->argument("name");
        Artisan::call("make:repository ".$name);
        Artisan::call("make:service ".$name);
        Artisan::call("make:controller.service ".$name);

        // make request
        $requestPath = base_path("App".DIRECTORY_SEPARATOR."Http".DIRECTORY_SEPARATOR."Requests".DIRECTORY_SEPARATOR.$name."Request");
        if(!$this->checkFileExist($requestPath)){
            Artisan::call("make:request ".$this->argument("name")."Request");
        }

        // make model
        $modelPath = base_path("App".DIRECTORY_SEPARATOR."Models".DIRECTORY_SEPARATOR.$name."Model");
        if(!$this->checkFileExist($modelPath)){
            Artisan::call("make:model ".$this->argument("name")."Model");
        }
        $this->info('Entity "'.$this->argument("name").'" Created');
    }
}
