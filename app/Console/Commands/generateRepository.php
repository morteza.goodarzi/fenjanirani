<?php

namespace App\Console\Commands;

use App\Traits\Commandable;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Pluralizer;

class generateRepository extends Command
{
    use Commandable; /* for generate php files */
    protected $signature = 'make:repository {name}';
    protected $description = 'make repository from base repository like this: php artisan make:repository Post';

    protected Filesystem $files;
    protected string $repositoryContent;
    protected string $interfaceContent;


    public function __construct()
    {
        parent::__construct();
        $this->files = App::make(Filesystem::class);
    }


    public function handle()
    {
        $directoryPath = base_path('App\\Repositories') .'\\' .$this->argument("name");
        $this->makeDirectory($directoryPath);

        //generate content of repository
        $this->repositoryContent = $this->files->get($this->getRepositoryPath());
        $contents = $this->getStubContents($this->repositoryContent,$this->getRepositoryVariables());

        //generate repository
        $path = $this->getSourceFilePath();
        if (!$this->files->exists($path["repository"])) {
            $this->files->put($path["repository"], $contents);
            $this->info("File : {$path["repository"]} created");
        } else {
            $this->info("File : {$path["repository"]} already exits");
        }

        //generate content of interface
        $this->interfaceContent = $this->files->get($this->getInterfacePath());
        $contents = $this->getStubContents($this->interfaceContent,$this->getInterfaceVariables());

        //generate repository
        if (!$this->files->exists($path["interface"])) {
            $this->files->put($path["interface"], $contents);
            $this->info("File : {$path["interface"]} created");
        } else {
            $this->info("File : {$path["interface"]} already exits");
        }
    }
    public function getRepositoryPath ()
    {
        return base_path(). '/stubs/repository.stub';
    }
    public function getInterfacePath ()
    {
        return base_path(). '/stubs/repositoryInterface.stub';
    }
    public function getRepositoryVariables()
    {
        return [
            'namespace'         => 'App\\Repositories\\'.$this->getSingularClassName($this->argument('name')),
            'name'        => $this->getSingularClassName($this->argument('name'))."Repository",
            'model'             => $this->getSingularClassName($this->argument('name')."Model"),
            'interface'             =>$this->getSingularClassName($this->argument('name'))."RepositoryInterface",
        ];
    }

    public function getInterfaceVariables()
    {
        return [
            'name'         => $this->getSingularClassName($this->argument('name'))."RepositoryInterface",
            'namespace'         => 'App\\Repositories\\'.$this->getSingularClassName($this->argument('name')),
        ];
    }


    public function getSourceFilePath()
    {
        return [
            "repository" => base_path('App\\Repositories') .'\\' .$this->argument('name') .'\\'. $this->argument('name')."Repository.php",
            "interface" => base_path('App\\Repositories') .'\\' .$this->argument('name') .'\\'. $this->argument('name')."RepositoryInterface.php",
        ];
    }

}
