<?php

namespace App\Providers;

use App\Services\Menu\MenuService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class DataServiceProvider extends ServiceProvider
{
    protected MenuService $menuService;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->menuService = App::make(MenuService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $frontMenus = $this->menuService->generateForFront();
        View::share("frontMenus",$frontMenus);
    }
}
