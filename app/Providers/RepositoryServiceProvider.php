<?php

namespace App\Providers;

use App\Repositories\Activity\ActivityRepository;
use App\Repositories\Activity\ActivityRepositoryInterface;
use App\Repositories\Address\AddressRepository;
use App\Repositories\Address\AddressRepositoryInterface;
use App\Repositories\Base\BaseRepository;
use App\Repositories\Base\BaseRepositoryInterface;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Brand\BrandRepositoryInterface;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\City\CityRepository;
use App\Repositories\City\CityRepositoryInterface;
use App\Repositories\Comment\CommentRepository;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\Consultation\ConsultationRepository;
use App\Repositories\Consultation\ConsultationRepositoryInterface;
use App\Repositories\Discount\DiscountRepository;
use App\Repositories\Discount\DiscountRepositoryInterface;
use App\Repositories\Gallery\GalleryRepository;
use App\Repositories\Gallery\GalleryRepositoryInterface;
use App\Repositories\Menu\MenuRepository;
use App\Repositories\Menu\MenuRepositoryInterface;
use App\Repositories\Newsletter\NewsletterRepository;
use App\Repositories\Newsletter\NewsletterRepositoryInterface;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\OrderHistory\OrderHistoryRepository;
use App\Repositories\OrderHistory\OrderHistoryRepositoryInterface;
use App\Repositories\Page\PageRepository;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Payment\PaymentRepository;
use App\Repositories\Payment\PaymentRepositoryInterface;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\ProductDetail\ProductDetailRepository;
use App\Repositories\ProductDetail\ProductDetailRepositoryInterface;
use App\Repositories\ProductKey\ProductKeyRepository;
use App\Repositories\ProductKey\ProductKeyRepositoryInterface;
use App\Repositories\ProductValue\ProductValueRepository;
use App\Repositories\ProductValue\ProductValueRepositoryInterface;
use App\Repositories\Province\ProvinceRepository;
use App\Repositories\Province\ProvinceRepositoryInterface;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\SearchHistory\SearchHistoryRepository;
use App\Repositories\SearchHistory\SearchHistoryRepositoryInterface;
use App\Repositories\Setting\SettingRepository;
use App\Repositories\Setting\SettingRepositoryInterface;
use App\Repositories\ShoppingCart\ShoppingCartRepository;
use App\Repositories\ShoppingCart\ShoppingCartRepositoryInterface;
use App\Repositories\ShoppingCartItem\ShoppingCartItemRepository;
use App\Repositories\ShoppingCartItem\ShoppingCartItemRepositoryInterface;
use App\Repositories\ShoppingCartItemValue\ShoppingCartItemValueRepository;
use App\Repositories\ShoppingCartItemValue\ShoppingCartItemValueRepositoryInterface;
use App\Repositories\Theme\ThemeRepository;
use App\Repositories\Theme\ThemeRepositoryInterface;
use App\Repositories\Ticket\TicketRepository;
use App\Repositories\Ticket\TicketRepositoryInterface;
use App\Repositories\TicketMessage\TicketMessageRepository;
use App\Repositories\TicketMessage\TicketMessageRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            BaseRepositoryInterface::class,
            BaseRepository::class,
        );
        $this->app->bind(
            BrandRepositoryInterface::class,
            BrandRepository::class,
        );
        $this->app->bind(
            ActivityRepositoryInterface::class,
            ActivityRepository::class,
        );
        $this->app->bind(
            PostRepositoryInterface::class,
            PostRepository::class,
        );
        $this->app->bind(
            PageRepositoryInterface::class,
            PageRepository::class,
        );
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class,
        );
        $this->app->bind(
            SearchHistoryRepositoryInterface::class,
            SearchHistoryRepository::class,
        );
        $this->app->bind(
            GalleryRepositoryInterface::class,
            GalleryRepository::class,
        );
        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class,
        );
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class,
        );
        $this->app->bind(
            CommentRepositoryInterface::class,
            CommentRepository::class,
        );
        $this->app->bind(
            SettingRepositoryInterface::class,
            SettingRepository::class,
        );
        $this->app->bind(
            ThemeRepositoryInterface::class,
            ThemeRepository::class,
        );
        $this->app->bind(
            MenuRepositoryInterface::class,
            MenuRepository::class,
        );
        $this->app->bind(
            ProductKeyRepositoryInterface::class,
            ProductKeyRepository::class,
        );
        $this->app->bind(
            ProductValueRepositoryInterface::class,
            ProductValueRepository::class,
        );
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class,
        );
        $this->app->bind(
            ProductDetailRepositoryInterface::class,
            ProductDetailRepository::class,
        );
        $this->app->bind(
            OrderRepositoryInterface::class,
            OrderRepository::class,
        );
        $this->app->bind(
            OrderHistoryRepositoryInterface::class,
            OrderHistoryRepository::class,
        );
        $this->app->bind(
            ProvinceRepositoryInterface::class,
            ProvinceRepository::class,
        );
        $this->app->bind(
            CityRepositoryInterface::class,
            CityRepository::class,
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class,
        );
        $this->app->bind(
            AddressRepositoryInterface::class,
            AddressRepository::class,
        );
        $this->app->bind(
            TicketRepositoryInterface::class,
            TicketRepository::class,
        );
        $this->app->bind(
            TicketMessageRepositoryInterface::class,
            TicketMessageRepository::class,
        );
        $this->app->bind(
            PaymentRepositoryInterface::class,
            PaymentRepository::class,
        );
        $this->app->bind(
            NewsletterRepositoryInterface::class,
            NewsletterRepository::class,
        );
        $this->app->bind(
            ConsultationRepositoryInterface::class,
            ConsultationRepository::class,
        );
        $this->app->bind(
            ShoppingCartRepositoryInterface::class,
            ShoppingCartRepository::class,
        );
        $this->app->bind(
            DiscountRepositoryInterface::class,
            DiscountRepository::class,
        );
        $this->app->bind(
            ShoppingCartItemRepositoryInterface::class,
            ShoppingCartItemRepository::class,
        );
        $this->app->bind(
            ShoppingCartItemValueRepositoryInterface::class,
            ShoppingCartItemValueRepository::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
