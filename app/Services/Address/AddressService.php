<?php

namespace App\Services\Address;

use App\Models\AddressModel;
use App\Repositories\Address\AddressRepositoryInterface;
use Illuminate\Support\Facades\App;

class AddressService{

    public AddressRepositoryInterface $addressRepository;

    public function __construct()
    {
        $this->addressRepository = App::make(AddressRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->addressRepository->get($condition);
    }

    public function all()
    {
        return $this->addressRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->addressRepository->paginate($paginate);
    }

    public function save(array $address,int $id = null)
    {
        return $this->addressRepository->save($address,$id);
    }

    public function update(array $address,int $id)
    {
        return $this->addressRepository->save($address,$id);
    }

    public function delete(AddressModel $address)
    {
        return $address->delete();
    }

}
