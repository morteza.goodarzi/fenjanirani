<?php

namespace App\Services\ProductDetail;

use App\Models\ProductDetailModel;
use App\Models\ProductModel;
use App\Repositories\ProductDetail\ProductDetailRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProductDetailService{

    public ProductDetailRepositoryInterface $productdetailRepository;

    public function __construct()
    {
        $this->productdetailRepository = App::make(ProductDetailRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->productdetailRepository->get($condition);
    }

    public function all()
    {
        return $this->productdetailRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->productdetailRepository->paginate($paginate);
    }

    public function save(array $productdetail,int $id = null)
    {
        return $this->productdetailRepository->save($productdetail,$id);
    }

    public function update(array $productdetail,int $id)
    {
        return $this->productdetailRepository->save($productdetail,$id);
    }

    public function delete(ProductDetailModel $productdetail)
    {
        return $productdetail->delete();
    }

    public function createMany(array $data)
    {
        return $this->productdetailRepository->createMany($data);
    }

    public function deleteByCondition(array $condition)
    {
        return $this->productdetailRepository->deleteByCondition($condition);
    }

    public function selectable(ProductModel $product)
    {
        return $this->productdetailRepository->selectable($product);
    }

    public function getProductDetailByValue(array $option, ProductModel $product)
    {
        return$this->productdetailRepository->getProductDetailByValue($option,$product);
    }
}
