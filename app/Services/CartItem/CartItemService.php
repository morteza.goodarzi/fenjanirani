<?php

namespace App\Services\CartItem;

use App\Models\CartItemModel;
use App\Repositories\CartItem\CartItemRepositoryInterface;
use Illuminate\Support\Facades\App;

class CartItemService{

    public CartItemRepositoryInterface $cartitemRepository;

    public function __construct()
    {
        $this->cartitemRepository = App::make(CartItemRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->cartitemRepository->get($condition);
    }

    public function paginate(int $paginate)
    {
        return $this->cartitemRepository->paginate($paginate);
    }

    public function save(array $cartitem,int $id = null)
    {
        return $this->cartitemRepository->save($cartitem,$id);
    }

    public function update(array $cartitem,int $id)
    {
        return $this->cartitemRepository->save($cartitem,$id);
    }

    public function delete(CartItemModel $cartitem)
    {
        return $cartitem->delete();
    }

}
