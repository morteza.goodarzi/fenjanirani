<?php

namespace App\Services\ShoppingCartItemValue;

use App\Models\ShoppingCartItemValueModel;
use App\Repositories\ShoppingCartItemValue\ShoppingCartItemValueRepositoryInterface;
use Illuminate\Support\Facades\App;

class ShoppingCartItemValueService{

    public ShoppingCartItemValueRepositoryInterface $shoppingcartitemvalueRepository;

    public function __construct()
    {
        $this->shoppingcartitemvalueRepository = App::make(ShoppingCartItemValueRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->shoppingcartitemvalueRepository->get($condition);
    }

    public function all()
    {
        return $this->shoppingcartitemvalueRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->shoppingcartitemvalueRepository->paginate($paginate);
    }

    public function save(array $shoppingcartitemvalue,int $id = null)
    {
        return $this->shoppingcartitemvalueRepository->save($shoppingcartitemvalue,$id);
    }

    public function update(array $shoppingcartitemvalue,int $id)
    {
        return $this->shoppingcartitemvalueRepository->save($shoppingcartitemvalue,$id);
    }

    public function delete(ShoppingCartItemValueModel $shoppingcartitemvalue)
    {
        return $shoppingcartitemvalue->delete();
    }

}
