<?php

namespace App\Services\Payment;

use App\Models\PaymentModel;
use App\Repositories\Payment\PaymentRepositoryInterface;
use Illuminate\Support\Facades\App;

class PaymentService{

    public PaymentRepositoryInterface $paymentRepository;

    public function __construct()
    {
        $this->paymentRepository = App::make(PaymentRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->paymentRepository->get($condition);
    }

    public function all()
    {
        return $this->paymentRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->paymentRepository->paginate($paginate);
    }

    public function save(array $payment,int $id = null)
    {
        return $this->paymentRepository->save($payment,$id);
    }

    public function update(array $payment,int $id)
    {
        return $this->paymentRepository->save($payment,$id);
    }

    public function delete(PaymentModel $payment)
    {
        return $payment->delete();
    }

}
