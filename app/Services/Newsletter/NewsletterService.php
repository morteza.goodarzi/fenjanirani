<?php

namespace App\Services\Newsletter;

use App\Models\NewsletterModel;
use App\Repositories\Newsletter\NewsletterRepositoryInterface;
use Illuminate\Support\Facades\App;

class NewsletterService{

    public NewsletterRepositoryInterface $newsletterRepository;

    public function __construct()
    {
        $this->newsletterRepository = App::make(NewsletterRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->newsletterRepository->get($condition);
    }

    public function all()
    {
        return $this->newsletterRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->newsletterRepository->paginate($paginate);
    }

    public function save(array $newsletter,int $id = null)
    {
        return $this->newsletterRepository->save($newsletter,$id);
    }

    public function update(array $newsletter,int $id)
    {
        return $this->newsletterRepository->save($newsletter,$id);
    }

    public function delete(NewsletterModel $newsletter)
    {
        return $newsletter->delete();
    }

}
