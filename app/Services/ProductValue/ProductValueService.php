<?php

namespace App\Services\ProductValue;

use App\Models\ProductValueModel;
use App\Repositories\ProductValue\ProductValueRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProductValueService{

    public ProductValueRepositoryInterface $productvalueRepository;

    public function __construct()
    {
        $this->productvalueRepository = App::make(ProductValueRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->productvalueRepository->get($condition);
    }

    public function all()
    {
        return $this->productvalueRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->productvalueRepository->paginate($paginate);
    }

    public function save(array $productvalue,int $id = null)
    {
        return $this->productvalueRepository->save($productvalue,$id);
    }

    public function update(array $productvalue,int $id)
    {
        return $this->productvalueRepository->save($productvalue,$id);
    }

    public function delete(ProductValueModel $productvalue)
    {
        return $productvalue->delete();
    }

}
