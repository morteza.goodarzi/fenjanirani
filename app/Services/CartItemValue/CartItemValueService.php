<?php

namespace App\Services\CartItemValue;

use App\Models\CartItemValueModel;
use App\Repositories\CartItemValue\CartItemValueRepositoryInterface;
use Illuminate\Support\Facades\App;

class CartItemValueService{

    public CartItemValueRepositoryInterface $cartitemvalueRepository;

    public function __construct()
    {
        $this->cartitemvalueRepository = App::make(CartItemValueRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->cartitemvalueRepository->get($condition);
    }

    public function paginate(int $paginate)
    {
        return $this->cartitemvalueRepository->paginate($paginate);
    }

    public function save(array $cartitemvalue,int $id = null)
    {
        return $this->cartitemvalueRepository->save($cartitemvalue,$id);
    }

    public function update(array $cartitemvalue,int $id)
    {
        return $this->cartitemvalueRepository->save($cartitemvalue,$id);
    }

    public function delete(CartItemValueModel $cartitemvalue)
    {
        return $cartitemvalue->delete();
    }

}
