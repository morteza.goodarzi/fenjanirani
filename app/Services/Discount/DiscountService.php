<?php

namespace App\Services\Discount;

use App\Models\DiscountModel;
use App\Repositories\Discount\DiscountRepositoryInterface;
use Illuminate\Support\Facades\App;

class DiscountService{

    public DiscountRepositoryInterface $discountRepository;

    public function __construct()
    {
        $this->discountRepository = App::make(DiscountRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->discountRepository->get($condition);
    }

    public function all()
    {
        return $this->discountRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->discountRepository->paginate($paginate);
    }

    public function save(array $discount,int $id = null)
    {
        return $this->discountRepository->save($discount,$id);
    }

    public function update(array $discount,int $id)
    {
        return $this->discountRepository->save($discount,$id);
    }

    public function delete(DiscountModel $discount)
    {
        return $discount->delete();
    }

}
