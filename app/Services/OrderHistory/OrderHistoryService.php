<?php

namespace App\Services\OrderHistory;

use App\Models\OrderHistoryModel;
use App\Repositories\OrderHistory\OrderHistoryRepositoryInterface;
use Illuminate\Support\Facades\App;

class OrderHistoryService{

    public OrderHistoryRepositoryInterface $orderhistoryRepository;

    public function __construct()
    {
        $this->orderhistoryRepository = App::make(OrderHistoryRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->orderhistoryRepository->get($condition);
    }

    public function all()
    {
        return $this->orderhistoryRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->orderhistoryRepository->paginate($paginate);
    }

    public function save(array $orderhistory,int $id = null)
    {
        return $this->orderhistoryRepository->save($orderhistory,$id);
    }

    public function update(array $orderhistory,int $id)
    {
        return $this->orderhistoryRepository->save($orderhistory,$id);
    }

    public function delete(OrderHistoryModel $orderhistory)
    {
        return $orderhistory->delete();
    }

}
