<?php

namespace App\Services\Comment;

use App\Models\CommentModel;
use App\Repositories\Comment\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class CommentService{

    public CommentRepositoryInterface $commentRepository;

    public function __construct()
    {
        $this->commentRepository = App::make(CommentRepositoryInterface::class);
    }
    public function get(array $condition)
    {
        return $this->commentRepository->get($condition);
    }

    public function all()
    {
        return $this->commentRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->commentRepository->paginate($paginate);
    }

    public function save(array $comment,Model $model,int $id = null)
    {
        if($id){
            $comment = $this->commentRepository->find($id);
            return $comment->update($comment);
        }
        return $model->comments()->create($comment);


//        return $this->commentRepository->save($comment,$id);
    }

    public function update(array $comment,int  $id)
    {
        return $this->commentRepository->save($comment,$id);
    }

    public function delete(CommentModel $comment)
    {
        return $comment->delete();
    }
    public function count(array $condition)
    {
        return $this->commentRepository->count($condition);
    }
}
