<?php

namespace App\Services\Product;

use App\Models\ProductModel;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProductService{

    public ProductRepositoryInterface $productRepository;

    public function __construct()
    {
        $this->productRepository = App::make(ProductRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->productRepository->get($condition);
    }
    public function first(array $condition)
    {
        return $this->productRepository->first($condition);
    }

    public function latest(int $count)
    {
        return $this->productRepository->latest($count);
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->productRepository->paginate($paginate);
    }

    public function save(array $product,int $id = null)
    {
        return $this->productRepository->save($product,$id);
    }

    public function update(array $product,int $id)
    {
        return $this->productRepository->save($product,$id);
    }

    public function delete(ProductModel $product)
    {
        return $product->delete();
    }

}
