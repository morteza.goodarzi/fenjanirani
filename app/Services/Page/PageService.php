<?php

namespace App\Services\Page;

use App\Models\PageModel;
use App\Repositories\Page\PageRepositoryInterface;
use Illuminate\Support\Facades\App;

class PageService{

    public PageRepositoryInterface $pageRepository;

    public function __construct()
    {
        $this->pageRepository = App::make(PageRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->pageRepository->get($condition);
    }

    public function all()
    {
        return $this->pageRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->pageRepository->paginate($paginate);
    }

    public function save(array $page,int $id = null)
    {
        return $this->pageRepository->save($page,$id);
    }

    public function update(array $page,int $id)
    {
        return $this->pageRepository->save($page,$id);
    }

    public function delete(PageModel $page)
    {
        return $page->delete();
    }

}
