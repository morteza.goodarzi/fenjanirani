<?php

namespace App\Services\Order;

use App\Models\OrderModel;
use App\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\Facades\App;

class OrderService{

    public OrderRepositoryInterface $orderRepository;

    public function __construct()
    {
        $this->orderRepository = App::make(OrderRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->orderRepository->get($condition);
    }

    public function all()
    {
        return $this->orderRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->orderRepository->paginate($paginate);
    }

    public function save(array $order,int $id = null)
    {
        return $this->orderRepository->save($order,$id);
    }

    public function update(array $order,int $id)
    {
        return $this->orderRepository->save($order,$id);
    }

    public function changeStatus(int $status,OrderModel $order)
    {
        // log history

        $order->status = $status;
        return $order->save();
    }

    public function delete(OrderModel $order)
    {
        return $order->delete();
    }

    public function getMultipleCondition(array $condition)
    {
        return $this->orderRepository->getMultipleCondition($condition);
    }
}
