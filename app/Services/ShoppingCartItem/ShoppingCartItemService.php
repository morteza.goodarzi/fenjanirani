<?php

namespace App\Services\ShoppingCartItem;

use App\Models\CartItemModel;
use App\Models\CartItemValueModel;
use App\Models\ShoppingCartItemModel;
use App\Models\ShoppingCartModel;
use App\Repositories\ShoppingCartItem\ShoppingCartItemRepository;
use App\Repositories\ShoppingCartItem\ShoppingCartItemRepositoryInterface;
use App\Repositories\ShoppingCartItemValue\ShoppingCartItemValueRepository;
use App\Repositories\ShoppingCartItemValue\ShoppingCartItemValueRepositoryInterface;
use Illuminate\Support\Facades\App;

class ShoppingCartItemService{

    public ShoppingCartItemRepositoryInterface $shoppingCartItemRepository;
    public ShoppingCartItemValueRepositoryInterface $shoppingCartItemValueRepository;

    public function __construct()
    {
        $this->shoppingCartItemRepository = App::make(ShoppingCartItemRepositoryInterface::class);
        $this->shoppingCartItemValueRepository = App::make(ShoppingCartItemValueRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->shoppingCartItemRepository->get($condition);
    }

    public function all()
    {
        return $this->shoppingCartItemRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->shoppingCartItemRepository->paginate($paginate);
    }

// override
//    public function save(array $shoppingcartitem,int $id = null)
//    {
//        return $this->shoppingcartitemRepository->save($shoppingcartitem,$id);
//    }
    public function save(ShoppingCartModel $shoppingCart):bool
    {
        $shoppingCartItemData = [];
        $shoppingCartItemValueData = [];
        $cartCollection = \Cart::getContent();
        foreach ($cartCollection as $item)
        {
             $shoppingCartItemData["product_id"] = $item->id;
             $shoppingCartItemData["shopping_cart_id"] = $shoppingCart->id;
             $shoppingCartItemData["quantity"] = $item->quantity;
             $shoppingCartItem = $this->shoppingCartItemRepository->save($shoppingCartItemData);
            if($item->attributes->option != null)
            {
                foreach ($item->attributes->option as $option)
                {
                    $shoppingCartItemValueData["cart_item_id"] = $shoppingCartItem->id;
                    $shoppingCartItemValueData["product_key_id"] = $option['option_parent'];
                    $shoppingCartItemValueData["product_value_id"] = $option['option_id'];
                    $shoppingCartItemValueData["price"] = $option['price'];;
                    $this->shoppingCartItemValueRepository->save($shoppingCartItemValueData);
                }
            }
        }
        return true;
    }
    public function update(array $shoppingcartitem,int $id)
    {
        return $this->shoppingCartItemRepository->save($shoppingcartitem,$id);
    }

    public function delete(ShoppingCartItemModel $shoppingcartitem)
    {
        return $shoppingcartitem->delete();
    }

}
