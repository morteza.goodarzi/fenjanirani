<?php

namespace App\Services\UserCode;

use App\Models\UserCodeModel;
use App\Repositories\UserCode\UserCodeRepositoryInterface;
use Illuminate\Support\Facades\App;

class UserCodeService{

    public UserCodeRepositoryInterface $usercodeRepository;

    public function __construct()
    {
        $this->usercodeRepository = App::make(UserCodeRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->usercodeRepository->get($condition);
    }

    public function all()
    {
        return $this->usercodeRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->usercodeRepository->paginate($paginate);
    }

    public function save(array $usercode,int $id = null)
    {
        return $this->usercodeRepository->save($usercode,$id);
    }

    public function update(array $usercode,int $id)
    {
        return $this->usercodeRepository->save($usercode,$id);
    }

    public function delete(UserCodeModel $usercode)
    {
        return $usercode->delete();
    }

}
