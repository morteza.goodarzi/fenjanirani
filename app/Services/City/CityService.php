<?php

namespace App\Services\City;

use App\Models\CityModel;
use App\Repositories\City\CityRepositoryInterface;
use Illuminate\Support\Facades\App;

class CityService{

    public CityRepositoryInterface $cityRepository;

    public function __construct()
    {
        $this->cityRepository = App::make(CityRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->cityRepository->get($condition);
    }

    public function all()
    {
        return $this->cityRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->cityRepository->paginate($paginate);
    }

    public function save(array $city,int $id = null)
    {
        return $this->cityRepository->save($city,$id);
    }

    public function update(array $city,int $id)
    {
        return $this->cityRepository->save($city,$id);
    }

    public function delete(CityModel $city)
    {
        return $city->delete();
    }

}
