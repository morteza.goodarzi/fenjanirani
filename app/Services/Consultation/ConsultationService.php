<?php

namespace App\Services\Consultation;

use App\Models\ConsultationModel;
use App\Repositories\Consultation\ConsultationRepositoryInterface;
use Illuminate\Support\Facades\App;

class ConsultationService{

    public ConsultationRepositoryInterface $consultationRepository;

    public function __construct()
    {
        $this->consultationRepository = App::make(ConsultationRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->consultationRepository->get($condition);
    }

    public function all()
    {
        return $this->consultationRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->consultationRepository->paginate($paginate);
    }

    public function save(array $consultation,int $id = null)
    {
        return $this->consultationRepository->save($consultation,$id);
    }

    public function update(array $consultation,int $id)
    {
        return $this->consultationRepository->save($consultation,$id);
    }

    public function delete(ConsultationModel $consultation)
    {
        return $consultation->delete();
    }

}
