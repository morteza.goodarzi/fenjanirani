<?php

namespace App\Services\Brand;

use App\Helpers\Images;
use App\Models\BrandModel;
use App\Repositories\Brand\BrandRepositoryInterface;
use Illuminate\Support\Facades\App;

class BrandService{

    public BrandRepositoryInterface $brandRepository;

    public function __construct()
    {
        $this->brandRepository = App::make(BrandRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->brandRepository->get($condition);
    }

    public function all()
    {
        return $this->brandRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->brandRepository->paginate($paginate);
    }

    public function save(array $brand,int $id = null)
    {
        return $this->brandRepository->save($brand,$id);
    }

    public function update(array $brand,int $id)
    {
        return $this->brandRepository->save($brand,$id);
    }

    public function delete(BrandModel $brand)
    {
        return $brand->delete();
    }
    public function deleteImageFile(BrandModel $brand)
    {
        if($brand->image != "thumbnail.png"){
            $image = new Images();
            $image->deleteFile(config("upload_image_path.brand-thumbnail"),$brand->image);
        }
    }

    public function deleteSeoImage(BrandModel $brand)
    {
        if($brand->seo_image != "thumbnail.png"){
            $image = new Images();
            $image->deleteFile(config("upload_image_path.seo-image"),$brand->seo_image);
        }
    }
}
