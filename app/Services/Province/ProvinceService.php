<?php

namespace App\Services\Province;

use App\Models\ProvinceModel;
use App\Repositories\Province\ProvinceRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProvinceService{

    public ProvinceRepositoryInterface $provinceRepository;

    public function __construct()
    {
        $this->provinceRepository = App::make(ProvinceRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->provinceRepository->get($condition);
    }

    public function all()
    {
        return $this->provinceRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->provinceRepository->paginate($paginate);
    }

    public function save(array $province,int $id = null)
    {
        return $this->provinceRepository->save($province,$id);
    }

    public function update(array $province,int $id)
    {
        return $this->provinceRepository->save($province,$id);
    }

    public function delete(ProvinceModel $province)
    {
        return $province->delete();
    }

}
