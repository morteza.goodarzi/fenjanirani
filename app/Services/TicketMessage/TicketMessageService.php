<?php

namespace App\Services\TicketMessage;

use App\Models\TicketMessageModel;
use App\Repositories\TicketMessage\TicketMessageRepositoryInterface;
use Illuminate\Support\Facades\App;

class TicketMessageService{

    public TicketMessageRepositoryInterface $ticketmessageRepository;

    public function __construct()
    {
        $this->ticketmessageRepository = App::make(TicketMessageRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->ticketmessageRepository->get($condition);
    }

    public function all()
    {
        return $this->ticketmessageRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->ticketmessageRepository->paginate($paginate);
    }

    public function save(array $ticketmessage,int $id = null)
    {
        return $this->ticketmessageRepository->save($ticketmessage,$id);
    }

    public function update(array $ticketmessage,int $id)
    {
        return $this->ticketmessageRepository->save($ticketmessage,$id);
    }

    public function delete(TicketMessageModel $ticketmessage)
    {
        return $ticketmessage->delete();
    }

}
