<?php

namespace App\Services\Ticket;

use App\Models\TicketModel;
use App\Repositories\Ticket\TicketRepositoryInterface;
use Illuminate\Support\Facades\App;

class TicketService{

    public TicketRepositoryInterface $ticketRepository;

    public function __construct()
    {
        $this->ticketRepository = App::make(TicketRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->ticketRepository->get($condition);
    }

    public function all()
    {
        return $this->ticketRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->ticketRepository->paginate($paginate);
    }

    public function save(array $ticket,int $id = null)
    {
        return $this->ticketRepository->save($ticket,$id);
    }

    public function update(array $ticket,int $id)
    {
        return $this->ticketRepository->save($ticket,$id);
    }

    public function delete(TicketModel $ticket)
    {
        return $ticket->delete();
    }

    public function getMultipleCondition(array $condition)
    {
        return $this->ticketRepository->getMultipleCondition($condition);
    }
    public function getMyTicket(int $userId, int $status = null)
    {
        return $this->ticketRepository->myTicket($userId,$status);
    }
}
