<?php

namespace App\Services\ShoppingCart;

use App\Models\ShoppingCartModel;
use App\Repositories\ShoppingCart\ShoppingCartRepositoryInterface;
use Illuminate\Support\Facades\App;

class ShoppingCartService{

    public ShoppingCartRepositoryInterface $shoppingcartRepository;

    public function __construct()
    {
        $this->shoppingcartRepository = App::make(ShoppingCartRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->shoppingcartRepository->get($condition);
    }

    public function all()
    {
        return $this->shoppingcartRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->shoppingcartRepository->paginate($paginate);
    }

    public function save(array $shoppingCart,int $id = null)
    {
        return $this->shoppingcartRepository->save($shoppingCart,$id);
    }

    public function update(array $shoppingCart,int $id)
    {
        return $this->shoppingcartRepository->save($shoppingCart,$id);
    }

    public function delete(ShoppingCartModel $shoppingCart)
    {
        return $shoppingCart->delete();
    }

}
