<?php

namespace App\Services\ProductKey;

use App\Models\ProductKeyModel;
use App\Repositories\ProductKey\ProductKeyRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProductKeyService{

    public ProductKeyRepositoryInterface $productkeyRepository;

    public function __construct()
    {
        $this->productkeyRepository = App::make(ProductKeyRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->productkeyRepository->get($condition);
    }

    public function all()
    {
        return $this->productkeyRepository->all();
    }

    public function paginate(int $paginate)
    {
        return $this->productkeyRepository->paginate($paginate);
    }

    public function save(array $productkey,int $id = null)
    {
        return $this->productkeyRepository->save($productkey,$id);
    }

    public function update(array $productkey,int $id)
    {
        return $this->productkeyRepository->save($productkey,$id);
    }

    public function delete(ProductKeyModel $productkey)
    {
        return $productkey->delete();
    }

}
