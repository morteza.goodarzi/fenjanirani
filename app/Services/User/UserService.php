<?php

namespace App\Services\User;

use App\Models\User;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserService{

    public UserRepositoryInterface $userRepository;

    public function __construct()
    {
        $this->userRepository = App::make(UserRepositoryInterface::class);
    }

    public function get(array $condition)
    {
        return $this->userRepository->get($condition);
    }

    public function first(array $condition)
    {
        return $this->userRepository->first($condition);
    }

    public function paginate(int $paginate)
    {
        return $this->userRepository->paginate($paginate);
    }

    public function save(array $user,int $id = null)
    {
        return $this->userRepository->save($user,$id);
    }

    public function update(array $user,int $id)
    {
        return $this->userRepository->save($user,$id);
    }

    public function delete(User $user)
    {
        return $user->delete();
    }

    public function getMe()
    {
        return Auth::user();
    }
}
